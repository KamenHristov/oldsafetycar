FROM eclipse-temurin:11-jdk-alpine
COPY build/libs/safetycar.jar safetycar.jar
ENTRYPOINT ["sh", "-c", "java ${JAVA_OPTS} -jar safetycar.jar"]
