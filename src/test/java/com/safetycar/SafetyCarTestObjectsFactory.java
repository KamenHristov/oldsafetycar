package com.safetycar;

import com.safetycar.enums.AllowedContent;
import com.safetycar.enums.PolicyStatuses;
import com.safetycar.enums.UserRoles;
import com.safetycar.models.*;
import com.safetycar.models.User;
import com.safetycar.models.UserDetails;
import com.safetycar.models.base.Token;
import com.safetycar.models.wrappers.CalculationWrapper;
import com.safetycar.models.wrappers.UserDuplicateWrapper;
import com.safetycar.security.recaptcha.RecaptchaResponse;
import com.safetycar.web.dto.offer.CreateOfferDtoForRest;
import com.safetycar.web.dto.offer.OfferDto;
import com.safetycar.web.dto.policy.CreatePolicyDto;
import com.safetycar.web.dto.policy.ShowDetailedPolicyDto;
import com.safetycar.web.dto.policy.ShowPolicyDto;
import com.safetycar.web.dto.user.*;
import com.safetycar.web.wrapper.TokenWrapper;
import org.springframework.http.HttpStatus;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

import static com.safetycar.security.MySpringUserDetailsService.getAuthorities;
import static com.safetycar.services.MailServiceImpl.ERROR_MAIL;
import static com.safetycar.services.UserHistoryServiceImpl.*;
import static com.safetycar.util.Constants.OfferConstants.MESSAGE;
import static com.safetycar.util.Constants.ValidationConstants.ERROR;
import static com.safetycar.web.errors.util.HandlerHelper.*;

public final class SafetyCarTestObjectsFactory {

    private static final String TEST_ADDRESS = "testAddress";
    private static final String TEST_FIRST_NAME = "testFirstName";
    private static final String TEST_LAST_NAME = "testLastName";
    private static final String TELEPHONE = "12321432";
    private static final String TEST_MAIL_COM = "test@mail.com";
    private static final String PASSWORD = "password";

    private SafetyCarTestObjectsFactory() {
    }

    public static UserDetails getUserDetails(User user) {
        UserDetails userDetails = new UserDetails();
        userDetails.setUserName(user.getUserName());
        userDetails.setAddress(TEST_ADDRESS);
        userDetails.setFirstName(TEST_FIRST_NAME);
        userDetails.setLastName(TEST_LAST_NAME);
        userDetails.setTelephone(TELEPHONE);
        userDetails.setActive(true);
        userDetails.setIntegerId(0);
        return userDetails;
    }

    public static User getDisabledUser() {
        User user = getUser();
        user.setEnabled(false);
        return user;
    }

    public static User getEnabledUser() {
        User user = getUser();
        user.setEnabled(true);
        return user;
    }

    public static User getAgent() {
        User agent = getUser();
        agent.setAuthorities(getAgentRoles(agent));
        agent.setEnabled(true);
        return agent;
    }

    private static User getUser() {
        User user = new User();
        user.setUserName(TEST_MAIL_COM);
        user.setUserDetails(getUserDetails(user));
        user.setPassword(PASSWORD);
        user.setAuthorities(getUserRoles(user));
        return user;
    }

    public static User getEnabledUser(String anotherName) {
        User user = new User();
        user.setUserName(anotherName);
        user.setUserDetails(getUserDetails(user));
        user.setPassword(anotherName);
        user.setAuthorities(getUserRoles(user));
        user.setEnabled(true);
        return user;
    }

    public static UserDuplicateWrapper getUserDuplicateWrapper(UserDetails details) {
        return new UserDuplicateWrapper(details.getUserName(),
                details.getTelephone(),
                details.getAddress(),
                details.getFirstName(),
                details.getLastName());
    }

    public static CalculationWrapper getCalcWrapper(BaseAmount baseAmount, Offer offer, Coefficient coefficient) {
        return new CalculationWrapper(baseAmount.getBaseAmount(), offer.hadAccidents(),
                offer.isAboveTwentyFive(), coefficient);
    }

    public static TokenWrapper getTokenWrapper(String appUrl, Token token, Locale locale) {
        return new TokenWrapper(appUrl, locale, token);
    }

    public static VerificationToken getToken(User user) {
        VerificationToken token = new VerificationToken();
        token.setUser(user);
        token.setToken(UUID.randomUUID().toString());
        return token;
    }

    public static PasswordToken getForgottenPassToken(User user) {
        PasswordToken token = new PasswordToken();
        token.setToken(UUID.randomUUID().toString());
        token.setUser(user);
        return token;
    }

    public static VerificationToken getExpiredToken(User user) {
        VerificationToken token = new VerificationToken();
        token.setUser(user);
        token.setToken(UUID.randomUUID().toString());
        long nowMilliSec = System.currentTimeMillis();
        token.setExpiryDate(new Date(nowMilliSec));
        return token;
    }

    public static org.springframework.security.core.userdetails.User getSpringUser(User safetyCarUser) {
        return new org.springframework.security.core.userdetails.User(safetyCarUser.getUserName(),
                safetyCarUser.getPassword(),
                !safetyCarUser.isDisabled(),
                true,
                true,
                true,
                getAuthorities(safetyCarUser.getAuthorities()));
    }

    public static Coefficient getCoefficient() {
        Coefficient coefficient = new Coefficient();
        coefficient.setAccidentRisk(1);
        coefficient.setAgeRisk(1);
        coefficient.setTax(1);
        return coefficient;
    }

    public static BaseAmount getBaseAmount() {
        BaseAmount baseAmount = new BaseAmount();
        baseAmount.setBaseAmount(BigDecimal.valueOf(1000));
        baseAmount.setCarAgeMax(0);
        baseAmount.setCarAgeMax(25);
        baseAmount.setCcMax(1125);
        baseAmount.setCcMin(0);
        return baseAmount;
    }

    public static Model getModel() {
        Model model = new Model();
        model.setBrand(getBrand());
        model.setName("testModel");
        model.setYear(1900);
        model.setId(1);
        return model;
    }

    public static Brand getBrand() {
        Brand brand = new Brand();
        brand.setId(1);
        brand.setName("testBrand");
        return brand;
    }

    public static Car getCar() {
        Car car = new Car();
        car.setId(1);
        car.setCapacity(1000);
        car.setFirstRegistration(LocalDate.now());
        car.setModelYearBrand(getModel());
        return car;
    }

    public static Offer getOffer() {
        BaseAmount baseAmount = getBaseAmount();
        Offer offer = new Offer();
        offer.setCar(getCar());
        offer.setAboveTwentyFive(true);
        offer.setHadAccidents(false);
        offer.setPremium(baseAmount.getBaseAmount());
        return offer;
    }

    public static Offer getOffer(boolean testFlags) {
        BaseAmount baseAmount = getBaseAmount();
        Offer offer = new Offer();
        offer.setCar(getCar());
        offer.setAboveTwentyFive(testFlags);
        offer.setHadAccidents(testFlags);
        offer.setPremium(baseAmount.getBaseAmount());
        return offer;
    }

    public static CreateDetailsDto getDetailsDto(User user) {
        UserDetails details = user.getUserDetails();
        CreateDetailsDto dto = new CreateDetailsDto();
        dto.setAddress(details.getAddress());
        dto.setFirstName(details.getFirstName());
        dto.setLastName(details.getLastName());
        dto.setTelephone(details.getTelephone());
        return dto;
    }

    public static Image getAnyContentImage() {
        Image image = getImage();
        image.setImageType("testImage");
        return image;
    }

    public static Image getAllowedContentImage() {
        Image image = getImage();
        image.setImageType(AllowedContent.JPEG.toString());
        return image;
    }

    private static Image getImage() {
        Image image = new Image();
        image.setData(new byte[]{1});
        image.setImageName("testImageName");
        return image;
    }

    public static Policy getPolicy(User user, Offer offer) {
        Policy policy = new Policy();
        policy.setCar(getCar());
        policy.setOwner(user);
        policy.setImage(getAnyContentImage());
        policy.setOffer(offer);
        java.sql.Date startDate = new java.sql.Date(System.currentTimeMillis());
        policy.setStartDate(startDate);
        LocalDate localEndDate = startDate.toLocalDate().plusYears(1);
        policy.setEndDate(java.sql.Date.valueOf(localEndDate));

        policy.setStatus(getStatus());
        return policy;
    }

    public static OfferDto getOfferDto(Offer offer,
                                       boolean aboveTwentyFive,
                                       boolean hadAccidents) {
        OfferDto dto = new OfferDto();
        dto.setAboveTwentyFive(String.valueOf(aboveTwentyFive));
        dto.setAboveTwentyFive(String.valueOf(hadAccidents));
        Car car = offer.getCar();
        dto.setCapacity(String.valueOf(car.getCapacity()));
        Model model = car.getModelYearBrand();
        Brand brand = model.getBrand();
        dto.setMakeId(brand.getId());
        dto.setModelId(model.getId());
        dto.setDateRegistered(car.getFirstRegistration());
        return dto;
    }

    public static CreateOfferDtoForRest getCreateOfferDtoForRest(Offer offer,
                                                                 boolean aboveTwentyFive,
                                                                 boolean hadAccidents) {
        CreateOfferDtoForRest dto = new CreateOfferDtoForRest();
        dto.setAboveTwentyFive(String.valueOf(aboveTwentyFive));
        dto.setAboveTwentyFive(String.valueOf(hadAccidents));
        Car car = offer.getCar();
        dto.setCapacity(car.getCapacity());
        Model model = car.getModelYearBrand();
        dto.setModelId(model.getId());
        dto.setDateRegistered(car.getFirstRegistration());
        return dto;
    }

    public static ShowUserDto getShowUserDto(User user) {
        UserDetails userDetails = user.getUserDetails();
        ShowUserDto dto = new ShowUserDto();
        dto.setFullName(userDetails.getFullName());
        dto.setSimulatedOffers(userDetails.getOffersCount());
        dto.setUserId(userDetails.getIntegerId());

        dto.setApprovedNCount(userDetails.getApprovedPoliciesCount());
        dto.setPendingCount(userDetails.getPendingPoliciesCount());

        BigDecimal approvedPoliciesNet = userDetails.getApprovedPoliciesNet();
        BigDecimal pendingPoliciesNet = userDetails.getPendingPoliciesNet();
        dto.setApprovedNet(approvedPoliciesNet.setScale(2, RoundingMode.HALF_UP));
        dto.setPendingNet(pendingPoliciesNet.setScale(2, RoundingMode.HALF_UP));

        dto.setHistoryList(userDetails.getHistory());
        return dto;
    }

    public static RestShowUserDto getShowUserRestDto(User user) {
        UserDetails details = user.getUserDetails();
        RestShowUserDto dto = new RestShowUserDto();
        dto.setUserId(details.getIntegerId());
        dto.setFullName(details.getFullName());
        dto.setAddress(details.getAddress());
        dto.setTelephone(details.getTelephone());
        dto.setUsername(details.getUserName());
        return dto;
    }

    public static CreateDetailsDto getCreateDetailsDto(User user) {
        CreateDetailsDto dto = new CreateDetailsDto();
        UserDetails details = user.getUserDetails();
        dto.setId(details.getIntegerId());
        dto.setTelephone(details.getTelephone());
        dto.setAddress(details.getAddress());
        dto.setFirstName(details.getFirstName());
        dto.setLastName(details.getLastName());
        return dto;
    }

    public static CreateUserDto getCreateUserDto(User user) {
        CreateUserDto dto = new CreateUserDto();
        dto.setPassword(user.getPassword());
        dto.setConfirmationPassword(user.getPassword());
        dto.setEmail(user.getUserName());
        return dto;
    }

    public static History getHistory(User actor) {
        History history = new History();
        history.setActor(actor.getUserDetails());
        history.setAction("action");
        history.setHistory("history");
        return history;
    }

    public static History getHistoryForRegistration(User user) {
        return getHistory(user, REGISTRATION,
                S_HAS_REGISTERED);
    }

    public static History getHistoryForOfferCreation(User user) {
        return getHistory(user, OFFER_CREATION,
                AN_OFFER_WAS_CREATED_FOR_S);
    }

    public static History getHistoryForDetailsUpdate(User user) {
        return getHistory(user, DETAILS_UPDATED,
                S_HAS_UPDATED_THEIR_DETAILS);
    }

    private static History getHistory(User actor, String action, String historyMessage) {
        History history = new History();
        history.setActor(actor.getUserDetails());
        history.setAction(action);
        history.setHistory(String.format(historyMessage, actor.getUserName()));
        return history;
    }

    public static CreatePolicyDto getCreatePolicyDto(Policy policy) {
        CreatePolicyDto dto = new CreatePolicyDto();
        dto.setStartDate(new java.sql.Date(policy.getStartDate().getTime()).toLocalDate());
        dto.setFile(new MockMultipartFile("name", "originalName", "type", new byte[]{}));
        return dto;
    }

    public static CreatePolicyDto getCreatePolicyDtoWithCurrentStartDate(Policy policy) {
        java.util.Date submissionDate = policy.getSubmissionDate();
        long bumpAhead = System.currentTimeMillis() - submissionDate.getTime();

        java.util.Date oldStartDate = policy.getStartDate();
        java.sql.Date newStartDate = new java.sql.Date(oldStartDate.getTime() + bumpAhead);
        java.sql.Date convertToLocal = new java.sql.Date(newStartDate.getTime());

        CreatePolicyDto dto = new CreatePolicyDto();
        dto.setStartDate(convertToLocal.toLocalDate());
        dto.setUpdatePolicyId(policy.getId());
        return dto;
    }

    public static ShowPolicyDto getShowPolicyDto(Policy policy) {

        ShowPolicyDto dto = new ShowPolicyDto();

        dto.setPolicyId(policy.getId());
        dto.setOwnerId(policy.getOwner().getUserDetails().getIntegerId());

        dto.setCarMake(policy.getCar().getModelYearBrand().getBrand().getName());
        dto.setCarModel(policy.getCar().getModelYearBrand().getName());
        dto.setOfferId(policy.getOffer().getId());
        dto.setPolicyPremium(policy.getOffer().getRoundedPremium());

        dto.setCarFirstRegistration(policy.getCar().getFirstRegistration().toString());
        dto.setSubmissionDate(policy.getSubmissionDate().toString());
        dto.setStartDate(policy.getStartDate().toString());
        dto.setEndDate(policy.getEndDate().toString());

        dto.setStatus(policy.getStatus().getStatus());
        dto.setHistoryList(policy.getHistories());

        return dto;
    }

    public static ShowDetailedPolicyDto getShowDetailedPolicyDto(Policy policy) {
        ShowDetailedPolicyDto dto = new ShowDetailedPolicyDto();

        dto.setPolicyId(policy.getId());
        dto.setOwnerId(policy.getOwner().getUserDetails().getIntegerId());
        dto.setOwnerFirstName(policy.getOwner().getUserDetails().getFirstName());
        dto.setOwnerLastName(policy.getOwner().getUserDetails().getLastName());
        dto.setOwnerTelephone(policy.getOwner().getUserDetails().getTelephone());
        dto.setOwnerAddress(policy.getOwner().getUserDetails().getAddress());
        dto.setCarMake(policy.getCar().getModelYearBrand().getBrand().getName());
        dto.setCarModel(policy.getCar().getModelYearBrand().getName());
        dto.setCarFirstRegistration(policy.getCar().getFirstRegistration().toString());
        dto.setOfferId(policy.getOffer().getId());
        dto.setAccidentsInPreviousYear(policy.getOffer().hadAccidents());
        dto.setOwnerAbove25Years(policy.getOffer().isAboveTwentyFive());
        dto.setPolicyPremium(policy.getOffer().getRoundedPremium());
        dto.setSubmissionDate(policy.getSubmissionDate().toString());
        dto.setStartDate(policy.getStartDate().toString());
        dto.setEndDate(policy.getEndDate().toString());
        return dto;
    }

    public static Status getStatus() {
        Status status = new Status();
        status.setStatus(PolicyStatuses.PENDING.toString());
        return status;
    }

    public static MultipartFile getFile() {
        return new MockMultipartFile("test", "testImageName", "testImage", new byte[]{1});
    }

    public static MultipartFile getFile(Image image) {
        return new MockMultipartFile("test", image.getImageName(), image.getImageType(), image.getData());
    }

    public static RecaptchaResponse getRecaptchaResponse() {
        RecaptchaResponse recaptchaResponse = new RecaptchaResponse();
        recaptchaResponse.setSuccess(true);
        return recaptchaResponse;
    }

    public static ForgottenPasswordDto getForgottenPassDto(PasswordToken token) {
        ForgottenPasswordDto dto = new ForgottenPasswordDto();
        dto.setPassword("password");
        dto.setToken(token.getToken());
        return dto;
    }

    public static SimpleMailMessage getSimpleMailMessage(String text, String subject, String sendTo) {
        SimpleMailMessage email = new SimpleMailMessage();
        email.setSubject(subject);
        email.setTo(sendTo);
        email.setText(text);
        return email;
    }

    public static SimpleMailMessage getSimpleMailMessage(Exception e) {
        SimpleMailMessage mailMessage = new SimpleMailMessage();
        mailMessage.setTo(ERROR_MAIL);
        mailMessage.setSubject("error");
        mailMessage.setText(e.toString() + "\n" + e.getMessage() + "\n" + Arrays.toString(e.getStackTrace()));
        return mailMessage;
    }

    public static ModelAndView getMav(Throwable e, HttpStatus status) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName(ERROR);
        modelAndView.addObject(MESSAGE, e.getMessage());
        modelAndView.addObject(STATUS_CODE, status.value());
        modelAndView.addObject(CODE_TITLE, status.getReasonPhrase());
        LocalDateTime now = LocalDateTime.now();
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(PATTERN);
        modelAndView.addObject(TIME_STAMP, dateTimeFormatter.format(now));
        return modelAndView;
    }

    private static Set<Authority> getUserRoles(User user) {
        Set<Authority> result = new HashSet<>();
        Authority authority = new Authority();
        authority.setUserName(user.getUserName());
        authority.setAuthority(UserRoles.ROLE_USER.name());
        result.add(authority);
        return result;
    }

    private static Set<Authority> getAgentRoles(User user) {
        Set<Authority> result = new HashSet<>();
        Authority authority = new Authority();
        authority.setUserName(user.getUserName());
        authority.setAuthority(UserRoles.ROLE_AGENT.name());
        result.add(authority);
        return result;
    }

}
