package com.safetycar;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.jsonpath.JsonPath;

public final class TestHelper {

    public static final String LOCAL_URL = "localUrl";
    public static final char SPECIAL_CHAR = '"';

    private TestHelper() {
    }

    public static String getJsonPropertyValueAsString(ObjectMapper objectMapper,
                                                      String body,
                                                      String property) throws JsonProcessingException {
        return objectMapper.writeValueAsString(JsonPath.read(body, "$." + property));
    }

}
