package com.safetycar.fast.models;

import com.openpojo.reflection.PojoClass;
import com.openpojo.reflection.filters.FilterPackageInfo;
import com.openpojo.reflection.impl.PojoClassFactory;
import com.openpojo.validation.Validator;
import com.openpojo.validation.ValidatorBuilder;
import com.openpojo.validation.affirm.Affirm;
import com.openpojo.validation.rule.impl.GetterMustExistRule;
import com.openpojo.validation.rule.impl.SetterMustExistRule;
import com.openpojo.validation.test.impl.GetterTester;
import com.openpojo.validation.test.impl.SetterTester;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class ModelsMustBeValidPojosTest {

    private static String MODELS = "com.safetycar.models";
    private int EXPECTED_MODELS_COUNT = 15;

    @Test
    public void ensureExpectedModelsCount() {
        List<PojoClass> pojoClasses = PojoClassFactory.getPojoClasses(MODELS,
                new FilterPackageInfo());
        Affirm.affirmEquals("Classes added / removed?", EXPECTED_MODELS_COUNT, pojoClasses.size());
    }

    @Test
    public void testModelsStructureAndBehavior() {
        Validator validator = ValidatorBuilder.create()
                // Add Rules to validate structure for POJO_PACKAGE
                .with(new GetterMustExistRule())
                .with(new SetterMustExistRule())
                // Add Testers to validate behaviour for POJO_PACKAGE
                .with(new SetterTester())
                .with(new GetterTester())
                .build();

        validator.validate(MODELS, new FilterPackageInfo());
    }
}
