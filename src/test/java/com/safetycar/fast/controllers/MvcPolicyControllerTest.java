package com.safetycar.fast.controllers;

import com.safetycar.exceptions.EntityNotFoundException;
import com.safetycar.models.Offer;
import com.safetycar.models.Policy;
import com.safetycar.models.User;
import com.safetycar.services.contracts.*;
import com.safetycar.web.controllers.mvc.MvcPolicyController;
import com.safetycar.web.dto.mappers.contracts.PolicyMapper;
import com.safetycar.web.dto.policy.CreatePolicyDto;
import com.safetycar.web.dto.policy.SearchPolicyDto;
import com.safetycar.web.dto.policy.ShowDetailedPolicyDto;
import com.safetycar.web.dto.policy.ShowPolicyDto;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.security.Principal;
import java.util.*;

import static com.safetycar.SafetyCarTestObjectsFactory.*;
import static com.safetycar.repositories.filter.UserSpec.POLICIES;
import static com.safetycar.repositories.filter.base.BaseMapSpec.SORT_PARAMETER;
import static com.safetycar.util.Constants.OfferConstants.TRANSFER_OFFER;
import static com.safetycar.util.Constants.QueryConstants.ID;
import static com.safetycar.util.Constants.UserConstants.DETAILS;
import static com.safetycar.util.Constants.UserConstants.FILL_DETAILS;
import static com.safetycar.util.Constants.Views.*;
import static com.safetycar.web.controllers.mvc.MvcOfferController.REDIRECT_DETAILS_ENDPOINT;
import static com.safetycar.web.controllers.mvc.MvcPolicyController.*;

@ExtendWith(MockitoExtension.class)
public class MvcPolicyControllerTest {

    @InjectMocks
    private MvcPolicyController controller;
    @Mock
    private PolicyService mockPolicyService;
    @Mock
    private PolicyMapper mockPolicyMapper;
    @Mock
    private UserService mockUserService;
    @Mock
    private OfferService mockOfferService;
    @Mock
    private AuthorisationService mockAuthorisationService;
    @Mock
    private PolicyManager mockPolicyManager;
    @Mock
    private HttpServletRequest mockRequest;
    @Mock
    private HttpSession mockSession;
    @Mock
    private Model mockModel;
    @Mock
    private BindingResult mockBindingResult;
    @Mock
    private Principal mockPrincipal;

    private User user;
    private Offer offer;
    private Policy policy;
    private List<Policy> policyList;
    private ShowDetailedPolicyDto detailedPolicyDto;
    private ShowPolicyDto showPolicyDto;
    private List<ShowPolicyDto> showPolicyDtoList;
    private SearchPolicyDto searchPolicyDto;
    private CreatePolicyDto createPolicyDto;

    @BeforeEach
    void init() {
        user = getEnabledUser();
        offer = getOffer();
        policy = getPolicy(user, offer);
        policyList = Collections.singletonList(policy);
        detailedPolicyDto = getShowDetailedPolicyDto(policy);
        searchPolicyDto = new SearchPolicyDto();
        createPolicyDto = getCreatePolicyDto(policy);
        showPolicyDto = getShowPolicyDto(policy);
        showPolicyDtoList = Collections.singletonList(showPolicyDto);
    }

    @Test
    public void getPolicyCreatePage_ShouldGetPage_WhenUserDetailsActive() {
        //arrange
        Mockito.when(mockPrincipal.getName()).thenReturn(user.getUserName());
        Mockito.when(mockUserService.userByEmail(user.getUserName())).thenReturn(user);
        Mockito.when(mockRequest.getSession(false)).thenReturn(mockSession);
        Mockito.when(mockOfferService.getOne(offer.getId())).thenReturn(offer);
        Mockito.when(mockSession.getAttribute(REDIRECT)).thenReturn(null);
        //act
        String actual = controller.getPolicyCreatePage
                (mockModel, mockPrincipal, offer.getId(), mockRequest);
        //assert
        Assertions.assertEquals(POLICY_CREATE_VIEW, actual);
        Mockito.verify(mockAuthorisationService, Mockito.times(1))
                .authorise(user, offer);
        Mockito.verify(mockSession, Mockito.times(1))
                .removeAttribute(REDIRECT);
        Mockito.verify(mockSession, Mockito.times(1))
                .setAttribute(TRANSFER_OFFER, offer);
        Mockito.verify(mockModel, Mockito.times(1))
                .addAttribute(OFFER, offer);
        Mockito.verify(mockModel, Mockito.times(1))
                .addAttribute(USER, user.getUserDetails());
        Mockito.verify(mockModel, Mockito.times(1))
                .addAttribute(POLICY_DTO, new CreatePolicyDto());
    }

    @Test
    public void getPolicyCreatePage_ShouldRedirectToProfileEdit_WithMessage_WhenUserDetailsInactive() {
        //arrange
        Mockito.when(mockPrincipal.getName()).thenReturn(user.getUserName());
        Mockito.when(mockUserService.userByEmail(user.getUserName())).thenReturn(user);
        Mockito.when(mockRequest.getSession(false)).thenReturn(mockSession);
        Mockito.when(mockOfferService.getOne(offer.getId())).thenReturn(offer);
        Mockito.when(mockSession.getAttribute(REDIRECT)).thenReturn(null);

        user.getUserDetails().setActive(false);
        //act
        String actual = controller.getPolicyCreatePage
                (mockModel, mockPrincipal, offer.getId(), mockRequest);
        //assert
        Assertions.assertEquals(REDIRECT_DETAILS_ENDPOINT, actual);
        Mockito.verify(mockSession, Mockito.times(1))
                .setAttribute(DETAILS, FILL_DETAILS);
        Mockito.verify(mockSession, Mockito.times(1))
                .setAttribute(REDIRECT, REDIRECT_POLICY_CREATE + offer.getId());
    }

    @Test
    public void createPolicy_ShouldThrow_WhenOfferNull() {
        //arrange
        Mockito.when(mockPrincipal.getName()).thenReturn(user.getUserName());
        Mockito.when(mockUserService.userByEmail(user.getUserName())).thenReturn(user);
        Mockito.when(mockRequest.getSession(false)).thenReturn(mockSession);
        Mockito.when(mockSession.getAttribute(TRANSFER_OFFER)).thenReturn(null);
        //act
        //assert
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> controller.createPolicy(mockModel, mockRequest, mockPrincipal,
                        createPolicyDto, mockBindingResult), OFFER_NOT_FOUND);
    }

    @Test
    public void createPolicy_ShouldRedirect_WhenUserDetailsInactive() {
        //arrange
        Mockito.when(mockPrincipal.getName()).thenReturn(user.getUserName());
        Mockito.when(mockUserService.userByEmail(user.getUserName())).thenReturn(user);
        Mockito.when(mockRequest.getSession(false)).thenReturn(mockSession);
        Mockito.when(mockSession.getAttribute(TRANSFER_OFFER)).thenReturn(offer);
        String expected = REDIRECT_POLICY_CREATE + offer.getId();

        user.getUserDetails().setActive(false);
        //act
        String actual = controller.createPolicy(mockModel, mockRequest, mockPrincipal,
                createPolicyDto, mockBindingResult);
        //assert
        Assertions.assertEquals(expected, actual);
    }


    @Test
    public void createPolicy_ShouldReturnPolicyCreatePage_OnBindingResultErrors() {
        //arrange
        Mockito.when(mockPrincipal.getName()).thenReturn(user.getUserName());
        Mockito.when(mockUserService.userByEmail(user.getUserName())).thenReturn(user);
        Mockito.when(mockRequest.getSession(false)).thenReturn(mockSession);
        Mockito.when(mockSession.getAttribute(TRANSFER_OFFER)).thenReturn(offer);
        Mockito.when(mockBindingResult.hasErrors()).thenReturn(true);
        //act
        String actual = controller.createPolicy(mockModel, mockRequest, mockPrincipal,
                createPolicyDto, mockBindingResult);
        //assert
        Assertions.assertEquals(POLICY_CREATE_VIEW, actual);
        Mockito.verify(mockModel, Mockito.times(1))
                .addAttribute(OFFER, offer);
        Mockito.verify(mockModel, Mockito.times(1))
                .addAttribute(USER, user.getUserDetails());
        Mockito.verify(mockModel, Mockito.times(1))
                .addAttribute(POLICY_DTO, createPolicyDto);

    }

    @Test
    public void createPolicy_ShouldCreatePolicy() {
        //arrange
        Mockito.when(mockPrincipal.getName()).thenReturn(user.getUserName());
        Mockito.when(mockUserService.userByEmail(user.getUserName())).thenReturn(user);
        Mockito.when(mockRequest.getSession(false)).thenReturn(mockSession);
        Mockito.when(mockSession.getAttribute(TRANSFER_OFFER)).thenReturn(offer);
        Mockito.when(mockBindingResult.hasErrors()).thenReturn(false);
        Mockito.when(mockPolicyMapper.assemble(offer, createPolicyDto, user)).thenReturn(policy);
        //act
        String actual = controller.createPolicy(mockModel, mockRequest, mockPrincipal,
                createPolicyDto, mockBindingResult);
        //assert
        Assertions.assertEquals(REDIRECT_USERS_PROFILE_ENDPOINT, actual);
        Mockito.verify(mockSession, Mockito.times(1))
                .removeAttribute(TRANSFER_OFFER);
        Mockito.verify(mockPolicyManager, Mockito.times(1))
                .createPolicy(policy, offer, user);
    }

    @Test
    public void getPolicyById_ShouldReturnPolicyDetailsView() {
        //arrange
        Mockito.when(mockPrincipal.getName()).thenReturn(user.getUserName());
        Mockito.when(mockUserService.userByEmail(user.getUserName())).thenReturn(user);
        Mockito.when(mockPolicyService.getOne(policy.getId())).thenReturn(policy);
        Mockito.when(mockPolicyMapper.toDetailedDto(policy)).thenReturn(detailedPolicyDto);
        //act
        String actual = controller.getPolicyById(policy.getId(), mockModel, mockPrincipal);
        //assert
        Assertions.assertEquals(POLICY_DETAILS_VIEW, actual);
        Mockito.verify(mockAuthorisationService, Mockito.times(1))
                .authorise(user, policy);
        Mockito.verify(mockModel, Mockito.times(1))
                .addAttribute(POLICY_DETAILS, detailedPolicyDto);
    }

    @Test
    public void getAllPolicies_ShouldReturnPolicyListPage() {
        //arrange
        Mockito.when(mockPolicyService.getAll()).thenReturn(policyList);
        Mockito.when(mockPolicyMapper.toDto(policyList)).thenReturn(showPolicyDtoList);
        searchPolicyDto.setSearchingAsRole(SEARCHING_AS_AGENT);
        //act
        String actual = controller.getAllPolicies(mockModel);
        //assert
        Assertions.assertEquals(POLICIES_LIST_VIEW, actual);
        Mockito.verify(mockModel, Mockito.times(1))
                .addAttribute(POLICIES, showPolicyDtoList);
        Mockito.verify(mockModel, Mockito.times(1))
                .addAttribute(SEARCH_POLICY_DTO, searchPolicyDto);
    }

    @Test
    public void searchAllPolicies_ShouldReturnPolicyListPage() {
        //arrange
        LinkedList<Policy> emptyListIfAgentIsSearching = new LinkedList<>();
        Mockito.when(mockPolicyService.searchMyPolicies(emptyListIfAgentIsSearching,
                searchPolicyDto.getSearchParams())).thenReturn(policyList);

        Mockito.when(mockPolicyMapper.toDto(policyList)).thenReturn(showPolicyDtoList);
        searchPolicyDto.setSearchingAsRole(SEARCHING_AS_AGENT);
        //act
        String actual = controller.searchAllPolicies(mockModel, searchPolicyDto);
        //assert
        Assertions.assertEquals(POLICIES_LIST_VIEW, actual);
        Mockito.verify(mockModel, Mockito.times(1))
                .addAttribute(POLICIES, showPolicyDtoList);
        Mockito.verify(mockModel, Mockito.times(1))
                .addAttribute(SEARCH_POLICY_DTO, searchPolicyDto);
    }

    @Test
    public void searchMyPolicies_ShouldReturnPolicyListPage() {
        //arrange
        Mockito.when(mockPrincipal.getName()).thenReturn(user.getUserName());
        Mockito.when(mockUserService.userByEmail(user.getUserName())).thenReturn(user);
        Mockito.when(mockPolicyMapper.toDto(policyList)).thenReturn(showPolicyDtoList);

        Map<String, String> criteria = new HashMap<>();
        criteria.put(SORT_PARAMETER, ID);
        criteria.put(USER, USER);
        Mockito.when(mockPolicyService.searchMyPolicies(user.getPolicies(), criteria))
                .thenReturn(policyList);

        searchPolicyDto.setSearchingAsRole(SEARCHING_AS_USER);
        //act
        String actual = controller.showMyPolicies(mockModel, mockPrincipal);
        //assert
        Assertions.assertEquals(POLICIES_LIST_VIEW, actual);
        Mockito.verify(mockModel, Mockito.times(1))
                .addAttribute(POLICIES, showPolicyDtoList);
        Mockito.verify(mockModel, Mockito.times(1))
                .addAttribute(SEARCH_POLICY_DTO, searchPolicyDto);
    }

    @Test
    public void handleSearchMyPolicies_ShouldReturnPolicyListPage() {
        //arrange
        Mockito.when(mockPrincipal.getName()).thenReturn(user.getUserName());
        Mockito.when(mockUserService.userByEmail(user.getUserName())).thenReturn(user);
        Mockito.when(mockPolicyService.searchMyPolicies(user.getPolicies(),
                searchPolicyDto.getSearchParams(USER))).thenReturn(policyList);

        Mockito.when(mockPolicyMapper.toDto(policyList)).thenReturn(showPolicyDtoList);
        searchPolicyDto.setSearchingAsRole(SEARCHING_AS_USER);
        //act
        String actual = controller.handleSearchMyPolicies(mockModel, mockPrincipal, searchPolicyDto);
        //assert
        Assertions.assertEquals(POLICIES_LIST_VIEW, actual);
        Mockito.verify(mockModel, Mockito.times(1))
                .addAttribute(POLICIES, showPolicyDtoList);
        Mockito.verify(mockModel, Mockito.times(1))
                .addAttribute(SEARCH_POLICY_DTO, searchPolicyDto);
    }
}
