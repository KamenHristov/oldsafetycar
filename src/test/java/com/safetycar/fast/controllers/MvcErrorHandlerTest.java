package com.safetycar.fast.controllers;

import com.safetycar.exceptions.*;
import com.safetycar.services.contracts.BrandService;
import com.safetycar.services.contracts.MailService;
import com.safetycar.services.contracts.ModelService;
import com.safetycar.web.dto.offer.OfferDto;
import com.safetycar.web.errors.handlers.MvcHandler;
import com.safetycar.web.errors.handlers.SafetyCarLogger;
import org.apache.tomcat.util.http.fileupload.impl.SizeLimitExceededException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.core.MethodParameter;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.NoHandlerFoundException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.constraints.NotNull;
import java.util.Arrays;
import java.util.LinkedList;

import static com.safetycar.SafetyCarTestObjectsFactory.getMav;
import static com.safetycar.util.Constants.ErrorsConstants.UNEXPECTED_MESSAGE;
import static com.safetycar.util.Constants.OfferConstants.*;
import static com.safetycar.util.Constants.OfferConstants.OFFER_DTO;
import static com.safetycar.util.Constants.UserConstants.DETAILS;
import static com.safetycar.util.Constants.Views.CALCULATE_FIRST;
import static com.safetycar.util.Constants.Views.INDEX_VIEW;

@ExtendWith(MockitoExtension.class)
public class MvcErrorHandlerTest {

    private String message = "error message";

    @InjectMocks
    private MvcHandler handler;

    @Mock
    private ModelService mockModelService;
    @Mock
    private BrandService mockBrandService;
    @Mock
    private MailService mockMailService;
    @Mock
    private HttpServletRequest mockHttpServletRequest;
    @Mock
    private HttpSession mockHttpSession;
    @Mock
    private SafetyCarLogger mockSafetyCarLogger;

    private ModelAndView expected;

    @Test
    public void handleDuplicateEntity_ShouldReturnMav() {
        //arrange
        DuplicateEntityException e = new DuplicateEntityException(message);
        expected = getMav(e, HttpStatus.CONFLICT);
        //act
        ModelAndView actual = handler.handleDuplicateEntity(e);
        //assert
        Assertions.assertTrue(assertMavEqual(expected, actual));
    }

    @Test
    public void handleSizeLimitExceeded_ShouldReturnMav() {
        //arrange
        int actualSize = 10000;
        int sizeLimit = 1000;
        SizeLimitExceededException e = new SizeLimitExceededException(message, actualSize, sizeLimit);
        expected = getMav(e, HttpStatus.PAYLOAD_TOO_LARGE);
        //act
        ModelAndView actual = handler.handleSizeLimitExceeded(e);
        //assert
        Assertions.assertTrue(assertMavEqual(expected, actual));
    }

    @Test
    public void handleTooManyOffers_ShouldReturnMav() {
        //arrange
        TooManyOffersException e = new TooManyOffersException(message);
        expected = getMav(e, HttpStatus.CONFLICT);
        //act
        ModelAndView actual = handler.handleTooManyOffers(e);
        //assert
        Assertions.assertTrue(assertMavEqual(expected, actual));
    }

    @Test
    public void handleFileStorage_ShouldReturnMav() {
        //arrange
        FileStorageException e = new FileStorageException(message);
        expected = getMav(e, HttpStatus.BAD_REQUEST);
        //act
        ModelAndView actual = handler.handleFileStorage(e);
        //assert
        Assertions.assertTrue(assertMavEqual(expected, actual));
    }

    @Test
    public void handleAccessDenied_ShouldReturnMav() {
        //arrange
        AccessDeniedException e = new AccessDeniedException(message);
        expected = getMav(e, HttpStatus.FORBIDDEN);
        //act
        ModelAndView actual = handler.handleAccessDenied(e);
        //assert
        Assertions.assertTrue(assertMavEqual(expected, actual));
    }

    @Test
    public void handleSessionAttributeMissing_ShouldReturnMav() {
        //arrange
        ServletRequestBindingException e = new ServletRequestBindingException(message);
        expected = getSessionAttributeMissingMav();

        Mockito.when(mockBrandService.getAll()).thenReturn(new LinkedList<>());
        Mockito.when(mockModelService.getAll()).thenReturn(new LinkedList<>());
        Mockito.when(mockHttpServletRequest.getSession(false))
                .thenReturn(mockHttpSession);
        //act
        ModelAndView actual = handler.handleSessionAttributeMissing(e, mockHttpServletRequest);
        //assert
        Mockito.verify(mockSafetyCarLogger,Mockito.times(1))
                .warn(e);
        Assertions.assertTrue(assertMavEqual(expected, actual));
        Mockito.verify(mockHttpSession, Mockito.times(1))
                .removeAttribute(DETAILS);
        Mockito.verify(mockHttpSession, Mockito.times(1))
                .removeAttribute(TRANSFER_OFFER);
    }

    private ModelAndView getSessionAttributeMissingMav() {
        ModelAndView mav = new ModelAndView();
        mav.setViewName(INDEX_VIEW);
        mav.addObject(MESSAGE, CALCULATE_FIRST);
        mav.addObject(MODELS, new LinkedList<>());
        mav.addObject(BRANDS, new LinkedList<>());
        mav.addObject(OFFER_DTO, new OfferDto());
        return mav;
    }

    @Test
    public void handleExpired_ShouldReturnMav() {
        //arrange
        ExpiredException e = new ExpiredException(message);
        expected = getMav(e, HttpStatus.UNAUTHORIZED);
        //act
        ModelAndView actual = handler.handleExpired(e);
        //assert
        Assertions.assertTrue(assertMavEqual(expected, actual));
    }

    @Test
    public void handleNoHandlerFound_ShouldReturnMav() {
        //arrange
        NoHandlerFoundException e = getNoHandlerFoundException();
        expected = getMav(e, HttpStatus.NOT_FOUND);
        //act
        ModelAndView actual = handler.handleNoHandlerFound(e);
        //assert
        Assertions.assertTrue(assertMavEqual(expected, actual));
    }

    private NoHandlerFoundException getNoHandlerFoundException() {
        String httpMethod = "get";
        String requestURL = "some url";
        HttpHeaders headers = null;
        message = "No handler found for " + httpMethod + " " + requestURL;
        return new NoHandlerFoundException(httpMethod, requestURL, headers);
    }

    @Test
    public void handleEntityNotFound_ShouldReturnMav() {
        //arrange
        EntityNotFoundException e = new EntityNotFoundException(message);
        expected = getMav(e, HttpStatus.NOT_FOUND);
        //act
        ModelAndView actual = handler.handleEntityNotFound(e);
        //assert
        Assertions.assertTrue(assertMavEqual(expected, actual));
    }

    @Test
    public void handleMethodArgumentTypeMismatch_ShouldReturnMav() {
        //arrange
        MethodArgumentTypeMismatchException e = getMockMethodArgumentTypeMismatchException();

        expected = getMav(e.getCause(), HttpStatus.BAD_REQUEST);
        //act
        ModelAndView actual = handler.handleMethodArgumentTypeMismatch(e);
        //assert
        Assertions.assertTrue(assertMavEqual(expected, actual));
    }

    private MethodArgumentTypeMismatchException getMockMethodArgumentTypeMismatchException() {
        Object anyValue = null;
        Class<?> anyClass = null;
        String anyName = null;
        MethodParameter anyParameter = null;

        RuntimeException ex = new RuntimeException(message);
        MethodArgumentTypeMismatchException weOnlyCareAboutTheMessageSoAllOtherArgumentsAreNull =
                new MethodArgumentTypeMismatchException(anyValue, anyClass, anyName, anyParameter, ex);
        return weOnlyCareAboutTheMessageSoAllOtherArgumentsAreNull;
    }

    @Test
    public void handleAccountNotActivated_ShouldReturnMav() {
        //arrange
        AccountNotActivatedException e = new AccountNotActivatedException(message);
        expected = getMav(e, HttpStatus.FORBIDDEN);
        //act
        ModelAndView actual = handler.handleAccountNotActivated(e);
        //assert
        Assertions.assertTrue(assertMavEqual(expected, actual));
    }

    @Test
    public void handleUnhandled_ShouldReturnMav() throws Exception {
        //arrange
        Exception e = new Exception(UNEXPECTED_MESSAGE);
        expected = getMav(e, HttpStatus.INTERNAL_SERVER_ERROR);
        //act
        ModelAndView actual = handler.handleUnhandled(e);
        //assert
        Assertions.assertTrue(assertMavEqual(expected, actual));
        Mockito.verify(mockMailService, Mockito.times(1))
                .sendUnhandledErrorMail(e);
        Mockito.verify(mockSafetyCarLogger,Mockito.times(1))
                .warn(e.getMessage(),e);
    }

    private boolean assertMavEqual(@NotNull ModelAndView expected, @NotNull ModelAndView actual) {
        assert expected.getViewName() != null;
        return expected.getViewName().equals(actual.getViewName()) &&
                expected.getModelMap().equals(actual.getModelMap());
    }

}
