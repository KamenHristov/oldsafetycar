package com.safetycar.fast.controllers;

import com.safetycar.exceptions.DuplicateEntityException;
import com.safetycar.exceptions.EntityNotFoundException;
import com.safetycar.exceptions.TooManyOffersException;
import com.safetycar.models.Brand;
import com.safetycar.models.Offer;
import com.safetycar.models.User;
import com.safetycar.services.contracts.*;
import com.safetycar.web.controllers.mvc.MvcOfferController;
import com.safetycar.web.dto.mappers.contracts.OfferMapper;
import com.safetycar.web.dto.offer.OfferDto;
import com.safetycar.web.dto.offer.SearchOffersDto;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import java.security.Principal;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import static com.safetycar.SafetyCarTestObjectsFactory.*;
import static com.safetycar.util.Constants.BaseAmountConstants.CAPACITY;
import static com.safetycar.util.Constants.OfferConstants.*;
import static com.safetycar.util.Constants.ValidationConstants.ERROR_;
import static com.safetycar.util.Constants.Views.*;
import static com.safetycar.web.controllers.mvc.MvcOfferController.*;

@ExtendWith(MockitoExtension.class)
public class MvcOfferControllerTest {

    @InjectMocks
    private MvcOfferController controller;

    @Mock
    private OfferService mockOfferService;
    @Mock
    private BrandService mockBrandService;
    @Mock
    private ModelService mockModelService;
    @Mock
    private OfferMapper mockOfferMapper;
    @Mock
    private UserService mockUserService;
    @Mock
    private OfferAppendingService mockOfferAppendingService;
    @Mock
    private HttpServletRequest mockRequest;
    @Mock
    private HttpSession mockSession;
    @Mock
    private Model mockModel;
    @Mock
    private BindingResult mockBindingResult;
    @Mock
    private Principal mockPrincipal;

    private Offer offer;
    private Collection<Offer> offers;
    private OfferDto offerDto;
    private SearchOffersDto searchOffersDto = new SearchOffersDto();
    private User user;

    @BeforeEach
    void init() {
        offer = getOffer();
        offerDto = getOfferDto(offer, offer.isAboveTwentyFive(), offer.hadAccidents());
        user = getEnabledUser();
        offers = Collections.singletonList(offer);
    }

    @Test
    public void getMakes_ShouldReturnMakesCollection() {
        //arrange
        List<Brand> expected = Collections.singletonList(getBrand());
        Mockito.when(mockBrandService.getAll()).thenReturn(expected);
        //act
        Collection<Brand> actual = controller.getMakes();
        //assert
        Assertions.assertEquals(expected, actual);
    }

    @Test
    public void getModels_ShouldReturnModelsCollection() {
        //arrange
        List<com.safetycar.models.Model> expected = Collections.singletonList(getModel());
        Mockito.when(mockModelService.getAll()).thenReturn(expected);
        //act
        Collection<com.safetycar.models.Model> actual = controller.getModels();
        //assert
        Assertions.assertEquals(expected, actual);
    }

    @Test
    public void proceedOffer_ShouldPerformTasks_WhenUserRegistered() {
        //arrange
        Mockito.when(mockUserService.userByEmail(Mockito.anyString()))
                .thenReturn(user);
        Mockito.when(mockPrincipal.getName()).thenReturn(user.getUserName());
        Mockito.when(mockRequest.getSession(false)).thenReturn(mockSession);
        //act
        String actual = controller.proceedOffer(mockModel, offer, mockPrincipal, mockRequest);
        //assert
        Assertions.assertEquals(REDIRECT_INDEX_ENDPOINT, actual);
        Mockito.verify(mockOfferAppendingService, Mockito.times(1))
                .appendOfferAndRecordHistory(offer, user);
        Mockito.verify(mockUserService, Mockito.times(1))
                .update(user);
        Mockito.verify(mockModel, Mockito.times(1))
                .addAttribute(OFFER_DTO, new OfferDto());
        Mockito.verify(mockSession, Mockito.times(1))
                .removeAttribute(TRANSFER_OFFER);

    }

    @Test
    public void proceedOffer_ShouldReturnIndex_WhenTooManyOffersException() {
        //arrange
        TooManyOffersException e = new TooManyOffersException("message");
        Mockito.when(mockUserService.userByEmail(Mockito.anyString()))
                .thenReturn(user);
        Mockito.when(mockPrincipal.getName()).thenReturn(user.getUserName());
        Mockito.when(mockRequest.getSession(false)).thenReturn(mockSession);
        Mockito.doThrow(e).when(mockOfferAppendingService)
                .appendOfferAndRecordHistory(offer, user);
        //act
        String actual = controller.proceedOffer(mockModel, offer, mockPrincipal, mockRequest);
        //assert
        Assertions.assertEquals(INDEX_VIEW, actual);
        Mockito.verify(mockModel, Mockito.times(1))
                .addAttribute(OFFER_ERROR, e.getMessage());
        Mockito.verify(mockModel, Mockito.times(1))
                .addAttribute(OFFER_DTO, new OfferDto());
        Mockito.verify(mockSession, Mockito.times(1))
                .removeAttribute(TRANSFER_OFFER);

    }

    @Test
    public void proceedOffer_ShouldReturnIndex_WhenDuplicateEntityException() {
        //arrange
        DuplicateEntityException e = new DuplicateEntityException("message");
        Mockito.when(mockUserService.userByEmail(Mockito.anyString()))
                .thenReturn(user);
        Mockito.when(mockPrincipal.getName()).thenReturn(user.getUserName());
        Mockito.when(mockRequest.getSession(false)).thenReturn(mockSession);
        Mockito.doThrow(e).when(mockOfferAppendingService)
                .appendOfferAndRecordHistory(offer, user);
        //act
        String actual = controller.proceedOffer(mockModel, offer, mockPrincipal, mockRequest);
        //assert
        Assertions.assertEquals(INDEX_VIEW, actual);
        Mockito.verify(mockModel, Mockito.times(1))
                .addAttribute(OFFER_ERROR, e.getMessage());
        Mockito.verify(mockModel, Mockito.times(1))
                .addAttribute(OFFER_DTO, new OfferDto());
        Mockito.verify(mockSession, Mockito.times(1))
                .removeAttribute(TRANSFER_OFFER);

    }

    @Test
    public void proceedOffer_ShouldPerformTasks_WhenUserUnregistered() {
        //arrange
        Mockito.when(mockRequest.getSession()).thenReturn(mockSession);
        //act
        String actual = controller.proceedOffer(mockModel, offer, mockPrincipal, mockRequest);
        //assert
        Assertions.assertEquals(REDIRECT_LOGIN_ENDPOINT, actual);
        Mockito.verify(mockSession, Mockito.times(1))
                .setAttribute(REGISTER_FIRST, CONTINUE_WITH_YOUR_OFFER);

    }

    @Test
    public void handleGetMyOffers_ShouldReturnOffersCollection() {
        //arrange
        Mockito.when(mockPrincipal.getName()).thenReturn(user.getUserName());
        Mockito.when(mockUserService.userByEmail(user.getUserName()))
                .thenReturn(user);
        Mockito.when(mockOfferService.getMyOffers(user))
                .thenReturn(offers);
        //act
        String actual = controller.handleGetMyOffers(mockModel, mockPrincipal);
        //assert
        Assertions.assertEquals(OFFER_RESULTS_VIEW, actual);
        Mockito.verify(mockModel, Mockito.times(1))
                .addAttribute(MY_OFFERS, offers);
        Mockito.verify(mockModel, Mockito.times(1))
                .addAttribute(SEARCH_OFFERS_DTO, searchOffersDto);
    }

    @Test
    public void handleSearchMyOffers_ShouldReturnOffersCollection() {
        //arrange
        Mockito.when(mockPrincipal.getName()).thenReturn(user.getUserName());
        Mockito.when(mockUserService.userByEmail(user.getUserName()))
                .thenReturn(user);
        Mockito.when(mockOfferService.searchMyOffers(user, searchOffersDto.getSearchParams()))
                .thenReturn(offers);
        //act
        String actual = controller.handleSearchMyOffers(mockModel, mockPrincipal, searchOffersDto);
        //assert
        Assertions.assertEquals(OFFER_RESULTS_VIEW, actual);
        Mockito.verify(mockModel, Mockito.times(1))
                .addAttribute(MY_OFFERS, offers);
        Mockito.verify(mockModel, Mockito.times(1))
                .addAttribute(SEARCH_OFFERS_DTO, searchOffersDto);
    }

    @Test
    public void calculatePremium_ShouldReturnIndexView() {
        //arrange
        //act
        String actual = controller.calculatePremium(mockModel);
        //assert
        Assertions.assertEquals(INDEX_VIEW, actual);
        Mockito.verify(mockModel, Mockito.times(1))
                .addAttribute(OFFER_DTO, new OfferDto());
    }

    @Test
    public void calculatePremium_ShouldReturnResult_WhenAllIsWell() {
        //arrange
        Mockito.when(mockBindingResult.hasErrors())
                .thenReturn(false);
        Mockito.when(mockOfferMapper.fromDto(offerDto))
                .thenReturn(offer);
        Mockito.when(mockRequest.getSession())
                .thenReturn(mockSession);
        //act
        String actual = controller.calculatePremium(mockModel, offerDto, mockBindingResult, mockRequest);
        //assert
        Assertions.assertEquals(INDEX_VIEW, actual);
        Mockito.verify(mockSession, Mockito.times(1))
                .setAttribute(TRANSFER_OFFER, offer);
        Mockito.verify(mockModel, Mockito.times(1))
                .addAttribute(OFFER_DTO, offerDto);
        String actualPremium = offer.getRoundedPremium().toString();
        Mockito.verify(mockModel, Mockito.times(1))
                .addAttribute(RESULT, actualPremium);
    }

    @Test
    public void calculatePremium_ShouldReturnIndexView_OnBindingResultErrors() {
        //arrange
        Mockito.when(mockBindingResult.hasErrors()).thenReturn(true);
        //act
        String actual = controller.calculatePremium(mockModel, offerDto, mockBindingResult, mockRequest);
        //assert
        Assertions.assertEquals(INDEX_VIEW, actual);
        Mockito.verify(mockModel, Mockito.times(1))
                .addAttribute(OFFER_DTO, offerDto);
    }

    @Test
    public void calculatePremium_ShouldReturnIndexView_OnEntityNotFoundException() {
        //arrange
        EntityNotFoundException e = new EntityNotFoundException("message");
        Mockito.when(mockBindingResult.hasErrors()).thenReturn(false);
        Mockito.doThrow(e).when(mockOfferMapper).fromDto(offerDto);
        //act
        String actual = controller.calculatePremium(mockModel, offerDto, mockBindingResult, mockRequest);
        //assert
        Assertions.assertEquals(INDEX_VIEW, actual);
        Mockito.verify(mockModel, Mockito.times(1))
                .addAttribute(OFFER_DTO, offerDto);
        Mockito.verify(mockBindingResult, Mockito.times(1))
                .rejectValue(CAPACITY, ERROR_ + OFFER_DTO, e.getMessage());

    }

    @Test
    public void calculatePremiumOfferPage_ShouldReturnOfferPageView() {
        //arrange
        //act
        String actual = controller.calculatePremiumOfferPage(mockModel);
        //assert
        Assertions.assertEquals(OFFER_VIEW, actual);
        Mockito.verify(mockModel, Mockito.times(1))
                .addAttribute(OFFER_DTO, new OfferDto());
    }

    @Test
    public void calculatePremiumOfferPage_ShouldReturnResult_WhenAllIsWell() {
        //arrange
        Mockito.when(mockBindingResult.hasErrors())
                .thenReturn(false);
        Mockito.when(mockOfferMapper.fromDto(offerDto))
                .thenReturn(offer);
        Mockito.when(mockRequest.getSession())
                .thenReturn(mockSession);
        //act
        String actual = controller.calculatePremiumOfferPage(mockModel, offerDto, mockBindingResult, mockRequest);
        //assert
        Assertions.assertEquals(OFFER_VIEW, actual);
        Mockito.verify(mockSession, Mockito.times(1))
                .setAttribute(TRANSFER_OFFER, offer);
        Mockito.verify(mockModel, Mockito.times(1))
                .addAttribute(OFFER_DTO, offerDto);
        String actualPremium = offer.getRoundedPremium().toString();
        Mockito.verify(mockModel, Mockito.times(1))
                .addAttribute(RESULT, actualPremium);
    }

    @Test
    public void calculatePremiumOfferPage_ShouldReturnIndexView_OnBindingResultErrors() {
        //arrange
        Mockito.when(mockBindingResult.hasErrors()).thenReturn(true);
        //act
        String actual = controller.calculatePremiumOfferPage(mockModel, offerDto, mockBindingResult, mockRequest);
        //assert
        Assertions.assertEquals(OFFER_VIEW, actual);
        Mockito.verify(mockModel, Mockito.times(1))
                .addAttribute(OFFER_DTO, offerDto);
    }

    @Test
    public void calculatePremiumOfferPage_ShouldReturnIndexView_OnEntityNotFoundException() {
        //arrange
        EntityNotFoundException e = new EntityNotFoundException("message");
        Mockito.when(mockBindingResult.hasErrors()).thenReturn(false);
        Mockito.doThrow(e).when(mockOfferMapper).fromDto(offerDto);
        //act
        String actual = controller.calculatePremiumOfferPage(mockModel, offerDto, mockBindingResult, mockRequest);
        //assert
        Assertions.assertEquals(OFFER_VIEW, actual);
        Mockito.verify(mockModel, Mockito.times(1))
                .addAttribute(OFFER_DTO, offerDto);
        Mockito.verify(mockBindingResult, Mockito.times(1))
                .rejectValue(CAPACITY, ERROR_ + OFFER_DTO, e.getMessage());

    }

    @Test
    public void appendOfferFromOfferPage_ShouldAppend_WhenAllIsWell() {
        //arrange
        Mockito.when(mockPrincipal.getName())
                .thenReturn(user.getUserName());
        Mockito.when(mockUserService.userByEmail(user.getUserName()))
                .thenReturn(user);
        Mockito.when(mockRequest.getSession())
                .thenReturn(mockSession);
        Mockito.when(mockSession.getAttribute(TRANSFER_OFFER))
                .thenReturn(offer);
        //act
        String actual = controller.appendOfferFromOfferPage(mockRequest, mockPrincipal, mockModel);
        //assert
        Assertions.assertEquals(REDIRECT_DEDICATED_SIMULATION, actual);
        Mockito.verify(mockOfferAppendingService, Mockito.times(1))
                .appendOfferAndRecordHistory(offer, user);
        Mockito.verify(mockUserService, Mockito.times(1))
                .update(user);
        Mockito.verify(mockSession, Mockito.times(1))
                .removeAttribute(TRANSFER_OFFER);
    }

    @Test
    public void appendOfferFromOfferPage_ShouldReturnOfferPage_OnDuplicateEntityException() {
        //arrange
        DuplicateEntityException e = new DuplicateEntityException("message");
        Mockito.when(mockPrincipal.getName())
                .thenReturn(user.getUserName());
        Mockito.when(mockUserService.userByEmail(user.getUserName()))
                .thenReturn(user);
        Mockito.when(mockRequest.getSession())
                .thenReturn(mockSession);
        Mockito.when(mockSession.getAttribute(TRANSFER_OFFER))
                .thenReturn(offer);
        Mockito.doThrow(e).when(mockOfferAppendingService).appendOfferAndRecordHistory(offer, user);
        //act
        String actual = controller.appendOfferFromOfferPage(mockRequest, mockPrincipal, mockModel);
        //assert
        Assertions.assertEquals(OFFER_VIEW, actual);
        Mockito.verify(mockModel).addAttribute(OFFER_ERROR, e.getMessage());
        Mockito.verify(mockModel).addAttribute(OFFER_DTO, new OfferDto());
        Mockito.verify(mockSession, Mockito.times(1))
                .removeAttribute(TRANSFER_OFFER);
    }

    @Test
    public void appendOfferFromOfferPage_ShouldReturnOfferPage_OnTooManyOfferException() {
        //arrange
        DuplicateEntityException e = new DuplicateEntityException("message");
        Mockito.when(mockPrincipal.getName())
                .thenReturn(user.getUserName());
        Mockito.when(mockUserService.userByEmail(user.getUserName()))
                .thenReturn(user);
        Mockito.when(mockRequest.getSession())
                .thenReturn(mockSession);
        Mockito.when(mockSession.getAttribute(TRANSFER_OFFER))
                .thenReturn(offer);
        Mockito.doThrow(e).when(mockOfferAppendingService).appendOfferAndRecordHistory(offer, user);
        //act
        String actual = controller.appendOfferFromOfferPage(mockRequest, mockPrincipal, mockModel);
        //assert
        Assertions.assertEquals(OFFER_VIEW, actual);
        Mockito.verify(mockModel).addAttribute(OFFER_ERROR, e.getMessage());
        Mockito.verify(mockModel).addAttribute(OFFER_DTO, new OfferDto());
        Mockito.verify(mockSession, Mockito.times(1))
                .removeAttribute(TRANSFER_OFFER);
    }

    @Test
    public void appendOfferFromOfferPage_ShouldReturnOfferPage_OnNullOffer() {
        //arrange
        Mockito.when(mockPrincipal.getName())
                .thenReturn(user.getUserName());
        Mockito.when(mockUserService.userByEmail(user.getUserName()))
                .thenReturn(user);
        Mockito.when(mockRequest.getSession())
                .thenReturn(mockSession);
        Mockito.when(mockSession.getAttribute(TRANSFER_OFFER))
                .thenReturn(null);
        //act
        String actual = controller.appendOfferFromOfferPage(mockRequest, mockPrincipal, mockModel);
        //assert
        Assertions.assertEquals(OFFER_VIEW, actual);
        Mockito.verify(mockModel).addAttribute(OFFER_ERROR, CALCULATE_FIRST);
        Mockito.verify(mockModel).addAttribute(OFFER_DTO, new OfferDto());
        Mockito.verify(mockSession, Mockito.times(1))
                .removeAttribute(TRANSFER_OFFER);
    }

}
