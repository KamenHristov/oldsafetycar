package com.safetycar.fast.controllers;

import com.safetycar.enums.UserRoles;
import com.safetycar.exceptions.DuplicateEntityException;
import com.safetycar.exceptions.InvalidOldPasswordException;
import com.safetycar.exceptions.SamePasswordException;
import com.safetycar.models.*;
import com.safetycar.services.contracts.*;
import com.safetycar.web.controllers.mvc.MvcUserController;
import com.safetycar.web.dto.mappers.contracts.ImageMapper;
import com.safetycar.web.dto.mappers.contracts.UserMapper;
import com.safetycar.web.dto.policy.CreatePolicyDto;
import com.safetycar.web.dto.user.CreateDetailsDto;
import com.safetycar.web.dto.user.CreateUserDto;
import com.safetycar.web.dto.user.PasswordChangeDto;
import com.safetycar.web.dto.user.ShowUserDto;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.security.Principal;
import java.util.HashSet;
import java.util.Set;

import static com.safetycar.SafetyCarTestObjectsFactory.*;
import static com.safetycar.util.Constants.QueryConstants.TELEPHONE;
import static com.safetycar.util.Constants.UserConstants.*;
import static com.safetycar.util.Constants.ValidationConstants.BINDING_RESULT_ERRORS;
import static com.safetycar.util.Constants.ValidationConstants.ERROR_;
import static com.safetycar.util.Constants.Views.*;
import static com.safetycar.web.controllers.mvc.MvcPolicyController.*;
import static com.safetycar.web.controllers.mvc.MvcUserController.*;

@ExtendWith(MockitoExtension.class)
public class MvcUserControllerTest {

    @InjectMocks
    private MvcUserController controller;
    @Mock
    private AccountManager mockAccountManager;
    @Mock
    private UserService mockUserService;
    @Mock
    private UserMapper mockUserMapper;
    @Mock
    private PolicyService mockPolicyService;
    @Mock
    private AuthorisationService mockAuthorisationService;
    @Mock
    private ImageMapper mockImageMapper;
    @Mock
    private PolicyManager mockPolicyManager;
    @Mock
    private PasswordChangingService mockPasswordChangingService;
    @Mock
    private HttpServletRequest mockRequest;
    @Mock
    private HttpSession mockSession;
    @Mock
    private Model mockModel;
    @Mock
    private BindingResult mockBindingResult;
    @Mock
    private Principal mockPrincipal;

    private User user;
    private Offer offer;
    private Policy policy;
    private CreatePolicyDto createPolicyDto;
    private CreateUserDto createUserDto;
    private CreateDetailsDto createDetailsDto;
    private PasswordChangeDto passwordChangeDto;
    private Integer id;

    @BeforeEach
    void init() {
        user = getEnabledUser();
        offer = getOffer();
        policy = getPolicy(user, offer);
        createPolicyDto = getCreatePolicyDto(policy);
        createUserDto = getCreateUserDto(user);
        createDetailsDto = getCreateDetailsDto(user);
        passwordChangeDto = new PasswordChangeDto();
        id = user.getUserDetails().getIntegerId();
    }

    @Test
    public void handleChangePassword_ShouldReturnProfileEditView_OnBindingResultErrors() {
        //arrange
        Mockito.when(mockBindingResult.hasErrors()).thenReturn(true);
        Mockito.when(mockPrincipal.getName()).thenReturn(user.getUserName());
        Mockito.when(mockAccountManager.getDetailsToEdit(user.getUserName()))
                .thenReturn(createDetailsDto);
        //act
        String actual = controller.handleChangePassword(passwordChangeDto, mockBindingResult,
                mockRequest, mockModel, mockPrincipal);
        //assert
        Assertions.assertEquals(PROFILE_EDIT_VIEW, actual);
        Mockito.verify(mockModel, Mockito.times(1))
                .addAttribute(PASSWORD_ERROR, PASSWORD_ERROR_MESSAGE);
        Mockito.verify(mockModel, Mockito.times(1))
                .addAttribute(CREATE_USER_DTO, createDetailsDto);
        Mockito.verify(mockModel, Mockito.times(1))
                .addAttribute(PASSWORD_DTO, passwordChangeDto);

    }

    @Test
    public void handleChangePassword_ShouldReturnProfileEditView_OnSamePasswordException() {
        //arrange
        Mockito.when(mockPrincipal.getName()).thenReturn(user.getUserName());
        Mockito.when(mockAccountManager.getDetailsToEdit(user.getUserName()))
                .thenReturn(createDetailsDto);
        Mockito.when(mockUserService.userByEmail(user.getUserName())).thenReturn(user);

        SamePasswordException e = new SamePasswordException();
        Mockito.doThrow(e).when(mockPasswordChangingService).changePassword(user,
                passwordChangeDto.getPassword(), passwordChangeDto.getOldPassword());
        //act
        String actual = controller.handleChangePassword(passwordChangeDto, mockBindingResult,
                mockRequest, mockModel, mockPrincipal);
        //assert
        Assertions.assertEquals(PROFILE_EDIT_VIEW, actual);
        Mockito.verify(mockBindingResult, Mockito.times(1))
                .rejectValue(PASSWORD, ERROR_ + PASSWORD_DTO, e.getMessage());
        Mockito.verify(mockModel, Mockito.times(1))
                .addAttribute(PASSWORD_ERROR, e.getMessage());
        Mockito.verify(mockModel, Mockito.times(1))
                .addAttribute(CREATE_USER_DTO, createDetailsDto);
        Mockito.verify(mockModel, Mockito.times(1))
                .addAttribute(PASSWORD_DTO, passwordChangeDto);

    }

    @Test
    public void handleChangePassword_ShouldReturnProfileEditView_OnInvalidOldPassword() {
        //arrange
        Mockito.when(mockPrincipal.getName()).thenReturn(user.getUserName());
        Mockito.when(mockAccountManager.getDetailsToEdit(user.getUserName()))
                .thenReturn(createDetailsDto);
        Mockito.when(mockUserService.userByEmail(user.getUserName())).thenReturn(user);

        InvalidOldPasswordException e = new InvalidOldPasswordException();
        Mockito.doThrow(e).when(mockPasswordChangingService).changePassword(user,
                passwordChangeDto.getPassword(), passwordChangeDto.getOldPassword());
        //act
        String actual = controller.handleChangePassword(passwordChangeDto, mockBindingResult,
                mockRequest, mockModel, mockPrincipal);
        //assert
        Assertions.assertEquals(PROFILE_EDIT_VIEW, actual);
        Mockito.verify(mockBindingResult, Mockito.times(1))
                .rejectValue(OLD_PASSWORD, ERROR_ + PASSWORD_DTO, e.getMessage());
        Mockito.verify(mockModel, Mockito.times(1))
                .addAttribute(PASSWORD_ERROR, e.getMessage());
        Mockito.verify(mockModel, Mockito.times(1))
                .addAttribute(CREATE_USER_DTO, createDetailsDto);
        Mockito.verify(mockModel, Mockito.times(1))
                .addAttribute(PASSWORD_DTO, passwordChangeDto);

    }

    @Test
    public void handleChangePassword_ShouldReturnProfileEditView_AndChangePassword() {
        //arrange
        Mockito.when(mockPrincipal.getName()).thenReturn(user.getUserName());
        Mockito.when(mockAccountManager.getDetailsToEdit(user.getUserName()))
                .thenReturn(createDetailsDto);
        Mockito.when(mockUserService.userByEmail(user.getUserName())).thenReturn(user);
        //act
        String actual = controller.handleChangePassword(passwordChangeDto, mockBindingResult,
                mockRequest, mockModel, mockPrincipal);
        //assert
        Assertions.assertEquals(PROFILE_EDIT_VIEW, actual);
        Mockito.verify(mockPasswordChangingService, Mockito.times(1)).changePassword(user,
                passwordChangeDto.getPassword(), passwordChangeDto.getOldPassword());
        Mockito.verify(mockModel, Mockito.times(1))
                .addAttribute(CREATE_USER_DTO, createDetailsDto);
        Mockito.verify(mockModel, Mockito.times(1))
                .addAttribute(PASSWORD_DTO, passwordChangeDto);

    }

    @Test
    public void getDetailsUpdateForm_ShouldReturnProfileEditView() {
        //arrange
        Mockito.when(mockPrincipal.getName()).thenReturn(user.getUserName());
        Mockito.when(mockAccountManager.getDetailsToEdit(user.getUserName()))
                .thenReturn(createDetailsDto);
        //act
        String actual = controller.getDetailsUpdateForm(mockModel, mockPrincipal, mockRequest);
        //assert
        Assertions.assertEquals(PROFILE_EDIT_VIEW, actual);
        Mockito.verify(mockModel, Mockito.times(1))
                .addAttribute(CREATE_USER_DTO, createDetailsDto);
        Mockito.verify(mockModel, Mockito.times(1))
                .addAttribute(PASSWORD_DTO, passwordChangeDto);
    }

    @Test
    public void getDetailsUpdateForm_ShouldReturnProfileEditView_AndShowSessionMessage() {
        //arrange
        Mockito.when(mockRequest.getSession(false)).thenReturn(mockSession);
        Mockito.when(mockSession.getAttribute(DETAILS)).thenReturn(FILL_DETAILS);
        Mockito.when(mockPrincipal.getName()).thenReturn(user.getUserName());
        Mockito.when(mockAccountManager.getDetailsToEdit(user.getUserName()))
                .thenReturn(createDetailsDto);
        //act
        String actual = controller.getDetailsUpdateForm(mockModel, mockPrincipal, mockRequest);
        //assert
        Assertions.assertEquals(PROFILE_EDIT_VIEW, actual);
        Mockito.verify(mockModel, Mockito.times(1))
                .addAttribute(CREATE_USER_DTO, createDetailsDto);
        Mockito.verify(mockModel, Mockito.times(1))
                .addAttribute(PASSWORD_DTO, passwordChangeDto);
        Mockito.verify(mockModel, Mockito.times(1))
                .addAttribute(DETAILS, FILL_DETAILS);
    }

    @Test
    public void handleEditDetailsForm_ShouldReturnProfileEditView_OnBindingResultErrors() {
        //arrange
        Mockito.when(mockBindingResult.hasErrors()).thenReturn(true);
        //act
        String actual = controller.handleEditDetailsForm(createDetailsDto, mockBindingResult,
                mockModel, mockPrincipal, mockRequest);
        //assert
        Assertions.assertEquals(PROFILE_EDIT_VIEW, actual);
        Mockito.verify(mockModel, Mockito.times(1))
                .addAttribute(CREATE_USER_DTO, createDetailsDto);
        Mockito.verify(mockModel, Mockito.times(1))
                .addAttribute(PASSWORD_DTO, passwordChangeDto);

    }

    @Test
    public void handleEditDetailsForm_ShouldReturnProfileEditView_OnDuplicateError() {
        //arrange
        DuplicateEntityException e = new DuplicateEntityException("duplicate");
        Mockito.when(mockPrincipal.getName()).thenReturn(user.getUserName());
        Mockito.doThrow(e).when(mockAccountManager)
                .updateDetailsAndRecordHistory(createDetailsDto, user.getUserName());
        //act
        String actual = controller.handleEditDetailsForm(createDetailsDto, mockBindingResult,
                mockModel, mockPrincipal, mockRequest);
        //assert
        Assertions.assertEquals(PROFILE_EDIT_VIEW, actual);
        Mockito.verify(mockModel, Mockito.times(1))
                .addAttribute(CREATE_USER_DTO, createDetailsDto);
        Mockito.verify(mockModel, Mockito.times(1))
                .addAttribute(PASSWORD_DTO, passwordChangeDto);
        Mockito.verify(mockBindingResult, Mockito.times(1))
                .rejectValue(TELEPHONE, ERROR_ +
                        CREATE_USER_DTO, e.getMessage());
        Mockito.verify(mockModel, Mockito.times(1))
                .addAttribute(BINDING_RESULT_ERRORS, e.getMessage());

    }

    @Test
    public void handleEditDetailsForm_ShouldUpdateAndRedirect_WhenRedirectIsPresent() {
        //arrange
        Mockito.when(mockPrincipal.getName()).thenReturn(user.getUserName());
        Mockito.when(mockRequest.getSession(false)).thenReturn(mockSession);
        Mockito.when(mockSession.getAttribute(REDIRECT)).thenReturn(REDIRECT_POLICY_CREATE);
        //act
        String actual = controller.handleEditDetailsForm(createDetailsDto, mockBindingResult,
                mockModel, mockPrincipal, mockRequest);
        //assert
        Assertions.assertEquals(REDIRECT_POLICY_CREATE, actual);
        Mockito.verify(mockAccountManager, Mockito.times(1))
                .updateDetailsAndRecordHistory(createDetailsDto, user.getUserName());
    }

    @Test
    public void handleEditDetailsForm_ShouldUpdateAndReturnProfileEditPage_WhenRedirectIsNotPresent() {
        //arrange
        Mockito.when(mockPrincipal.getName()).thenReturn(user.getUserName());
        Mockito.when(mockRequest.getSession(false)).thenReturn(mockSession);
        Mockito.when(mockSession.getAttribute(REDIRECT)).thenReturn(null);
        //act
        String actual = controller.handleEditDetailsForm(createDetailsDto, mockBindingResult,
                mockModel, mockPrincipal, mockRequest);
        //assert
        Assertions.assertEquals(PROFILE_EDIT_VIEW, actual);
        Mockito.verify(mockAccountManager, Mockito.times(1))
                .updateDetailsAndRecordHistory(createDetailsDto, user.getUserName());
        Mockito.verify(mockModel, Mockito.times(1))
                .addAttribute(CREATE_USER_DTO, createDetailsDto);
        Mockito.verify(mockModel, Mockito.times(1))
                .addAttribute(PASSWORD_DTO, passwordChangeDto);
    }

    @Test
    public void showUserProfileById_ShouldShowProfilePage() {
        //arrange
        Mockito.when(mockPrincipal.getName()).thenReturn(user.getUserName());
        Mockito.when(mockUserService.userByEmail(user.getUserName())).thenReturn(user);
        user.setAuthorities(getAdminRoles(user));

        User notSameAsLogged = getEnabledUser("NotSameAsLogged");
        UserDetails canBeDifferentFromLogged = getUserDetails(notSameAsLogged);
        Mockito.when(mockUserService.getUserDetails(id)).thenReturn(canBeDifferentFromLogged);

        ShowUserDto dto = getShowUserDto(notSameAsLogged);
        Mockito.when(mockUserMapper.toDto(canBeDifferentFromLogged)).thenReturn(dto);
        //act
        String actual = controller.showUserProfileById(id,
                mockModel, mockPrincipal);
        //assert
        Assertions.assertEquals(USER_PROFILE_VIEW, actual);
        Mockito.verify(mockAuthorisationService, Mockito.times(1))
                .authorise(canBeDifferentFromLogged, user);
        Mockito.verify(mockModel, Mockito.times(1))
                .addAttribute(USER, dto);
        Mockito.verify(mockModel, Mockito.times(1))
                .addAttribute(USER_DETAILS, canBeDifferentFromLogged);

    }

    @Test
    public void showLoggedUserProfile_ShouldShowProfilePage() {
        //arrange
        Mockito.when(mockPrincipal.getName()).thenReturn(user.getUserName());
        Mockito.when(mockUserService.userByEmail(user.getUserName())).thenReturn(user);
        ShowUserDto dto = getShowUserDto(user);
        Mockito.when(mockUserMapper.toDto(user.getUserDetails()))
                .thenReturn(dto);
        //act
        String actual = controller.showLoggedUserProfile(mockModel, mockPrincipal);
        //assert
        Assertions.assertEquals(USER_PROFILE_VIEW, actual);
        Mockito.verify(mockModel, Mockito.times(1))
                .addAttribute(USER, dto);
        Mockito.verify(mockModel, Mockito.times(1))
                .addAttribute(USER_DETAILS, user.getUserDetails());

    }

    @Test
    public void getManagePolicyPage_ShouldReturnUpdatePage() {
        //arrange
        CreatePolicyDto dto = getCreatePolicyDtoWithCurrentStartDate(policy);

        Mockito.when(mockPrincipal.getName()).thenReturn(user.getUserName());
        Mockito.when(mockUserService.userByEmail(user.getUserName())).thenReturn(user);
        Mockito.when(mockPolicyService.getOne(id)).thenReturn(policy);
        //act
        String actual = controller.getManagePolicyPage(mockModel, mockPrincipal, id);
        //assert
        Assertions.assertEquals(POLICY_UPDATE_VIEW, actual);
        Mockito.verify(mockAuthorisationService, Mockito.times(1))
                .authorise(user, policy);
        Mockito.verify(mockModel, Mockito.times(1))
                .addAttribute(OFFER, policy.getOffer());
        Mockito.verify(mockModel, Mockito.times(1))
                .addAttribute(USER, policy.getOwner().getUserDetails());
        Mockito.verify(mockModel, Mockito.times(1))
                .addAttribute(POLICY_DTO, dto);
        Mockito.verify(mockModel, Mockito.times(1))
                .addAttribute(IMAGE_ID, policy.getImage().getId());
    }

    @Test
    public void handleManageUserPolicy_ShouldReturnUpdatePage_OnBindingResultErrors() {
        //arrange
        Mockito.when(mockPrincipal.getName())
                .thenReturn(user.getUserName());
        Mockito.when(mockUserService.userByEmail(user.getUserName()))
                .thenReturn(user);
        Mockito.when(mockPolicyService.getOne(createPolicyDto.getUpdatePolicyId()))
                .thenReturn(policy);
        Mockito.when(mockBindingResult.hasErrors()).thenReturn(true);
        //act
        String actual = controller.handleManageUserPolicy(mockModel, mockPrincipal,
                createPolicyDto, mockBindingResult);
        //assert
        Assertions.assertEquals(POLICY_UPDATE_VIEW, actual);
        Mockito.verify(mockAuthorisationService, Mockito.times(1))
                .authorise(user, policy);
        Mockito.verify(mockModel, Mockito.times(1))
                .addAttribute(OFFER, policy.getOffer());
        Mockito.verify(mockModel, Mockito.times(1))
                .addAttribute(USER, policy.getOwner().getUserDetails());
        Mockito.verify(mockModel, Mockito.times(1))
                .addAttribute(POLICY_DTO, createPolicyDto);
        Mockito.verify(mockModel, Mockito.times(1))
                .addAttribute(IMAGE_ID, policy.getImage().getId());

    }

    @Test
    public void handleManageUserPolicy_ShouldReturnUpdatePage() {
        //arrange
        Mockito.when(mockPrincipal.getName())
                .thenReturn(user.getUserName());
        Mockito.when(mockUserService.userByEmail(user.getUserName()))
                .thenReturn(user);
        Mockito.when(mockPolicyService.getOne(createPolicyDto.getUpdatePolicyId()))
                .thenReturn(policy);
        Image image = getAnyContentImage();
        Mockito.when(mockImageMapper.updateFromMultipart(policy.getImage(),
                createPolicyDto.getFile())).thenReturn(image);
        //act
        String actual = controller.handleManageUserPolicy(mockModel, mockPrincipal,
                createPolicyDto, mockBindingResult);
        //assert
        Assertions.assertEquals(POLICY_UPDATE_VIEW, actual);
        Mockito.verify(mockAuthorisationService, Mockito.times(1))
                .authorise(user, policy);
        Mockito.verify(mockPolicyManager, Mockito.times(1))
                .userUpdatePolicy(policy, user, image);
        Mockito.verify(mockModel, Mockito.times(1))
                .addAttribute(OFFER, policy.getOffer());
        Mockito.verify(mockModel, Mockito.times(1))
                .addAttribute(USER, policy.getOwner().getUserDetails());
        Mockito.verify(mockModel, Mockito.times(1))
                .addAttribute(POLICY_DTO, createPolicyDto);
        Mockito.verify(mockModel, Mockito.times(1))
                .addAttribute(IMAGE_ID, policy.getImage().getId());

    }

    private Set<Authority> getAdminRoles(User user) {
        Set<Authority> result = new HashSet<>();
        Authority authority = new Authority();
        authority.setUserName(user.getUserName());
        authority.setAuthority(UserRoles.ROLE_AGENT.name());
        result.add(authority);
        return result;
    }
}
