package com.safetycar.fast.controllers;

import com.safetycar.exceptions.EntityNotFoundException;
import com.safetycar.models.PasswordToken;
import com.safetycar.models.User;
import com.safetycar.services.contracts.*;
import com.safetycar.services.factories.contracts.TokenWrapperFactory;
import com.safetycar.web.controllers.mvc.MvcLoginController;
import com.safetycar.web.dto.user.ForgottenPasswordDto;
import com.safetycar.web.wrapper.TokenWrapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.WebAttributes;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import java.util.Locale;

import static com.safetycar.SafetyCarTestObjectsFactory.*;
import static com.safetycar.TestHelper.LOCAL_URL;
import static com.safetycar.util.Constants.ErrorsConstants.NO_USER_LIKE_THIS_WAS_FOUND;
import static com.safetycar.util.Constants.ValidationConstants.ERROR;
import static com.safetycar.util.Constants.Views.LOGIN_VIEW;
import static com.safetycar.util.Constants.Views.RESET_PASSWORD_VIEW;
import static com.safetycar.web.controllers.mvc.MvcLoginController.*;
import static com.safetycar.web.controllers.mvc.MvcUserController.PASSWORD_DTO;

@ExtendWith(MockitoExtension.class)
public class MvcLoginControllerTest {

    @InjectMocks
    private MvcLoginController controller;

    @Mock
    private AuthorisationService mockAuthorisationService;
    @Mock
    private HttpServletRequest mockHttpServletRequest;
    @Mock
    private HttpSession mockHttpSession;
    @Mock
    private PasswordTokenService mockPasswordTokenService;
    @Mock
    private PasswordChangingService mockPasswordChangingService;
    @Mock
    private Model mockModel;
    @Mock
    private BindingResult mockBindingResult;
    @Mock
    private UserService mockUserService;
    @Mock
    private MailService mockMailService;
    @Mock
    private TokenWrapperFactory mockTokenWrapperFactory;

    private User user;
    private PasswordToken token;
    private TokenWrapper tokenWrapper;
    private ForgottenPasswordDto dto;
    private final String email = "email";

    @BeforeEach
    void init() {
        user = getEnabledUser();
        token = getForgottenPassToken(user);
        tokenWrapper = getTokenWrapper(LOCAL_URL, token, Locale.ENGLISH);
        dto = getForgottenPassDto(token);
    }

    @Test
    public void showLoginPage_ShouldLoadPage() {
        //arrange
        //act
        String actual = controller.showLoginPage();
        //assert
        Assertions.assertEquals(LOGIN_VIEW, actual);
    }

    @Test
    public void showErrorPage_ShouldLoadPageWithError() {
        //arrange
        AuthenticationException e = new BadCredentialsException("message");
        Mockito.when(mockHttpServletRequest.getSession(false)).thenReturn(mockHttpSession);
        Mockito.when(mockHttpSession.getAttribute(WebAttributes.AUTHENTICATION_EXCEPTION))
                .thenReturn(e);
        //act
        String actual = controller.showError(mockModel, mockHttpServletRequest);
        //assert
        Assertions.assertEquals(LOGIN_VIEW, actual);
        Mockito.verify(mockModel, Mockito.times(1))
                .addAttribute(ERROR, e.getMessage());
    }

    @Test
    public void requestPasswordResetPublic_ShouldPerformTasks_WhenAllIsWell() {
        //arrange
        Mockito.when(mockUserService.userByEmail(email)).thenReturn(user);
        Mockito.when(mockPasswordTokenService.getNewUnsavedToken(user))
                .thenReturn(token);

        Mockito.when(mockHttpServletRequest.getRequestURL())
                .thenReturn(new StringBuffer(LOCAL_URL + REQUEST_PASS_RESET_ENDPOINT));
        Mockito.when(mockHttpServletRequest.getRequestURI())
                .thenReturn(REQUEST_PASS_RESET_ENDPOINT);
        Mockito.when(mockHttpServletRequest.getLocale()).thenReturn(Locale.ENGLISH);

        Mockito.when(mockTokenWrapperFactory.getTokenWrapper(LOCAL_URL, Locale.ENGLISH, token))
                .thenReturn(tokenWrapper);
        //act
        String actual = controller
                .requestPasswordResetPublic(mockModel, email, mockHttpServletRequest);
        //assert
        Assertions.assertEquals(LOGIN_VIEW, actual);

        Mockito.verify(mockPasswordTokenService, Mockito.times(1))
                .create(token);
        Mockito.verify(mockMailService, Mockito.times(1))
                .sendPasswordChangeRequest(tokenWrapper);
    }

    @Test
    public void requestPasswordResetPublic_ShouldPerformTasks_WhenUserAdmin() {
        //arrange
        Mockito.when(mockUserService.userByEmail(email)).thenReturn(user);

        AccessDeniedException e = new AccessDeniedException(NO_USER_LIKE_THIS_WAS_FOUND);
        Mockito.doThrow(e).when(mockAuthorisationService)
                .authoriseNotAdmin(user, NO_USER_LIKE_THIS_WAS_FOUND);
        //act
        String actual = controller
                .requestPasswordResetPublic(mockModel, email, mockHttpServletRequest);
        //assert
        Mockito.verify(mockModel, Mockito.times(1))
                .addAttribute(ERROR, e.getMessage());
        Assertions.assertEquals(LOGIN_VIEW, actual);
    }

    @Test
    public void requestPasswordResetPublic_ShouldPerformTasks_WhenNoUserWithThisEmailFound() {
        //arrange
        EntityNotFoundException e = new EntityNotFoundException(NO_USER_LIKE_THIS_WAS_FOUND);
        Mockito.doThrow(e).when(mockUserService).userByEmail(email);
        //act
        String actual = controller
                .requestPasswordResetPublic(mockModel, email, mockHttpServletRequest);
        //assert
        Mockito.verify(mockModel, Mockito.times(1))
                .addAttribute(ERROR, e.getMessage());
        Assertions.assertEquals(LOGIN_VIEW, actual);
    }

    @Test
    public void verifyPasswordResetToken_ShouldPerformTasks_WhenTokenValid() {
        //arrange
        ForgottenPasswordDto dto = new ForgottenPasswordDto();
        dto.setToken(token.getToken());
        //act
        String actual = controller
                .verifyPasswordResetToken(token.getToken(), mockModel);
        //assert
        Mockito.verify(mockModel, Mockito.times(1))
                .addAttribute(PASSWORD_DTO, dto);
        Assertions.assertEquals(RESET_PASSWORD_VIEW, actual);
    }

    @Test
    public void verifyPasswordResetToken_ShouldThrow_WhenTokenInvalid() {
        //arrange
        EntityNotFoundException e = new EntityNotFoundException(NO_USER_LIKE_THIS_WAS_FOUND);
        Mockito.doThrow(e).when(mockPasswordTokenService)
                .validateToken(token.getToken());
        //act
        //assert
        Assertions.assertThrows(e.getClass(),
                () -> controller.verifyPasswordResetToken(token.getToken(), mockModel), e.getMessage());
    }

    @Test
    public void handleResetPasswordPublic_ShouldPerformTasks_WhenAllIsWell() {
        //arrange
        Mockito.when(mockBindingResult.hasErrors()).thenReturn(false);
        Mockito.when(mockPasswordTokenService
                .findForgottenPasswordToken(dto.getToken())).thenReturn(token);
        //act
        String actual = controller.handleResetPasswordPublic(mockModel, dto, mockBindingResult);
        //assert
        Assertions.assertEquals(REDIRECT_LOGIN, actual);
        Mockito.verify(mockPasswordChangingService, Mockito.times(1))
                .changeForgottenPassword(user, dto.getPassword());
        Mockito.verify(mockPasswordTokenService, Mockito.times(1))
                .delete(token);
    }

    @Test
    public void handleResetPasswordPublic_ReturnPasswordResetView_OnBindingResultErrors() {
        //arrange
        Mockito.when(mockBindingResult.hasErrors()).thenReturn(true);
        //act
        String actual = controller.handleResetPasswordPublic(mockModel, dto, mockBindingResult);
        //assert
        Assertions.assertEquals(RESET_PASSWORD_VIEW, actual);
        Mockito.verify(mockModel, Mockito.times(1))
                .addAttribute(PASSWORD_DTO, dto);
    }


}
