package com.safetycar.fast.repositories;

import static com.safetycar.SafetyCarTestObjectsFactory.getEnabledUser;
import static com.safetycar.SafetyCarTestObjectsFactory.getOffer;
import static com.safetycar.SafetyCarTestObjectsFactory.getPolicy;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import com.safetycar.models.Offer;
import com.safetycar.models.Policy;
import com.safetycar.models.User;
import com.safetycar.repositories.impl.OfferExistsImpl;

@ExtendWith(MockitoExtension.class)
public class OfferExistsImplTest {

	@InjectMocks
	private OfferExistsImpl offerExists;

	private User user;
	private Offer sameCarOffer;
	private Policy sameCarPolicy;

	@BeforeEach
	void init() {
		user = getEnabledUser();
		sameCarOffer = getOffer();
		sameCarPolicy = getPolicy(user, sameCarOffer);
	}

	@Test
	public void exists_ShouldReturnFalse_WhenOfferNull() {
		// arrange
		sameCarOffer = null;
		// act
		boolean actual = offerExists.exists(sameCarOffer, user);
		// assert
		Assertions.assertFalse(actual);
	}

	@Test
	public void exists_ShouldReturnFalse_WhenOfferUnique() {
		// arrange
		// act
		boolean actual = offerExists.exists(sameCarOffer, user);
		// assert
		Assertions.assertFalse(actual);
	}

	@Test
	public void exists_ShouldReturnTrue_WhenCarNotUnique_ByOffer() {
		// arrange
		user.addOffer(sameCarOffer);
		// act
		boolean actual = offerExists.exists(sameCarOffer, user);
		// assert
		Assertions.assertTrue(actual);
	}

	@Test
	public void exists_ShouldReturnTrue_WhenCarNotUnique_ByPolicy() {
		// arrange
		user.addPolicy(sameCarPolicy);
		// act
		boolean actual = offerExists.exists(sameCarOffer, user);
		// assert
		Assertions.assertTrue(actual);
	}

}
