//package com.safetycar.fast.repositories;
//
//import com.safetycar.SafetyCarTestObjectsFactory;
//import com.safetycar.models.User;
//import com.safetycar.models.UserDetails;
//import com.safetycar.models.wrappers.UserDuplicateWrapper;
//import com.safetycar.repositories.impl.UserExistsImpl;
//import org.junit.Ignore;
//import org.junit.jupiter.api.Assertions;
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.Test;
//import org.junit.jupiter.api.extension.ExtendWith;
//import org.mockito.InjectMocks;
//import org.mockito.Mock;
//import org.mockito.Mockito;
//import org.mockito.junit.jupiter.MockitoExtension;
//
//import javax.persistence.EntityManager;
//import javax.persistence.TypedQuery;
//import javax.persistence.criteria.*;
//
//import java.util.Collections;
//import java.util.LinkedList;
//import java.util.List;
//
//import static com.safetycar.SafetyCarTestObjectsFactory.getEnabledUser;
//import static com.safetycar.SafetyCarTestObjectsFactory.getUserDuplicateWrapper;
//import static com.safetycar.repositories.filter.UserSpec.USERNAME;
//import static com.safetycar.util.Constants.QueryConstants.*;
//import static com.safetycar.fast.wrappers.UserDuplicateWrapperTest.ANY_STRING;
//@ExtendWith(MockitoExtension.class)
//public class UserExistsImplTests {
//
//    @InjectMocks
//    private UserExistsImpl userExists;
//    @Mock
//    private EntityManager mockEntityManager;
//    @Mock
//    private CriteriaBuilder mockCriteriaBuilder;
//    @Mock
//    private CriteriaQuery<UserDuplicateWrapper> mockCriteriaQuery;
//    @Mock
//    private TypedQuery<UserDuplicateWrapper> mockTypedQuery;
//    @Mock
//    private Root<UserDetails> mockRoot;
//    @Mock
//    private Path<Object> mockPath;
//    @Mock
//    private Predicate mockPredicate;
//
//    private Class<UserDuplicateWrapper> wrapperClass = UserDuplicateWrapper.class;
//    private Class<UserDetails> entityClass = UserDetails.class;
//    private User user;
//    private UserDetails userDetails;
//    private List<UserDuplicateWrapper> userDuplicateWrappers;
//
//    @BeforeEach
//    void init() {
//        user = SafetyCarTestObjectsFactory.getEnabledUser();
//        userDetails = user.getUserDetails();
//    }
//
//    @Test
//    public void existsByUserDetails_ShouldReturnFalse_WhenResultListEmpty() {
//        //arrange
//        userDuplicateWrappers = Collections.emptyList();
//
//        prepareMocks(userDuplicateWrappers);
//        //act
//        boolean actual = userExists.existsByUserDetails(this.userDetails);
//        //assert
//        Assertions.assertFalse(actual);
//    }
//
//    @Test
//    public void existsByUserDetails_ShouldReturnTrue_WhenDuplicate() {
//        //arrange
//        UserDuplicateWrapper duplicate = getUserDuplicateWrapper(userDetails);
//        userDuplicateWrappers = Collections.singletonList(duplicate);
//
//        prepareMocks(userDuplicateWrappers);
//        //act
//        boolean actual = userExists.existsByUserDetails(userDetails);
//        //assert
//        Assertions.assertTrue(actual);
//    }
//
//    @Test
//    public void existsByUserDetails_ShouldReturnFalse_WhenUnique() {
//        //arrange
//        UserDetails uniqueDetails = getUnique();
//        UserDuplicateWrapper notDuplicate = getUserDuplicateWrapper(uniqueDetails);
//        userDuplicateWrappers = Collections.singletonList(notDuplicate);
//
//        prepareMocks(userDuplicateWrappers);
//        //act
//        boolean actual = userExists.existsByUserDetails(this.userDetails);
//        //assert
//        Assertions.assertFalse(actual);
//    }
//
//    private UserDetails getUnique() {
//        UserDetails uniqueDetails = new UserDetails();
//        uniqueDetails.setLastName(ANY_STRING);
//        uniqueDetails.setTelephone(ANY_STRING);
//        uniqueDetails.setAddress(ANY_STRING);
//        uniqueDetails.setFirstName(ANY_STRING);
//        return uniqueDetails;
//    }
//
//    private void prepareMocks(List<UserDuplicateWrapper> userDuplicateWrappers) {
//        Mockito.when(mockEntityManager.getCriteriaBuilder())
//                .thenReturn(mockCriteriaBuilder);
//        Mockito.when(mockCriteriaBuilder.createQuery(wrapperClass))
//                .thenReturn(mockCriteriaQuery);
//        Mockito.when(mockCriteriaQuery.from(entityClass))
//                .thenReturn(mockRoot);
//
//        Mockito.when(mockRoot.get(USERNAME)).thenReturn(mockPath);
//        Mockito.when(mockRoot.get(TELEPHONE)).thenReturn(mockPath);
//        Mockito.when(mockRoot.get(FIRST_NAME)).thenReturn(mockPath);
//        Mockito.when(mockRoot.get(LAST_NAME)).thenReturn(mockPath);
//        Mockito.when(mockRoot.get(ADDRESS)).thenReturn(mockPath);
//        List<Path<Object>> paths = addPaths();
//
//        Mockito.when(mockCriteriaBuilder.notEqual(mockPath, user.getUserName()))
//                .thenReturn(mockPredicate);
//
//        Mockito.when(mockCriteriaQuery.multiselect(paths.toArray(Path[]::new)))
//                .thenReturn(mockCriteriaQuery);
//        Mockito.when(mockCriteriaQuery.where(mockPredicate))
//                .thenReturn(mockCriteriaQuery);
//
//        Mockito.when(mockEntityManager.createQuery(mockCriteriaQuery))
//                .thenReturn(mockTypedQuery);
//        Mockito.when(mockTypedQuery.getResultList())
//                .thenReturn(userDuplicateWrappers);
//    }
//
//    private List<Path<Object>> addPaths() {
//        List<Path<Object>> paths = new LinkedList<>();
//        paths.add(mockPath);
//        paths.add(mockPath);
//        paths.add(mockPath);
//        paths.add(mockPath);
//        paths.add(mockPath);
//        return paths;
//    }
//
//}
