package com.safetycar.fast.services;

import com.safetycar.models.History;
import com.safetycar.models.Offer;
import com.safetycar.models.Policy;
import com.safetycar.models.User;
import com.safetycar.services.PolicyHistoryServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.junit.jupiter.MockitoExtension;

import static com.safetycar.SafetyCarTestObjectsFactory.*;

@RunWith(MockitoJUnitRunner.class)
@ExtendWith(MockitoExtension.class)
public class PolicyUserHistoryService {

    @InjectMocks
    private PolicyHistoryServiceImpl historyService;

    @Test
    public void appendHistory_ShouldAppend() {
        //arrange
        User testUser = getEnabledUser();
        Offer testOffer = getOffer();
        String testAction = "testAction";
        String testHistory = "testHistory";

        History expected = new History();
        expected.setActor(testUser.getUserDetails());
        expected.setHistory(testHistory);
        expected.setAction(testAction);
        Policy policy = getPolicy(testUser, testOffer);
        //act
        historyService.appendHistory(testUser, policy, testAction, testHistory);
        History actual = policy.getHistories().stream().findAny().get();
        //assert
        Assertions.assertEquals(1, policy.getHistories().size());
        Assertions.assertEquals(expected, actual);

    }

}
