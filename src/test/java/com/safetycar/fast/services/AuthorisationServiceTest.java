package com.safetycar.fast.services;

import com.safetycar.enums.UserRoles;
import com.safetycar.models.Authority;
import com.safetycar.models.Offer;
import com.safetycar.models.Policy;
import com.safetycar.models.User;
import com.safetycar.services.AuthorisationServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.access.AccessDeniedException;

import java.util.HashSet;
import java.util.Set;

import static com.safetycar.SafetyCarTestObjectsFactory.*;

@RunWith(MockitoJUnitRunner.class)
@ExtendWith(MockitoExtension.class)
public class AuthorisationServiceTest {

    @InjectMocks
    private AuthorisationServiceImpl authorisationService;
    private User testUser;
    private User testAgent;
    private Policy testPolicy;
    private Offer testOffer;

    @BeforeEach
    void init() {
        testAgent = getEnabledUser();
        testAgent.setUserName("agent");
        testAgent.getUserDetails().setUserName("agent");
        testAgent.setAuthorities(getAdminRoles(testAgent));

        testUser = getEnabledUser();

        testOffer = getOffer();
        testUser.addOffer(testOffer);

        testPolicy = getPolicy(testUser, testOffer);
        testUser.addPolicy(testPolicy);
    }

    @Test
    public void authorise_ShouldThrow_WhenNotAdmin_AndNotOfferOwner() {
        //arrange
        User actual = getEnabledUser("actual");
        //act, assert
        Assertions.assertThrows(AccessDeniedException.class,
                () -> authorisationService.authorise(actual, testOffer));
    }

    @Test
    public void authorise_ShouldNotThrow_WhenNotAdmin_ButOfferOwner() {
        //arrange
        //act, assert
        Assertions.assertDoesNotThrow(
                () -> authorisationService.authorise(testUser, testOffer));
    }

    @Test
    public void authorise_ShouldNotThrow_WhenAdmin_ButNotOfferOwner() {
        //arrange, act, assert
        Assertions.assertDoesNotThrow(
                () -> authorisationService.authorise(testAgent, testOffer));
    }

    @Test
    public void authorise_ShouldThrow_WhenNotAdmin_AndNotPolicyOwner() {
        //arrange
        User actual = getEnabledUser("actual");
        //act, assert
        Assertions.assertThrows(AccessDeniedException.class,
                () -> authorisationService.authorise(actual, testPolicy));
    }

    @Test
    public void authorise_ShouldNotThrow_WhenAdmin_ButNotPolicyOwner() {
        //arrange, act, assert
        Assertions.assertDoesNotThrow(
                () -> authorisationService.authorise(testAgent, testPolicy));
    }

    @Test
    public void authorise_ShouldNotThrow_WhenNotAdmin_ButPolicyOwner() {
        //arrange, act, assert
        Assertions.assertDoesNotThrow(
                () -> authorisationService.authorise(testUser, testPolicy));
    }

    @Test
    public void authorise_ShouldNotThrow_WhenAdmin() {
        //arrange, act, assert
        Assertions.assertDoesNotThrow(
                () -> authorisationService.authorise(testUser.getUserDetails(), testAgent));
    }

    @Test
    public void authorise_ShouldThrow_WhenNotAdmin() {
        //arrange
        User actual = getEnabledUser("actual");
        //act, assert
        Assertions.assertThrows(AccessDeniedException.class,
                () -> authorisationService.authorise(testUser.getUserDetails(), actual));
    }

    @Test
    public void authoriseNotAdmin_ShouldThrow_WhenAdmin() {
        //arrange
        String message = "message";
        //act
        //assert
        Assertions.assertThrows(AccessDeniedException.class,
                () -> authorisationService.authoriseNotAdmin(testAgent, message), message);
    }

    @Test
    public void authoriseNotAdmin_ShouldNotThrow_WhenNotAdmin() {
        //arrange
        String message = "message";
        //act
        //assert
        Assertions.assertDoesNotThrow(
                () -> authorisationService.authoriseNotAdmin(testUser, message));
    }
    private Set<Authority> getAdminRoles(User user) {
        Set<Authority> result = new HashSet<>();
        Authority authority = new Authority();
        authority.setUserName(user.getUserName());
        authority.setAuthority(UserRoles.ROLE_AGENT.name());
        result.add(authority);
        return result;
    }

}
