package com.safetycar.fast.services.mappers;

import com.safetycar.models.Car;
import com.safetycar.models.Model;
import com.safetycar.models.Offer;
import com.safetycar.services.contracts.ModelService;
import com.safetycar.web.dto.mappers.CarMapperImpl;
import com.safetycar.web.dto.offer.CreateOfferDtoForRest;
import com.safetycar.web.dto.offer.OfferDto;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.junit.jupiter.MockitoExtension;

import static com.safetycar.SafetyCarTestObjectsFactory.*;

@ExtendWith(MockitoExtension.class)
@RunWith(MockitoJUnitRunner.class)
public class CarMapperTest {

    @InjectMocks
    private CarMapperImpl carMapper;

    @Mock
    private ModelService mockModelService;

    private Car car;
    private Model model;
    private OfferDto offerDto;
    private CreateOfferDtoForRest createOfferDtoForRest;

    @BeforeEach
    void init() {
        Offer offer = getOffer();
        car = offer.getCar();
        model = car.getModelYearBrand();
        offerDto = getOfferDto(offer, true, true);
        createOfferDtoForRest = getCreateOfferDtoForRest(offer, true, true);

    }

    @Test
    public void fromDto_OfferDtoParam_ShouldReturnCar() {
        //arrange
        Mockito.when(mockModelService.getOne(Mockito.anyInt())).thenReturn(model);
        //act
        Car actual = carMapper.fromDto(offerDto);
        //assert
        Assertions.assertEquals(car, actual);
    }

    @Test
    public void fromDto_CreateOfferDtoForRestParam_ShouldReturnCar() {
        //arrange
        Mockito.when(mockModelService.getOne(Mockito.anyInt())).thenReturn(model);
        //act
        Car actual = carMapper.fromDto(createOfferDtoForRest);
        //assert
        Assertions.assertEquals(car, actual);
    }

}
