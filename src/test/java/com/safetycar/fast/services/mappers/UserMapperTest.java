package com.safetycar.fast.services.mappers;

import com.safetycar.models.User;
import com.safetycar.models.UserDetails;
import com.safetycar.services.contracts.UserAuthorityService;
import com.safetycar.web.dto.mappers.UserMapperImpl;
import com.safetycar.web.dto.user.CreateDetailsDto;
import com.safetycar.web.dto.user.CreateUserDto;
import com.safetycar.web.dto.user.RestShowUserDto;
import com.safetycar.web.dto.user.ShowUserDto;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;

import static com.safetycar.SafetyCarTestObjectsFactory.*;

@ExtendWith(MockitoExtension.class)
@RunWith(MockitoJUnitRunner.class)
public class UserMapperTest {

    @InjectMocks
    private UserMapperImpl userMapper;

    @Mock
    private PasswordEncoder mockPasswordEncoder;
    @Mock
    private UserAuthorityService mockUserAuthorityService;

    private User enabled;
    private User disabled;
    private ShowUserDto testShowDto;
    private RestShowUserDto testRestShowUserDto;
    private CreateDetailsDto testCreateDetailsDto;
    private CreateUserDto testCreateUserDto;

    @BeforeEach
    void init() {
        enabled = getEnabledUser();
        disabled = getDisabledUser();
        testShowDto = getShowUserDto(enabled);
        testCreateUserDto = getCreateUserDto(enabled);
        testShowDto = getShowUserDto(enabled);
        testCreateDetailsDto = getCreateDetailsDto(enabled);
        testRestShowUserDto = getShowUserRestDto(enabled);
    }

    @Test
    public void fromDto_withCreateUserDto_ShouldReturn_User() {
        //arrange
        Mockito.when(mockPasswordEncoder.encode(Mockito.anyString())).thenReturn(disabled.getPassword());

        prepareExpectedNewUser();
        //act
        User actual = userMapper.fromDto(testCreateUserDto);
        //assert
        Assertions.assertEquals(disabled, actual);
        Mockito.verify(mockUserAuthorityService, Mockito
                .times(1)).assignUserAuthority(disabled);
    }

    private void prepareExpectedNewUser() {
        UserDetails expected = new UserDetails();
        expected.setUserName(disabled.getUserName());
        disabled.setUserDetails(expected);
        disabled.setAuthorities(null);
    }

    @Test
    public void fromDto_withCreateDetailsDto_ShouldReturn_UserDetails() {
        //arrange, act
        UserDetails actual = userMapper.updateFromDto(testCreateDetailsDto, enabled.getUserDetails());
        //assert
        Assertions.assertEquals(enabled.getUserDetails(), actual);

    }

    @Test
    public void toDto_WithUser_ShouldReturn_CreateDetailsDto() {
        //arrange, act
        CreateDetailsDto actual = userMapper.toDto(enabled);
        //assert
        Assertions.assertEquals(testCreateDetailsDto, actual);
    }

    @Test
    public void toDto_WithUserDetails_ShouldReturn_ShowUserDto() {
        //arrange, act
        ShowUserDto actual = userMapper.toDto(enabled.getUserDetails());
        //assert
        Assertions.assertEquals(testShowDto, actual);
    }

    @Test
    public void toDto_WithUserDetailsIterable_ShouldReturn_ShowUserDtoCollection() {
        //arrange, act
        Collection<ShowUserDto> expected = new LinkedList<>(Collections.singleton(testShowDto));
        Collection<ShowUserDto> actual = userMapper.toDto(Collections.singleton(enabled.getUserDetails()));
        //assert
        Assertions.assertEquals(expected, actual);
    }

    @Test
    public void toRestShowDto_ShouldReturn_ShowUserDto() {
        //arrange, act
        RestShowUserDto actual = userMapper.toRestShowDto(enabled.getUserDetails());
        //assert
        Assertions.assertEquals(testRestShowUserDto, actual);
    }

    @Test
    public void toRestShowDto_WithUserDetailsIterable_ShouldReturn_ShowUserDtoCollection() {
        //arrange, act
        Collection<RestShowUserDto> expected = new LinkedList<>(Collections.singleton(testRestShowUserDto));
        Collection<RestShowUserDto> actual = userMapper.toRestShowDto(Collections.singleton(enabled.getUserDetails()));
        //assert
        Assertions.assertEquals(expected, actual);
    }

}
