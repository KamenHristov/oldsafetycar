package com.safetycar.fast.services;

import com.safetycar.models.Coefficient;
import com.safetycar.models.Offer;
import com.safetycar.models.User;
import com.safetycar.models.wrappers.CalculationWrapper;
import com.safetycar.repositories.OfferRepository;
import com.safetycar.exceptions.EntityNotFoundException;
import com.safetycar.repositories.filter.OfferSpec;
import com.safetycar.services.OfferServiceImpl;
import com.safetycar.services.factories.contracts.OfferSpecFactory;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Sort;

import java.math.BigDecimal;
import java.util.*;

import static com.safetycar.SafetyCarTestObjectsFactory.*;
import static com.safetycar.repositories.filter.base.BaseMapSpec.SORT_PARAMETER;
import static com.safetycar.util.Constants.QueryConstants.*;

@ExtendWith(MockitoExtension.class)
public class OfferServiceTest {

    @InjectMocks
    private OfferServiceImpl offerService;

    @Mock
    private OfferRepository mockOfferRepository;

    @Mock
    private OfferSpecFactory mockOfferSpecFactory;

    private Offer testOffer;
    private Coefficient testCoeff;
    private User testUser;
    private List<Offer> testOffers;

    @BeforeEach
    public void init() {
        testOffer = getOffer();
        testOffers = Collections.singletonList(testOffer);
        testCoeff = getCoefficient();
        testUser = getEnabledUser();
    }

    @Test
    public void get_ShouldReturn_WhenEntityExists() {
        //arrange
        Mockito.when(mockOfferRepository.findById(Mockito.anyInt()))
                .thenReturn(Optional.of(testOffer));
        //act
        Offer actual = offerService.getOne(Mockito.anyInt());
        //assert
        Assertions.assertEquals(actual, testOffer);
    }

    @Test
    public void getAll_ShouldReturnCollection() {
        //arrange
        OfferSpec spec = new OfferSpec(new HashMap<>());
        spec.addSpec(SORT_PARAMETER, ID);
        Sort sort = spec.getSort();

        Mockito.when(mockOfferSpecFactory.getSpec()).thenReturn(spec);
        Mockito.when(mockOfferRepository.findAll(spec, sort))
                .thenReturn(testOffers);
        //act
        Collection<Offer> actual = offerService.getAll();
        //assert
        Assertions.assertEquals(testOffers, actual);
    }

    @Test
    public void getMyOffers_ShouldReturnCollection() {
        //arrange
        testUser.addOffer(testOffer);
        Collection<Offer> offers = testUser.getOffers();
        OfferSpec spec = new OfferSpec(offers);
        Mockito.when(mockOfferSpecFactory.getSpec(offers)).thenReturn(spec);
        Mockito.when(mockOfferRepository.findAll(spec))
                .thenReturn(testOffers);
        //act
        List<Offer> actual = new LinkedList<>(offerService.getMyOffers(testUser));
        //assert
        Assertions.assertEquals(testOffers, actual);
    }

    @Test
    public void searchMyOffers_ShouldReturnCollection() {
        //arrange
        testUser.addOffer(testOffer);
        Map<String, String> filter = new HashMap<>();
        OfferSpec spec = new OfferSpec(testUser.getOffers(), filter);
        Sort sort = spec.getSort();
        Mockito.when(mockOfferSpecFactory.getSpec(testUser.getOffers(), filter)).thenReturn(spec);
        Mockito.when(mockOfferRepository.findAll(spec, sort))
                .thenReturn(testOffers);
        //act
        List<Offer> actual = new LinkedList<>(offerService.searchMyOffers(testUser, filter));
        //assert
        Assertions.assertEquals(testOffers, actual);
    }


    @Test
    public void get_ShouldThrow_WhenEntityDoesNotExist() {
        //arrange
        Mockito.when(mockOfferRepository.findById(Mockito.anyInt()))
                .thenReturn(Optional.empty());
        //act, assert
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> offerService.getOne(Mockito.anyInt()));
    }

    @Test
    public void calculatePremium_ShouldCalculateCorrectly_WhenAllFlagsTrue() {
        //arrange
        BigDecimal samePremium = BigDecimal.TEN;
        Offer allTrueOffer = getOffer(true);
        allTrueOffer.setPremium(samePremium);

        BigDecimal baseAmount = BigDecimal.TEN;
        CalculationWrapper allTrueWrapper = getWrapper(baseAmount, allTrueOffer);
        //act
        BigDecimal allTruePremiumActual = offerService.calculatePremium(allTrueWrapper);

        BigDecimal allTruePremiumExpected = expected(allTrueWrapper);
        //assert

        Assertions.assertEquals(allTruePremiumExpected, allTruePremiumActual);

    }

    @Test
    public void calculatePremium_ShouldCalculateCorrectly_WhenAllFlagsFalse() {
        //arrange
        Offer allFalseOffer = getOffer(false);
        BigDecimal samePremium = BigDecimal.TEN;
        allFalseOffer.setPremium(samePremium);

        BigDecimal baseAmount = BigDecimal.TEN;
        CalculationWrapper allFalseWrapper = getWrapper(baseAmount, allFalseOffer);
        //act
        BigDecimal allFalsePremiumActual = offerService.calculatePremium(allFalseWrapper);

        BigDecimal allFalsePremiumExpected = expected(allFalseWrapper);
        //assert

        Assertions.assertEquals(allFalsePremiumExpected, allFalsePremiumActual);
    }

    @Test
    public void calculatePremium_ShouldCalculateCorrectly_WhenHadAccidentsTrue_AboveTwentyFiveFalse() {
        //arrange
        Offer accidentsBelowTwentyFiveOffer = getOffer(true);
        BigDecimal samePremium = BigDecimal.TEN;
        accidentsBelowTwentyFiveOffer.setPremium(samePremium);
        accidentsBelowTwentyFiveOffer.setAboveTwentyFive(false);

        BigDecimal baseAmount = BigDecimal.TEN;
        CalculationWrapper accidentsBelowTwentyFiveWrapper =
                getWrapper(baseAmount, accidentsBelowTwentyFiveOffer);
        //act
        BigDecimal accidentsBelowTwentyFivePremiumActual =
                offerService.calculatePremium(accidentsBelowTwentyFiveWrapper);

        BigDecimal accidentsBelowTwentyFivePremiumExpected =
                expected(accidentsBelowTwentyFiveWrapper);
        //assert

        Assertions.assertEquals(accidentsBelowTwentyFivePremiumExpected,
                accidentsBelowTwentyFivePremiumActual);

    }

    @Test
    public void calculatePremium_ShouldCalculateCorrectly_WhenHadAccidentsFalse_AboveTwentyFiveTrue() {
        //arrange
        Offer noAccidentsAboveTwentyFiveOffer = getOffer();
        BigDecimal samePremium = BigDecimal.TEN;
        noAccidentsAboveTwentyFiveOffer.setPremium(samePremium);

        BigDecimal baseAmount = BigDecimal.TEN;
        CalculationWrapper noAccidentsAboveTwentyFiveWrapper =
                getWrapper(baseAmount, noAccidentsAboveTwentyFiveOffer);
        //act
        BigDecimal noAccidentsAboveTwentyFivePremiumActual =
                offerService.calculatePremium(noAccidentsAboveTwentyFiveWrapper);

        BigDecimal noAccidentsAboveTwentyFivePremiumExpected =
                expected(noAccidentsAboveTwentyFiveWrapper);
        //assert

        Assertions.assertEquals(noAccidentsAboveTwentyFivePremiumExpected,
                noAccidentsAboveTwentyFivePremiumActual);

    }

    private CalculationWrapper getWrapper(BigDecimal baseAmount, Offer allTrue) {
        return new CalculationWrapper(baseAmount, allTrue.hadAccidents(), allTrue.isAboveTwentyFive(), testCoeff);
    }

    private BigDecimal expected(CalculationWrapper wrapper) {
        BigDecimal baseAmount = wrapper.getBaseAmount();
        if (baseAmount == null) return BigDecimal.ZERO;

        BigDecimal ageRisk = BigDecimal.valueOf(testCoeff.getAgeRisk());
        BigDecimal accidentRisk = BigDecimal.valueOf(testCoeff.getAccidentRisk());

        BigDecimal taxCoeff = BigDecimal.valueOf(testCoeff.getTax());
        BigDecimal ageCoeff = baseAmount.multiply(ageRisk);
        BigDecimal accidentCoeff = baseAmount.multiply(accidentRisk);


        if (wrapper.hadAccidents()) {
            baseAmount = baseAmount.add(accidentCoeff);
        }
        if (wrapper.isAboveTwentyFive()) {
            baseAmount = baseAmount.add(ageCoeff);
        }

        baseAmount = baseAmount.add(taxCoeff);
        return baseAmount;
    }

}
