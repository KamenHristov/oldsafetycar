package com.safetycar.fast.services;

import com.safetycar.SafetyCarTestObjectsFactory;
import com.safetycar.models.VerificationToken;
import com.safetycar.models.User;
import com.safetycar.repositories.AuthorityRepository;
import com.safetycar.repositories.VerificationTokenRepository;
import com.safetycar.repositories.UserRepository;
import com.safetycar.services.ExpiredTokenDisposalImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.junit.jupiter.MockitoExtension;

import static com.safetycar.SafetyCarTestObjectsFactory.getToken;
import static com.safetycar.SafetyCarTestObjectsFactory.getEnabledUser;

@RunWith(MockitoJUnitRunner.class)
@ExtendWith(MockitoExtension.class)
public class ExpiredTokenDisposalTest {

    @InjectMocks
    private ExpiredTokenDisposalImpl tokenDisposal;

    @Mock
    private VerificationTokenRepository mockVerificationTokenRepository;
    @Mock
    private UserRepository mockUserRepository;
    @Mock
    private AuthorityRepository mockAuthorityRepository;

    private User testUser;
    private VerificationToken testToken;

    @BeforeEach
    void init() {
        testUser = SafetyCarTestObjectsFactory.getEnabledUser();
        testToken = getToken(testUser);
    }

    @Test
    public void clearExpired_ShouldDeleteTokenAndNotUser_WhenUserIsActive() {
        //arrange, act
        tokenDisposal.clearExpiredAndDeleteInactiveUser(testToken);
        //assert
        Mockito.verify(mockVerificationTokenRepository, Mockito.times(1)).delete(testToken);
    }

    @Test
    public void clearExpired_ShouldDeleteTokenAndUser_WhenUserNotActive() {
        //arrange, act
        testUser.setEnabled(false);
        tokenDisposal.clearExpiredAndDeleteInactiveUser(testToken);
        //assert
        Mockito.verify(mockVerificationTokenRepository, Mockito.times(1))
                .delete(testToken);
        Mockito.verify(mockUserRepository, Mockito.times(1))
                .delete(testUser);
        Mockito.verify(mockAuthorityRepository, Mockito.times(1))
                .deleteAuthority(testUser.getUserName());
    }

}
