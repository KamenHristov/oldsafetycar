package com.safetycar.fast.services;

import com.safetycar.SafetyCarTestObjectsFactory;
import com.safetycar.models.User;
import com.safetycar.security.CustomAuthenticationProvider;
import com.safetycar.services.contracts.UserService;
import com.safetycar.exceptions.AccountNotActivatedException;
import com.safetycar.exceptions.EntityNotFoundException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.context.MessageSource;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.LinkedList;
import java.util.List;
import java.util.Locale;

import static com.safetycar.SafetyCarTestObjectsFactory.getSpringUser;
import static com.safetycar.SafetyCarTestObjectsFactory.getEnabledUser;
import static com.safetycar.security.CustomAuthenticationProvider.MESSAGE_INVALID_USER;
import static com.safetycar.security.MySpringUserDetailsService.getAuthorities;

@ExtendWith(MockitoExtension.class)
public class CustomAuthenticationProviderTest {

    @InjectMocks
    private CustomAuthenticationProvider authenticationProvider;

    @Mock
    private UserService mockUserService;
    @Mock
    private UserDetailsService mockDetailsService;
    @Mock
    private PasswordEncoder passwordEncoder;
    @Mock
    private MessageSource messageSource;

    private Authentication testAuth;
    private User testUser;
    private UserDetails testSpringUser;

    @BeforeEach
    void init() {
        authenticationProvider.setUserDetailsService(mockDetailsService);
        authenticationProvider.setPasswordEncoder(passwordEncoder);
        testUser = SafetyCarTestObjectsFactory.getEnabledUser();
        List<GrantedAuthority> authorities = new LinkedList<>(getAuthorities(testUser.getAuthorities()));
        testAuth = new UsernamePasswordAuthenticationToken(testUser, testUser.getPassword(), authorities);
        testSpringUser = getSpringUser(testUser);
    }

    @Test
    public void throw_AppropriateException_whenUserDoesNotExist() {
        //arrange
        Mockito.when(mockUserService.userByEmail(Mockito.anyString()))
                .thenThrow(EntityNotFoundException.class);
        Mockito.when(messageSource.getMessage(MESSAGE_INVALID_USER, null, Locale.ENGLISH))
                .thenReturn(Mockito.anyString());
        //act, assert
        Assertions.assertThrows(UsernameNotFoundException.class,
                () -> authenticationProvider.authenticate(testAuth));
    }

    @Test
    public void throw_AppropriateException_whenAccountNotActive() {
        //arrange
        Mockito.when(mockUserService.userByEmail(Mockito.anyString()))
                .thenThrow(AccountNotActivatedException.class);
        //act, assert
        Assertions.assertThrows(DisabledException.class,
                () -> authenticationProvider.authenticate(testAuth));
    }

    @Test
    public void authenticate_ShouldAuthenticate_WhenCredentialsGood() {
        //arrange
        Mockito.when(passwordEncoder.matches(Mockito.anyString(), Mockito.anyString()))
                .thenReturn(true);
        Mockito.when(mockUserService.userByEmail(Mockito.anyString()))
                .thenReturn(testUser);
        Mockito.when(mockDetailsService.loadUserByUsername(Mockito.any()))
                .thenReturn(testSpringUser);
        //act
        Authentication actual = authenticationProvider.authenticate(testAuth);
        //assert
        Assertions.assertTrue(actual.isAuthenticated());
    }

    @Test
    public void authenticate_ShouldThrow_WhenCredentialsBad() {
        //arrange
        Mockito.when(passwordEncoder.matches(Mockito.anyString(), Mockito.anyString()))
                .thenReturn(false);
        Mockito.when(mockUserService.userByEmail(Mockito.anyString()))
                .thenReturn(testUser);
        Mockito.when(mockDetailsService.loadUserByUsername(Mockito.any()))
                .thenReturn(testSpringUser);
        //act
        //assert
        Assertions.assertThrows(BadCredentialsException.class,
                () -> authenticationProvider.authenticate(testAuth));
    }

    @Test
    public void supports_ShouldReturnTrue_WhenCorrectToken() {
        //arrange, act, assert
        Assertions.assertTrue(authenticationProvider.supports(testAuth.getClass()));
    }

}
