package com.safetycar.fast.services;

import com.safetycar.SafetyCarTestObjectsFactory;
import com.safetycar.exceptions.DuplicateEntityException;
import com.safetycar.exceptions.TooManyOffersException;
import com.safetycar.models.Offer;
import com.safetycar.models.User;
import com.safetycar.security.MyCustomLoginAuthenticationSuccessHandler;
import com.safetycar.services.contracts.OfferAppendingService;
import com.safetycar.services.contracts.UserService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import static com.safetycar.SafetyCarTestObjectsFactory.getOffer;
import static com.safetycar.SafetyCarTestObjectsFactory.getEnabledUser;
import static com.safetycar.security.MySpringUserDetailsService.getAuthorities;
import static com.safetycar.util.Constants.OfferConstants.TRANSFER_OFFER;
import static com.safetycar.web.controllers.mvc.MvcIndex.INDEX_ENDPOINT;
import static com.safetycar.web.controllers.mvc.MvcOfferController.OFFER_ERROR;

@ExtendWith(MockitoExtension.class)
@RunWith(MockitoJUnitRunner.class)
public class MyCustomAuthenticationSuccessHandlerTest {

    @InjectMocks
    private MyCustomLoginAuthenticationSuccessHandler successHandler;

    @Mock
    private UserService mockUserService;

    @Mock
    private OfferAppendingService mockAppendingService;
    @Mock
    private HttpServletRequest mockRequest;
    @Mock
    private HttpServletResponse mockResponse;
    @Mock
    private HttpSession mockSession;

    private Authentication testAuth;
    private User testUser;
    private Offer testOffer;

    @BeforeEach
    void init() {
        testUser = SafetyCarTestObjectsFactory.getEnabledUser();
        List<GrantedAuthority> authorities = new LinkedList<>(getAuthorities(testUser.getAuthorities()));
        testAuth = new UsernamePasswordAuthenticationToken(testUser, testUser.getPassword(), authorities);
        testOffer = getOffer();
    }

    @Test
    public void onAuthenticationSuccess_ShouldAppendOffer() throws IOException, ServletException {
        //arrange
        Mockito.when(mockRequest.getSession(false)).thenReturn(mockSession);
        Mockito.when(mockSession.getAttribute(TRANSFER_OFFER)).thenReturn(testOffer);
        Mockito.when(mockUserService.userByEmail(Mockito.anyString())).thenReturn(testUser);
        //act
        successHandler.onAuthenticationSuccess(mockRequest, mockResponse, testAuth);
        //assert
        Mockito.verify(mockAppendingService, Mockito.times(1))
                .appendOfferAndRecordHistory(testOffer, testUser);
        Mockito.verify(mockResponse, Mockito.times(1))
                .sendRedirect(INDEX_ENDPOINT);
    }

    @Test
    public void onAuthenticationSuccess_ShouldNotAppendOffer_WhenSessionNull() throws IOException, ServletException {
        //arrange
        Mockito.when(mockRequest.getSession(false)).thenReturn(null);
        //act
        successHandler.onAuthenticationSuccess(mockRequest, mockResponse, testAuth);
        //assert
        Mockito.verify(mockAppendingService, Mockito.times(0))
                .appendOfferAndRecordHistory(testOffer, testUser);
        Mockito.verify(mockResponse, Mockito.times(1))
                .sendRedirect(INDEX_ENDPOINT);
    }

    @Test
    public void onAuthenticationSuccess_ShouldNotAppendOffer_AndShouldRemoveSessionAttribute_WhenTooManyOffers() throws IOException, ServletException {
        //arrange
        Mockito.when(mockRequest.getSession(false)).thenReturn(mockSession);
        Mockito.when(mockSession.getAttribute(TRANSFER_OFFER)).thenReturn(testOffer);
        Mockito.when(mockUserService.userByEmail(Mockito.anyString())).thenReturn(testUser);

        TooManyOffersException e = new TooManyOffersException();
        Mockito.doThrow(e).when(mockAppendingService)
                .appendOfferAndRecordHistory(testOffer, testUser);
        //act
        successHandler.onAuthenticationSuccess(mockRequest, mockResponse, testAuth);
        //assert
        Mockito.verify(mockSession, Mockito.times(1))
                .removeAttribute(TRANSFER_OFFER);
        Mockito.verify(mockSession, Mockito.times(1))
                .setAttribute(OFFER_ERROR, e.getMessage());
        Mockito.verify(mockResponse, Mockito.times(1))
                .sendRedirect(INDEX_ENDPOINT);
    }

    @Test
    public void onAuthenticationSuccess_ShouldNotAppendOffer_AndShouldRemoveSessionAttribute_WhenDuplicate() throws IOException, ServletException {
        //arrange
        Mockito.when(mockRequest.getSession(false)).thenReturn(mockSession);
        Mockito.when(mockSession.getAttribute(TRANSFER_OFFER)).thenReturn(testOffer);
        Mockito.when(mockUserService.userByEmail(Mockito.anyString())).thenReturn(testUser);

        DuplicateEntityException e = new DuplicateEntityException("message");
        Mockito.doThrow(e).when(mockAppendingService)
                .appendOfferAndRecordHistory(testOffer, testUser);
        //act
        successHandler.onAuthenticationSuccess(mockRequest, mockResponse, testAuth);
        //assert
        Mockito.verify(mockSession, Mockito.times(1))
                .removeAttribute(TRANSFER_OFFER);
        Mockito.verify(mockSession, Mockito.times(1))
                .setAttribute(OFFER_ERROR, e.getMessage());
        Mockito.verify(mockResponse, Mockito.times(1))
                .sendRedirect(INDEX_ENDPOINT);
    }


}
