package com.safetycar.fast.services;

import com.safetycar.exceptions.InvalidRecaptchaException;
import com.safetycar.security.recaptcha.RecaptchaResponse;
import com.safetycar.security.recaptcha.SimpleRecaptchaServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.core.env.Environment;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import static com.safetycar.SafetyCarTestObjectsFactory.getRecaptchaResponse;
import static com.safetycar.security.recaptcha.SimpleRecaptchaServiceImpl.*;
import static org.springframework.http.HttpMethod.POST;

@ExtendWith(MockitoExtension.class)
public class SimpleRecaptchaServiceTest {

    @InjectMocks
    private SimpleRecaptchaServiceImpl simpleRecaptchaService;

    @Mock
    private RestTemplate restTemplate;
    @Mock
    private Environment environment;
    @Mock
    private ResponseEntity<RecaptchaResponse> mockResponse;

    private RecaptchaResponse response = getRecaptchaResponse();
    private String recaptcha = "recaptcha";
    private String secret;

    @Test
    public void validateRecaptcha_ShouldValidate_WhenSuccess() {
        //arrange
        final String params = QUERY_SECRET + secret + AND_RESPONSE + recaptcha;
        Mockito.when(restTemplate.exchange(RECAPTCHA_API + params,
                POST, null, RecaptchaResponse.class)).thenReturn(mockResponse);

        Mockito.when(mockResponse.getBody()).thenReturn(response);
        //act
        //assert
        Assertions.assertDoesNotThrow(() -> simpleRecaptchaService.validateRecaptcha(recaptcha));
    }

    @Test
    public void validateRecaptcha_ShouldThrow_WhenFail() {
        //arrange
        response.setSuccess(false);
        final String params = QUERY_SECRET + secret + AND_RESPONSE + recaptcha;
        Mockito.when(restTemplate.exchange(RECAPTCHA_API + params,
                POST, null, RecaptchaResponse.class)).thenReturn(mockResponse);
        Mockito.when(mockResponse.getBody()).thenReturn(response);
        //act
        //assert
        Assertions.assertThrows(InvalidRecaptchaException.class,
                () -> simpleRecaptchaService.validateRecaptcha(recaptcha));
    }

}
