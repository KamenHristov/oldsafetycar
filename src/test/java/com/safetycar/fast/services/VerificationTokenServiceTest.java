package com.safetycar.fast.services;

import com.safetycar.exceptions.EntityNotFoundException;
import com.safetycar.exceptions.ExpiredException;
import com.safetycar.models.VerificationToken;
import com.safetycar.models.User;
import com.safetycar.repositories.VerificationTokenRepository;
import com.safetycar.services.ExpiredTokenDisposalImpl;
import com.safetycar.services.VerificationTokenServiceImpl;
import com.safetycar.services.factories.contracts.AnonymousSpecFactory;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.context.MessageSource;
import org.springframework.data.jpa.domain.Specification;


import java.util.Locale;
import java.util.Optional;

import static com.safetycar.SafetyCarTestObjectsFactory.*;
import static com.safetycar.services.VerificationTokenServiceImpl.AUTH_MESSAGE_EXPIRED;
import static com.safetycar.web.controllers.mvc.MvcRegisterController.TOKEN;

@ExtendWith(MockitoExtension.class)
@RunWith(MockitoJUnitRunner.class)
public class VerificationTokenServiceTest {

    public static final String MOCKITO_ANY_STRING = "Mockito.anyString()";

    @InjectMocks
    private VerificationTokenServiceImpl tokenService;

    @Mock
    private VerificationTokenRepository mockVerificationTokenRepository;

    @Spy
    private MessageSource mockMessageSource;

    @Mock
    private ExpiredTokenDisposalImpl tokenDisposal;

    @Mock
    private AnonymousSpecFactory mockSpecFactory;

    @Mock
    private Specification<VerificationToken> testSpec;

    private User testUser;
    private VerificationToken testToken;

    @BeforeEach
    void init() {
        testUser = getEnabledUser();
        testToken = getToken(testUser);
    }

    @Test
    public void getVerificationToken_ShouldReturnToken() {
        //arrange
        Mockito.when(mockSpecFactory.getEqualAnonymousSpec(TOKEN, MOCKITO_ANY_STRING, VerificationToken.class))
                .thenReturn(testSpec);
        Mockito.when(mockVerificationTokenRepository.findOne(testSpec))
                .thenReturn(Optional.ofNullable(testToken));
        //act
        VerificationToken actual = tokenService.findVerificationToken(MOCKITO_ANY_STRING);
        //assert
        Assertions.assertEquals(actual, testToken);
    }

    @Test
    public void getVerificationToken_ShouldThrow_WhenTokenInvalid() {
        //arrange
        Mockito.when(mockSpecFactory.getEqualAnonymousSpec(TOKEN, MOCKITO_ANY_STRING, VerificationToken.class))
                .thenReturn(testSpec);
        Mockito.when(mockVerificationTokenRepository.findOne(testSpec))
                .thenReturn(Optional.empty());
        //act, assert
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> tokenService.findVerificationToken(MOCKITO_ANY_STRING));
    }

    @Test
    public void getVerificationToken_ShouldThrow_WhenTokenExpired() {
        //arrange
        testToken = getExpiredToken(testUser);
        Mockito.when(mockSpecFactory.getEqualAnonymousSpec(TOKEN, MOCKITO_ANY_STRING, VerificationToken.class))
                .thenReturn(testSpec);
        Mockito.when(mockVerificationTokenRepository.findOne(testSpec))
                .thenReturn(Optional.ofNullable(testToken));
        //act, assert
        Assertions.assertThrows(ExpiredException.class,
                () -> tokenService.findVerificationToken(MOCKITO_ANY_STRING));
        Mockito.verify(mockMessageSource, Mockito.times(1))
                .getMessage(AUTH_MESSAGE_EXPIRED, null, Locale.ENGLISH);
        Mockito.verify(tokenDisposal, Mockito.times(1)).clearExpiredAndDeleteInactiveUser(testToken);
    }

    @Test
    public void createVerificationToken_ShouldCreateToken() {
        //arrange, act
        tokenService.create(Mockito.any());
        //assert
        Mockito.verify(mockVerificationTokenRepository, Mockito.times(1)).save(Mockito.any());

    }

    @Test
    public void getNewToken_ShouldGet() {
        //arrange, act
        VerificationToken actual = tokenService.getNewUnsavedToken(testUser);
        //assert
        assertTokensAreEqualExceptUUID(testToken, actual);
    }

    private void assertTokensAreEqualExceptUUID(VerificationToken expected, VerificationToken actual) {
        Assertions.assertEquals(expected.getUser(), actual.getUser());
        Assertions.assertEquals(expected.getId(), actual.getId());
        int expectedUUIDLength = expected.getToken().length();
        int actualUUIDLength = actual.getToken().length();
        Assertions.assertEquals(expectedUUIDLength, actualUUIDLength);

        Assertions.assertNotEquals(expected.getToken(), actual.getToken());
    }


}
