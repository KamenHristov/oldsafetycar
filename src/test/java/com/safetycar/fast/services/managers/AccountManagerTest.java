package com.safetycar.fast.services.managers;

import com.safetycar.models.Offer;
import com.safetycar.models.VerificationToken;
import com.safetycar.models.User;
import com.safetycar.models.UserDetails;
import com.safetycar.security.recaptcha.SimpleRecaptchaService;
import com.safetycar.services.AccountManagerImpl;
import com.safetycar.services.contracts.*;
import com.safetycar.web.dto.mappers.contracts.UserMapper;
import com.safetycar.web.dto.user.CreateDetailsDto;
import com.safetycar.web.dto.user.CreateUserDto;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import static com.safetycar.SafetyCarTestObjectsFactory.*;
import static com.safetycar.TestHelper.LOCAL_URL;

@ExtendWith(MockitoExtension.class)
public class AccountManagerTest {

    @InjectMocks
    private AccountManagerImpl accountManager;

    @Mock
    private UserMapper mockUserMapper;
    @Mock
    private MailService mockMailService;
    @Mock
    private VerificationTokenService mockTokenService;
    @Mock
    private OfferAppendingService mockOfferAppendingService;
    @Mock
    private SimpleRecaptchaService mockSimpleRecaptchaService;
    @Mock
    private UserHistoryService mockUserHistoryService;
    @Mock
    private UserService mockUserService;
    @Mock
    private HttpServletRequest testRequest;
    @Mock
    private HttpSession testSession;

    private User testUser;
    private UserDetails testDetails;
    private Offer testOffer;
    private VerificationToken testToken;
    private CreateUserDto testCreateDto;
    private CreateDetailsDto testDetailsDto;
    private String mockRecaptchaResponse = "test";

    @BeforeEach
    void init() {
        testOffer = getOffer();
        testUser = getEnabledUser();
        testDetails = testUser.getUserDetails();
        testToken = getToken(testUser);
        testDetailsDto = getDetailsDto(testUser);
    }

    @Test
    public void registerUser_ShouldRegisterUser() {
        //arrange
        Mockito.when(mockUserMapper.fromDto(testCreateDto))
                .thenReturn(testUser);
        Mockito.when(testRequest.getSession()).thenReturn(testSession);
        Mockito.when(mockTokenService.getNewUnsavedToken(testUser))
                .thenReturn(testToken);
        String uri = "/register";
        Mockito.when(testRequest.getRequestURL())
                .thenReturn(new StringBuffer(LOCAL_URL + uri));
        Mockito.when(testRequest.getRequestURI()).thenReturn(uri);
        //act
        accountManager.registerUserAndRecordHistory(testCreateDto, testOffer, testRequest, mockRecaptchaResponse);
        //assert
        Mockito.verify(mockUserService, Mockito.times(1))
                .create(testUser);
        Mockito.verify(mockOfferAppendingService, Mockito.times(1))
                .appendOfferAndRecordHistory(testOffer, testUser);
        Mockito.verify(mockTokenService, Mockito.times(1))
                .create(testToken);
        Mockito.verify(mockMailService, Mockito.times(1))
                .sendConfirmationMail(Mockito.any());
        Mockito.verify(mockSimpleRecaptchaService, Mockito.times(1))
                .validateRecaptcha(mockRecaptchaResponse);
        Mockito.verify(mockUserHistoryService, Mockito.times(1))
                .appendHistoryOnRegistration(testUser);
    }

    @Test
    public void registerUserRest_ShouldRegisterUser() {
        //arrange
        Mockito.when(mockUserMapper.fromDto(testCreateDto))
                .thenReturn(testUser);
        Mockito.when(mockTokenService.getNewUnsavedToken(testUser))
                .thenReturn(testToken);
        String uri = "/api/users/register";
        Mockito.when(testRequest.getRequestURL())
                .thenReturn(new StringBuffer(LOCAL_URL + uri));
        Mockito.when(testRequest.getRequestURI()).thenReturn(uri);
        //act
        accountManager.registerUserAndRecordHistoryRest(testCreateDto, testRequest);
        //assert
        Mockito.verify(mockUserService, Mockito.times(1))
                .create(testUser);
        Mockito.verify(mockTokenService, Mockito.times(1))
                .create(testToken);
        Mockito.verify(mockMailService, Mockito.times(1))
                .sendConfirmationMailRest(Mockito.any());
        Mockito.verify(mockUserHistoryService, Mockito.times(1))
                .appendHistoryOnRegistration(testUser);
    }

    @Test
    public void activateAccount_ShouldActivate() {
        //arrange, act
        accountManager.activateAcc(testToken);
        //assert
        Mockito.verify(mockUserService, Mockito.times(1))
                .update(testUser);
    }

    @Test
    public void getVerificationToken_ShouldGet() {
        //arrange
        Mockito.when(mockTokenService.findVerificationToken(Mockito.anyString()))
                .thenReturn(testToken);
        //act
        VerificationToken actual = accountManager.getVerificationToken(Mockito.anyString());
        //assert
        Assertions.assertEquals(testToken, actual);
    }

    @Test
    public void getDetailsToEdit_ShouldGetDetailsDto() {
        //arrange
        Mockito.when(mockUserMapper.toDto(testUser)).thenReturn(testDetailsDto);
        Mockito.when(mockUserService.userByEmail(Mockito.anyString()))
                .thenReturn(testUser);
        //act
        CreateDetailsDto actual = accountManager.getDetailsToEdit(Mockito.anyString());
        //assert
        Assertions.assertEquals(testDetailsDto, actual);
    }

    @Test
    public void updateDetails_ShouldUpdate() {
        //arrange
        Mockito.when(mockUserService.userByEmail(Mockito.anyString()))
                .thenReturn(testUser);
        Mockito.when(mockUserMapper.updateFromDto(testDetailsDto, testDetails))
                .thenReturn(testDetails);
        //act
        accountManager.updateDetailsAndRecordHistory(testDetailsDto, Mockito.anyString());
        //assert
        Mockito.verify(mockUserService, Mockito.times(1))
                .update(testUser);
    }

}
