package com.safetycar.fast.services;

import com.safetycar.SafetyCarTestObjectsFactory;
import com.safetycar.exceptions.InvalidOldPasswordException;
import com.safetycar.exceptions.SamePasswordException;
import com.safetycar.models.User;
import com.safetycar.repositories.UserRepository;
import com.safetycar.services.PasswordChangingServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.crypto.password.PasswordEncoder;

import static com.safetycar.SafetyCarTestObjectsFactory.getEnabledUser;
import static com.safetycar.services.PasswordChangingServiceImpl.ADMINS;

@RunWith(MockitoJUnitRunner.class)
@ExtendWith(MockitoExtension.class)
public class PasswordChangingServiceTest {

    @InjectMocks
    private PasswordChangingServiceImpl passwordChangingService;
    @Mock
    private UserRepository mockUserRepository;
    @Mock
    private PasswordEncoder mockPasswordEncoder;
    private User user;
    private final String newPassword = "newPass";
    private final String encodedNewPass = "encodedNewPass";
    private final String oldPass = "oldPass";

    @BeforeEach
    void init() {
        user = SafetyCarTestObjectsFactory.getEnabledUser();
    }

    @Test
    public void changePassword_ShouldChangePassword_WhenAllIsWell() {
        //arrange
        Mockito.when(mockPasswordEncoder.matches(oldPass, user.getPassword()))
                .thenReturn(true);
        Mockito.when(mockPasswordEncoder.matches(newPassword, user.getPassword()))
                .thenReturn(false);
        Mockito.when(mockPasswordEncoder.encode(newPassword)).thenReturn(encodedNewPass);
        //act
        passwordChangingService.changePassword(user, newPassword, oldPass);
        //assert
        Mockito.verify(mockUserRepository, Mockito.times(1)).save(user);
        Assertions.assertEquals(user.getPassword(), encodedNewPass);
    }

    @Test
    public void changePassword_ShouldThrow_WhenPasswordSameAsOld() {
        //arrange
        Mockito.when(mockPasswordEncoder.matches(oldPass, user.getPassword()))
                .thenReturn(true);
        Mockito.when(mockPasswordEncoder.matches(newPassword, user.getPassword()))
                .thenReturn(true);
        //act
        //assert
        Assertions.assertThrows(SamePasswordException.class,
                () -> passwordChangingService.changePassword(user, newPassword, oldPass));
    }

    @Test
    public void changePassword_ShouldThrow_WhenOldPassDoesNotMatch() {
        //arrange
        Mockito.when(mockPasswordEncoder.matches(oldPass, user.getPassword()))
                .thenReturn(false);
        //act
        //assert
        Assertions.assertThrows(InvalidOldPasswordException.class,
                () -> passwordChangingService.changePassword(user, newPassword, oldPass));
    }

    @Test
    public void changePassword_ShouldThrow_WhenAdminsDemoAccount() {
        //arrange
        user.setUserName(ADMINS);

        //act
        //assert
        Assertions.assertThrows(InvalidOldPasswordException.class,
                () -> passwordChangingService.changePassword(user, newPassword, oldPass));
    }

    @Test
    public void changeForgottenPass_ShouldThrow_WhenAdminsDemoAccount() {
        //arrange
        user.setUserName(ADMINS);

        //act
        //assert
        Assertions.assertThrows(InvalidOldPasswordException.class,
                () -> passwordChangingService.changeForgottenPassword(user, newPassword));
    }

    @Test
    public void changeForgottenPassword_ShouldChangePassword() {
        //arrange
        Mockito.when(mockPasswordEncoder.encode(newPassword)).thenReturn(encodedNewPass);
        //act
        passwordChangingService.changeForgottenPassword(user, newPassword);
        //assert
        Mockito.verify(mockUserRepository, Mockito.times(1)).save(user);
        Assertions.assertEquals(user.getPassword(), encodedNewPass);
    }

}
