package com.safetycar.fast.services;

import com.safetycar.enums.UserRoles;
import com.safetycar.exceptions.DuplicateEntityException;
import com.safetycar.exceptions.TooManyOffersException;
import com.safetycar.models.Authority;
import com.safetycar.models.Offer;
import com.safetycar.models.User;
import com.safetycar.repositories.OfferRepository;
import com.safetycar.services.OfferAppendingServiceImpl;
import com.safetycar.services.contracts.UserHistoryService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

import static com.safetycar.SafetyCarTestObjectsFactory.*;
import static com.safetycar.exceptions.TooManyOffersException.MAXIMUM_OFFERS_ALLOWED;

@ExtendWith(MockitoExtension.class)
public class OfferAppendingServiceTest {

    @InjectMocks
    private OfferAppendingServiceImpl offerAppendingService;

    @Mock
    private OfferRepository mockOfferRepository;

    @Mock
    private UserHistoryService mockUserHistoryService;

    private User testUser;
    private Offer testOffer;

    @BeforeEach
    void init() {
        testUser = getEnabledUser();
        testOffer = getOffer();
    }

    @Test
    public void append_ShouldThrow_WhenExceedingMaximumOffers() {
        //arrange
        prepareUserToExceedOfferLimit();
        //act
        // assert
        Assertions.assertThrows(TooManyOffersException.class,
                () -> offerAppendingService.appendOfferAndRecordHistory(testOffer, testUser));
        Mockito.verify(mockUserHistoryService, Mockito.times(0))
                .appendHistoryOnOfferCreation(testUser);
    }

    private void prepareUserToExceedOfferLimit() {
        for (int i = 0; i < MAXIMUM_OFFERS_ALLOWED; i++) {
            Offer offer = getOffer();
            offer.setPremium(BigDecimal.valueOf(Integer.MAX_VALUE + i));
            testUser.addOffer(offer);
        }
    }

    @Test
    public void append_ShouldNotAppend_WhenOfferNull() {
        //arrange
        Offer offer = null;
        //act
        offerAppendingService.appendOfferAndRecordHistory(offer, testUser);
        //assert
        int expected = 0;
        int actual = testUser.getOffers().size();
        Assertions.assertEquals(expected, actual);
        Mockito.verify(mockUserHistoryService, Mockito.times(0))
                .appendHistoryOnOfferCreation(testUser);
    }

    @Test
    public void append_ShouldNotAppend_WhenUserAdmin() {
        //arrange
        testUser = getAgent();
        //act
        offerAppendingService.appendOfferAndRecordHistory(testOffer, testUser);
        //assert
        int expected = 0;
        int actual = testUser.getOffers().size();
        Assertions.assertEquals(expected, actual);
        Mockito.verify(mockUserHistoryService, Mockito.times(0))
                .appendHistoryOnOfferCreation(testUser);

    }

    @Test
    public void append_ShouldThrow_WhenDuplicate() {
        //arrange
        Mockito.when(mockOfferRepository.exists(testOffer, testUser)).thenReturn(true);
        //act, assert
        Assertions.assertThrows(DuplicateEntityException.class, () ->
                offerAppendingService.appendOfferAndRecordHistory(testOffer, testUser));
        Mockito.verify(mockUserHistoryService, Mockito.times(0))
                .appendHistoryOnOfferCreation(testUser);
    }

    @Test
    public void append_ShouldAppendOffer_AndHistory_WhenAllIsWell() {
        //arrange, act
        offerAppendingService.appendOfferAndRecordHistory(testOffer, testUser);
        //assert
        int expected = 1;
        int actual = testUser.getOffers().size();
        Assertions.assertEquals(expected, actual);
        Mockito.verify(mockUserHistoryService, Mockito.times(1))
                .appendHistoryOnOfferCreation(testUser);
    }

}

