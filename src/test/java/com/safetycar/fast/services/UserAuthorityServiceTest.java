package com.safetycar.fast.services;

import com.safetycar.models.User;
import com.safetycar.services.UserAuthorityServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static com.safetycar.SafetyCarTestObjectsFactory.*;

@ExtendWith(MockitoExtension.class)
public class UserAuthorityServiceTest {

    @Mock
    private UserAuthorityServiceImpl userPrivilegeService;

    private User actual;
    private User expectedAgent;
    private User expectedUser;
    private boolean isEmpty;

    @BeforeEach
    void init() {
        actual = getEnabledUser();
        actual.setAuthorities(null);
        isEmpty = actual.getAuthorities().isEmpty();
        expectedUser = getEnabledUser();
        expectedAgent = getAgent();
    }


    @Test
    public void assignUserPrivilege_ShouldAssignUserRole() {
        //arrange
        Mockito.doCallRealMethod().when(userPrivilegeService).assignUserAuthority(actual);
        Assertions.assertTrue(isEmpty);
        //act
        userPrivilegeService.assignUserAuthority(actual);
        //assert
        Assertions.assertEquals(expectedUser, actual);
    }

    @Test
    public void assignUserPrivilege_ShouldAssignAgentRole() {
        //arrange
        Mockito.doCallRealMethod().when(userPrivilegeService).assignAgentAuthority(actual);
        Assertions.assertTrue(isEmpty);
        //act
        userPrivilegeService.assignAgentAuthority(actual);
        //assert
        Assertions.assertEquals(expectedAgent, actual);
    }

}
