package com.safetycar.fast.services;

import com.safetycar.models.History;
import com.safetycar.models.User;
import com.safetycar.services.UserHistoryServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.junit.jupiter.MockitoExtension;

import static com.safetycar.SafetyCarTestObjectsFactory.*;

@RunWith(MockitoJUnitRunner.class)
@ExtendWith(MockitoExtension.class)
public class UserHistoryServiceTest {

    @InjectMocks
    private UserHistoryServiceImpl historyService;

    private User user;
    private History expected;

    @BeforeEach
    void init() {
        user = getEnabledUser();
    }

    @Test
    public void appendHistoryOnOfferCreation_ShouldAppendHistory() {
        //arrange
        expected = getHistoryForOfferCreation(user);
        //act
        historyService.appendHistoryOnOfferCreation(user);
        //assert
        Assertions.assertEquals(expected, user.getHistory().toArray()[0]);
    }

    @Test
    public void appendHistoryOnRegistration_ShouldAppendHistory() {
        //arrange
        expected = getHistoryForRegistration(user);
        //act
        historyService.appendHistoryOnRegistration(user);
        //assert
        Assertions.assertEquals(expected, user.getHistory().toArray()[0]);
    }

    @Test
    public void appendHistoryOnDetailsUpdate_ShouldAppendHistory() {
        //arrange
        expected = getHistoryForDetailsUpdate(user);
        //act
        historyService.appendHistoryOnDetailsUpdate(user);
        //assert
        Assertions.assertEquals(expected, user.getHistory().toArray()[0]);
    }

}
