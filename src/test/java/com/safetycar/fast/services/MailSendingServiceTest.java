package com.safetycar.fast.services;

import com.safetycar.models.Offer;
import com.safetycar.models.Policy;
import com.safetycar.models.VerificationToken;
import com.safetycar.models.User;
import com.safetycar.services.MailServiceImpl;
import com.safetycar.web.wrapper.TokenWrapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.context.MessageSource;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;

import java.util.Arrays;
import java.util.Locale;

import static com.safetycar.SafetyCarTestObjectsFactory.*;
import static com.safetycar.services.MailServiceImpl.*;
import static com.safetycar.util.Constants.ErrorsConstants.UNEXPECTED_MESSAGE;

@ExtendWith(MockitoExtension.class)
public class MailSendingServiceTest {

    @InjectMocks
    private MailServiceImpl mailService;

    @Mock
    private MessageSource messageSource;

    @Mock
    private JavaMailSender mailSender;

    private String message;
    private String appUrl;
    private Locale locale;
    private User user;
    private String sendTo;

    @BeforeEach
    void init() {
        user = getEnabledUser();
        locale = Locale.ENGLISH;
        appUrl = "appUrl";
        message = "message";
        sendTo = user.getUserName();
    }

    @Test
    public void confirmRegistrationMail_ShouldSendMail() {
        //arrange
        VerificationToken token = getToken(user);

        Mockito.when(messageSource.getMessage(Mockito.anyString(), Mockito.any(), Mockito.any()))
                .thenReturn(message);

        TokenWrapper wrapper = new TokenWrapper(appUrl, locale, token);
        String confirmationUrl = wrapper.getAppUrl() + CONFIRMATION_RETURN_URL + token.getToken();
        String text = message + NEW_LINE + confirmationUrl;

        SimpleMailMessage email = getSimpleMailMessage(text, REGISTRATION_CONFIRMATION, sendTo);
        //act
        mailService.sendConfirmationMail(wrapper);
        // assert
        Mockito.verify(mailSender, Mockito.times(1)).send(email);
    }

    @Test
    public void confirmRegistrationMailRest_ShouldSendMail() {
        //arrange
        VerificationToken token = getToken(user);
        String text = message + SPACE_COLUMN_SPACE + token.getToken();

        Mockito.when(messageSource.getMessage(Mockito.anyString(), Mockito.any(), Mockito.any()))
                .thenReturn(message);

        TokenWrapper wrapper = new TokenWrapper(appUrl, locale, token);

        SimpleMailMessage email = getSimpleMailMessage(text, REGISTRATION_CONFIRMATION, sendTo);
        //act
        mailService.sendConfirmationMailRest(wrapper);
        // assert
        Mockito.verify(mailSender, Mockito.times(1)).send(email);
    }

    @Test
    public void changeStatusMail_ShouldSendMail() {
        //arrange
        Offer offer = getOffer();
        Policy policy = getPolicy(user, offer);
        //
        String text = YOUR_POLICY_S_STATUS_HAS_BEEN_CHANGED_TO + policy.getStatus().getStatus();

        SimpleMailMessage email = getSimpleMailMessage(text, POLICY_STATUS_CHANGE, sendTo);
        //act
        mailService.sendPolicyStatusMail(policy);
        //assert
        Mockito.verify(mailSender, Mockito.times(1)).send(email);
    }

    @Test
    public void sendPasswordChangeRequest_ShouldSendMail() {
        //arrange
        VerificationToken token = getToken(user);
        TokenWrapper wrapper =
                new TokenWrapper(appUrl, locale, token);

        Mockito.when(messageSource.getMessage(Mockito.anyString(), Mockito.any(), Mockito.any()))
                .thenReturn(message);

        String confirmationUrl = wrapper.getAppUrl() + CHANGE_PASSWORD_RETURN_URL + token.getToken();
        String text = message + NEW_LINE + confirmationUrl;

        SimpleMailMessage email = getSimpleMailMessage(text, CHANGE_FORGOTTEN_PASSWORD, sendTo);

        //act
        mailService.sendPasswordChangeRequest(wrapper);
        // assert
        Mockito.verify(mailSender, Mockito.times(1)).send(email);
    }

    @Test
    public void sendUnhandledErrorMail_ShouldSendAStackTraceMessage() {
        //arrange
        Exception e = new Exception(UNEXPECTED_MESSAGE);
        SimpleMailMessage email = getSimpleMailMessage(e);
        //act
        mailService.sendUnhandledErrorMail(e);
        //assert
        Mockito.verify(mailSender, Mockito.times(1))
                .send(email);
    }

}
