package com.safetycar.fast.services;

import com.safetycar.SafetyCarTestObjectsFactory;
import com.safetycar.exceptions.EntityNotFoundException;
import com.safetycar.models.PasswordToken;
import com.safetycar.models.User;
import com.safetycar.repositories.PasswordTokenRepository;
import com.safetycar.services.PasswordTokenServiceImpl;
import com.safetycar.services.factories.contracts.AnonymousSpecFactory;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.jpa.domain.Specification;

import static com.safetycar.SafetyCarTestObjectsFactory.getForgottenPassToken;
import static com.safetycar.SafetyCarTestObjectsFactory.getEnabledUser;
import static com.safetycar.web.controllers.mvc.MvcRegisterController.TOKEN;

@RunWith(MockitoJUnitRunner.class)
@ExtendWith(MockitoExtension.class)
public class PasswordTokenServiceTest {

    @InjectMocks
    private PasswordTokenServiceImpl tokenService;

    @Mock
    private PasswordTokenRepository tokenRepository;
    @Mock
    private AnonymousSpecFactory specFactory;

    private Specification<PasswordToken> tokenSpecification;
    private PasswordToken expected;
    private User user;
    private String token = "token";

    @BeforeEach
    void init() {
        user = SafetyCarTestObjectsFactory.getEnabledUser();
        expected = getForgottenPassToken(user);
    }

    @Test
    public void createVerificationToken_ShouldCreate() {
        //arrange
        //act
        tokenService.create(expected);
        //assert
        Mockito.verify(tokenRepository, Mockito.times(1))
                .save(expected);
    }

    @Test
    public void findForgottenPasswordToken_ShouldFind_WhenTokenStringValid() {
        //arrange
        Mockito.when(specFactory.getEqualAnonymousSpec(TOKEN, token, PasswordToken.class))
                .thenReturn(tokenSpecification);
        Mockito.when(tokenRepository.findOne(tokenSpecification))
                .thenReturn(java.util.Optional.ofNullable(expected));
        //act
        PasswordToken actual = tokenService.findForgottenPasswordToken(token);
        //assert
        Assertions.assertEquals(actual, expected);
    }

    @Test
    public void findForgottenPasswordToken_ShouldThrow_WhenTokenStringInvalid() {
        //arrange
        Mockito.when(specFactory.getEqualAnonymousSpec(TOKEN, token, PasswordToken.class))
                .thenReturn(tokenSpecification);
        Mockito.when(tokenRepository.findOne(tokenSpecification))
                .thenReturn(java.util.Optional.empty());
        //act
        //assert
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> tokenService.findForgottenPasswordToken(token));
    }

    @Test
    public void getNewUnsavedToken_ShouldReturnToken() {
        //arrange
        //act
        PasswordToken actual = tokenService.getNewUnsavedToken(user);
        //assert
        Assertions.assertEquals(actual.getUser(), expected.getUser());
        Assertions.assertEquals(getTokenUUIDLength(actual), getTokenUUIDLength(expected));
    }

    private int getTokenUUIDLength(PasswordToken token) {
        String uuidToken = token.getToken();
        return uuidToken.length();
    }

    @Test
    public void validateToken_ShouldNotThrow_WhenTokenExists() {
        //arrange
        Mockito.when(tokenRepository.existsByToken(token))
                .thenReturn(true);
        //act
        //assert
        Assertions.assertDoesNotThrow(() -> tokenService.validateToken(token));
    }

    @Test
    public void validateToken_ShouldThrow_WhenTokenDoesExist() {
        //arrange
        Mockito.when(tokenRepository.existsByToken(token))
                .thenReturn(false);
        //act
        //assert
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> tokenService.validateToken(token));
    }


    @Test
    public void delete_ShouldCallRepositoryToDelete() {
        //arrange
        //act
        tokenService.delete(expected);
        //assert
        Mockito.verify(tokenRepository, Mockito.times(1))
                .delete(expected);
    }

}
