package com.safetycar.fast.services;

import com.safetycar.models.Brand;
import com.safetycar.models.Model;
import com.safetycar.repositories.ModelRepository;
import com.safetycar.exceptions.EntityNotFoundException;
import com.safetycar.services.ModelServiceImpl;
import com.safetycar.services.contracts.BrandService;
import com.safetycar.services.factories.contracts.AnonymousSpecFactory;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;

import java.util.*;
import java.util.stream.Collectors;

import static com.safetycar.SafetyCarTestObjectsFactory.getModel;
import static com.safetycar.services.ModelServiceImpl.YEAR;
import static com.safetycar.util.Constants.QueryConstants.BRAND;
import static com.safetycar.util.Constants.QueryConstants.NAME;

@ExtendWith(MockitoExtension.class)
public class ModelServiceTest {

    @InjectMocks
    private ModelServiceImpl modelService;

    @Mock
    private ModelRepository mockModelRepository;
    @Mock
    private AnonymousSpecFactory mockSpecFactory;
    @Mock
    private Specification<Model> mockSpec;
    @Mock
    private BrandService mockBrandService;

    private Model testModel;
    private Brand brand;
    private List<Model> testModels;
    private Sort sort;

    @BeforeEach
    public void init() {
        testModel = getModel();
        brand = testModel.getBrand();
        testModels = Collections.singletonList(testModel);

        List<Sort.Order> orders = new LinkedList<>();
        orders.add(Sort.Order.asc(NAME));
        orders.add(Sort.Order.asc(YEAR));
        sort = Sort.by(orders);
    }

    @Test
    public void get_ShouldReturn_WhenEntityExists() {
        //arrange
        Mockito.when(mockModelRepository.findById(Mockito.anyInt()))
                .thenReturn(Optional.of(testModel));
        //act
        Model actual = modelService.getOne(Mockito.anyInt());
        //assert
        Assertions.assertEquals(actual, testModel);

    }

    @Test
    public void get_ShouldThrow_WhenEntityDoesNotExist() {
        //arrange
        Mockito.when(mockModelRepository.findById(Mockito.anyInt()))
                .thenReturn(Optional.empty());
        //act, assert
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> modelService.getOne(Mockito.anyInt()));
    }

    @Test
    public void getAll_ShouldReturnCollection() {
        //arrange
        Mockito.when(mockModelRepository.findAll()).thenReturn(testModels);
        //act
        List<Model> actual = new ArrayList<>(modelService.getAll());
        //assert
        Assertions.assertEquals(actual, testModels);
    }

    @Test
    public void filter_ShouldReturnCollection() {
        //arrange
        Mockito.when(mockBrandService.getOne(Mockito.anyInt())).thenReturn(brand);
        Mockito.when(mockSpecFactory.getEqualAnonymousSpec(BRAND, brand, Model.class))
                .thenReturn(mockSpec);
        Mockito.when(mockModelRepository.findAll(mockSpec, sort)).thenReturn(testModels);
        //act
        List<Model> actual = new ArrayList<>(modelService.filter(Mockito.anyInt()));
        //assert
        Assertions.assertEquals(actual, testModels);
    }

    @Test
    public void getModelNamesByBrandId_ShouldReturnList() {
        //arrange
        Mockito.when(mockBrandService.getOne(Mockito.anyInt())).thenReturn(brand);
        Mockito.when(mockSpecFactory.getEqualAnonymousSpec(BRAND, brand, Model.class))
                .thenReturn(mockSpec);
        Mockito.when(mockModelRepository.findAll(mockSpec)).thenReturn(testModels);
        List<String> expected = getExpected();
        //act
        List<String> actual = modelService.getModelNamesByBrandId(Mockito.anyInt());
        //assert
        Assertions.assertEquals(expected, actual);
    }

    private List<String> getExpected() {
        return testModels.stream()
                .map(Model::getName)
                .collect(Collectors.toList());
    }


}
