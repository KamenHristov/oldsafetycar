package com.safetycar.fast.services.factories;

import com.safetycar.models.User;
import com.safetycar.services.factories.AnonymousSpecFactoryImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.*;

@RunWith(MockitoJUnitRunner.class)
@ExtendWith(MockitoExtension.class)
public class AnonymousSpecFactoryTest {

    @InjectMocks
    private AnonymousSpecFactoryImpl specFactory;

    @Mock
    private CriteriaBuilder mockCriteriaBuilder;
    @Mock
    private CriteriaQuery<User> mockCriteriaQuery;
    @Mock
    private Root<User> mockRoot;
    private Specification<User> spec;

    private final String anyAttr = "anyAttr";
    private final String anyValue = "anyValue";

    @Test
    public void getSingleAttributeAnonymousSpec_ShouldGetSpec() {
        //arrange
        Class<User> expectedClass = User.class;
        //act
        spec = specFactory.getEqualAnonymousSpec(anyAttr, anyValue, expectedClass);
        spec.toPredicate(mockRoot, mockCriteriaQuery, mockCriteriaBuilder);
        //assert
        Mockito.verify(mockCriteriaBuilder, Mockito.times(1))
                .equal(mockRoot.get(anyAttr), anyValue);
    }

}
