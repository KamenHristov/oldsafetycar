package com.safetycar.fast.services.factories;

import com.safetycar.models.User;
import com.safetycar.repositories.filter.base.MapBasedSpecification;
import com.safetycar.repositories.filter.UserSpec;
import com.safetycar.services.factories.UserSpecFactoryImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.HashMap;
import java.util.Map;

import static com.safetycar.repositories.filter.UserSpec.*;

@RunWith(MockitoJUnitRunner.class)
@ExtendWith(MockitoExtension.class)
public class UserSpecFactoryTest {

    @InjectMocks
    private UserSpecFactoryImpl specFactory;
    private Map<String, String> filter;

    @BeforeEach
    void init() {
        filter = new HashMap<>() {
            {
                put(SORT_PARAMETER, ID);
                put(POLICY_STATUS, ACTIVE);
            }
        };
    }

    @Test
    public void getNoParameters_ShouldGet() {
        //arrange
        UserSpec expected = new UserSpec();
        //act
        MapBasedSpecification<User> actual = specFactory.getSpec();
        //assert
        Assertions.assertEquals(expected.getSort(), actual.getSort());
        Assertions.assertEquals(expected.getSpecs(), actual.getSpecs());
    }

    @Test
    public void getUserSpec_MapParameter() {
        //arrange
        UserSpec expected = new UserSpec(filter);
        //act
        MapBasedSpecification<User> actual = specFactory.getSpec(filter);
        //assert
        Assertions.assertEquals(expected.getSort(), actual.getSort());
        Assertions.assertEquals(expected.getSpecs(), actual.getSpecs());
    }


}
