package com.safetycar.fast.services.factories;

import com.safetycar.models.Offer;
import com.safetycar.repositories.filter.base.MapBasedSpecification;
import com.safetycar.repositories.filter.OfferSpec;
import com.safetycar.services.factories.OfferSpecFactoryImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static com.safetycar.SafetyCarTestObjectsFactory.getOffer;
import static com.safetycar.repositories.filter.base.BaseMapSpec.SORT_PARAMETER;
import static com.safetycar.util.Constants.QueryConstants.*;

@RunWith(MockitoJUnitRunner.class)
@ExtendWith(MockitoExtension.class)
public class OfferSpecFactoryTest {

    @InjectMocks
    private OfferSpecFactoryImpl specFactory;

    private Map<String, String> filter;
    private Collection<Offer> offers;

    @BeforeEach
    void init() {
        filter = new HashMap<>() {
            {
                put(SORT_PARAMETER, ID);
                put(SUBMISSION_DATE, LocalDate.now().toString());
            }
        };
        Offer offer = getOffer();
        offers = Collections.singleton(offer);
    }

    @Test
    public void getSpec_NoParameters_ShouldGetSpec() {
        //arrange
        MapBasedSpecification<Offer> expected = new OfferSpec();
        //act
        MapBasedSpecification<Offer> actual = specFactory.getSpec();
        //assert
        Assertions.assertEquals(expected.getSort(), actual.getSort());
        Assertions.assertEquals(expected.getSpecs(), actual.getSpecs());
    }

    @Test
    public void getSpec_WithCollection_ShouldGetSpec() {
        //arrange
        MapBasedSpecification<Offer> expected = new OfferSpec(offers);
        //act
        MapBasedSpecification<Offer> actual = specFactory.getSpec(offers);
        //assert
        Assertions.assertEquals(expected.getSort(), actual.getSort());
        Assertions.assertEquals(expected.getSpecs(), actual.getSpecs());
    }

    @Test
    public void getSpec_WithCollectionAndMap_ShouldGetSpec() {
        //arrange;
        MapBasedSpecification<Offer> expected = new OfferSpec(offers, filter);
        //act
        MapBasedSpecification<Offer> actual = specFactory.getSpec(offers, filter);
        //assert
        Assertions.assertEquals(expected.getSort(), actual.getSort());
        Assertions.assertEquals(expected.getSpecs(), actual.getSpecs());
    }

}
