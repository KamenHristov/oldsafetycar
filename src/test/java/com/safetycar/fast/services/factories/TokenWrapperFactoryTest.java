package com.safetycar.fast.services.factories;

import com.safetycar.models.PasswordToken;
import com.safetycar.models.User;
import com.safetycar.models.VerificationToken;
import com.safetycar.services.factories.TokenWrapperFactoryImpl;
import com.safetycar.web.wrapper.TokenWrapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Locale;

import static com.safetycar.SafetyCarTestObjectsFactory.*;
import static com.safetycar.TestHelper.LOCAL_URL;

@RunWith(MockitoJUnitRunner.class)
@ExtendWith(MockitoExtension.class)
public class TokenWrapperFactoryTest {

    @InjectMocks
    private TokenWrapperFactoryImpl tokenWrapperFactory;
    private TokenWrapper expectedVerificationWrapper;
    private TokenWrapper expectedForgottenPassWrapper;
    private VerificationToken verificationToken;
    private PasswordToken forgottenPassToken;


    @BeforeEach
    void init() {
        User user = getEnabledUser();
        verificationToken = getToken(user);
        forgottenPassToken = getForgottenPassToken(user);
        expectedForgottenPassWrapper = getTokenWrapper(LOCAL_URL, forgottenPassToken, Locale.ENGLISH);
        expectedVerificationWrapper = getTokenWrapper(LOCAL_URL, verificationToken, Locale.ENGLISH);
    }

    @Test
    public void getTokenWrapper_ShouldReturn_TokenWrapper() {
        //arrange
        //act
        TokenWrapper actualForgottenPassWrapper = tokenWrapperFactory
                .getTokenWrapper(LOCAL_URL, Locale.ENGLISH, forgottenPassToken);
        TokenWrapper actualVerificationWrapper = tokenWrapperFactory
                .getTokenWrapper(LOCAL_URL, Locale.ENGLISH, verificationToken);
        //assert
        Assertions.assertEquals(expectedForgottenPassWrapper, actualForgottenPassWrapper);
        Assertions.assertEquals(expectedVerificationWrapper, actualVerificationWrapper);
    }

}
