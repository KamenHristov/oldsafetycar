package com.safetycar.fast.services.factories;

import com.safetycar.models.Offer;
import com.safetycar.models.Policy;
import com.safetycar.models.User;
import com.safetycar.repositories.filter.base.MapBasedSpecification;
import com.safetycar.repositories.filter.PolicySpec;
import com.safetycar.services.factories.PolicySpecFactoryImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.util.*;

import static com.safetycar.SafetyCarTestObjectsFactory.*;
import static com.safetycar.repositories.filter.base.BaseMapSpec.SORT_PARAMETER;
import static com.safetycar.util.Constants.QueryConstants.*;

@RunWith(MockitoJUnitRunner.class)
@ExtendWith(MockitoExtension.class)
public class PolicySpecFactoryTest {

    @InjectMocks
    private PolicySpecFactoryImpl specFactory;

    private Collection<Policy> collection;
    private Map<String, String> filter;

    @BeforeEach
    void init() {
        User testUser = getEnabledUser();
        Offer testOffer = getOffer();
        Policy testPolicy = getPolicy(testUser, testOffer);
        testUser.addPolicy(testPolicy);
        collection = testUser.getPolicies();
        filter = new HashMap<>() {
            {
                put(SORT_PARAMETER, ID);
                put(SUBMISSION_DATE, LocalDate.now().toString());
            }
        };
    }

    @Test
    public void getSpec_NoParameters_ShouldGetSpec() {
        //arrange
        MapBasedSpecification<Policy> expected = new PolicySpec();
        //act
        MapBasedSpecification<Policy> actual = specFactory.getSpec();
        //assert
        Assertions.assertEquals(expected.getSort(), actual.getSort());
        Assertions.assertEquals(expected.getSpecs(), actual.getSpecs());
    }

    @Test
    public void getSpec_WithCollection_ShouldGetSpec() {
        //arrange
        MapBasedSpecification<Policy> expected = new PolicySpec(collection);
        //act
        MapBasedSpecification<Policy> actual = specFactory.getSpec(collection);
        //assert
        Assertions.assertEquals(expected.getSort(), actual.getSort());
        Assertions.assertEquals(expected.getSpecs(), actual.getSpecs());
    }

    @Test
    public void getSpec_WithCollectionAndMap_ShouldGetSpec() {
        //arrange
        MapBasedSpecification<Policy> expected = new PolicySpec(collection, filter);
        //act
        MapBasedSpecification<Policy> actual = specFactory.getSpec(collection, filter);
        //assert
        Assertions.assertEquals(expected.getSort(), actual.getSort());
        Assertions.assertEquals(expected.getSpecs(), actual.getSpecs());
    }

}
