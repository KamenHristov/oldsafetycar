package com.safetycar.fast.wrappers;

import com.safetycar.SafetyCarTestObjectsFactory;
import com.safetycar.models.User;
import com.safetycar.models.UserDetails;
import com.safetycar.models.wrappers.UserDuplicateWrapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.junit.jupiter.MockitoExtension;

import static com.safetycar.SafetyCarTestObjectsFactory.getEnabledUser;
import static com.safetycar.SafetyCarTestObjectsFactory.getUserDuplicateWrapper;

@ExtendWith(MockitoExtension.class)
@RunWith(MockitoJUnitRunner.class)
public class UserDuplicateWrapperTest {

    public static final String ANY_STRING = "Mockito.anyString()";
    private final User user = SafetyCarTestObjectsFactory.getEnabledUser();
    private UserDetails details;

    private UserDuplicateWrapper duplicateWrapper;

    @BeforeEach
    void init() {
        details = user.getUserDetails();
    }

    @Test
    public void isDuplicate_ShouldReturnTrue_WhenFullNameAndAddressNotEqual_AndTelephoneIsEqual() {
        //arrange
        UserDuplicateWrapper duplicateWrapper = getUserDuplicateWrapper(details);
        //act, assert
        Assertions.assertTrue(duplicateWrapper.isDuplicateOf(details));
    }


    @Test
    public void isDuplicate_ShouldReturnTrue_FullNameAndAddressEqual_AndTelephoneNotEqual() {
        //arrange
        duplicateWrapper = getUserDuplicateWrapper(details);

        details.setTelephone(ANY_STRING);
        //act, assert
        Assertions.assertTrue(duplicateWrapper.isDuplicateOf(details));
    }

    @Test
    public void isDuplicate_ShouldReturnFalse_WhenFullNameAndAddressNotEqual_AndTelephoneIsNotEqual() {
        //arrange
        duplicateWrapper = getUserDuplicateWrapper(details);
        details.setLastName(ANY_STRING);
        details.setTelephone(ANY_STRING);
        details.setAddress(ANY_STRING);
        details.setFirstName(ANY_STRING);
        //act, assert
        Assertions.assertFalse(duplicateWrapper.isDuplicateOf(details));
    }


    @Test
    public void isDuplicate_ShouldReturnFalse_WhenFullNameAndAddressNotEqual_AndTelephoneIsEqual_ButDetailsInactive() {
        //arrange
        details.setActive(false);
        duplicateWrapper = getUserDuplicateWrapper(details);

        details.setLastName(ANY_STRING);
        details.setAddress(ANY_STRING);
        details.setFirstName(ANY_STRING);
        //act, assert
        Assertions.assertFalse(duplicateWrapper.isDuplicateOf(details));
    }

    @Test
    public void isDuplicate_ShouldReturnFalse_FullNameAndAddressEqual_AndTelephoneNotEqual_ButDetailsInactive() {
        //arrange
        details.setActive(false);
        duplicateWrapper = getUserDuplicateWrapper(details);

        details.setTelephone(ANY_STRING);
        //act, assert
        Assertions.assertFalse(duplicateWrapper.isDuplicateOf(details));
    }

    @Test
    public void detailsHaveChanged_ShouldReturnFalse_WhenDetailsHaveNotChanged_AndDetailsActive() {
        //arrange
        details.setActive(true);
        duplicateWrapper = getUserDuplicateWrapper(details);
        //act
        //assert
        Assertions.assertFalse(duplicateWrapper.detailsHaveChanged(details));
    }

    @Test
    public void detailsHaveChanged_ShouldReturnTrue_WhenDetailsHaveNotChanged_AndDetailsNotActive() {
        //arrange
        details.setActive(false);
        duplicateWrapper = getUserDuplicateWrapper(details);
        //act
        //assert
        Assertions.assertTrue(duplicateWrapper.detailsHaveChanged(details));
    }

    @Test
    public void detailsHaveChanged_ShouldReturnTrue_WhenDetailsHaveChanged_AndDetailsActive() {
        //arrange
        details.setActive(true);
        duplicateWrapper = getUserDuplicateWrapper(details);
        //act
        details.setFirstName("newName");
        //assert
        Assertions.assertTrue(duplicateWrapper.detailsHaveChanged(details));
    }

}
