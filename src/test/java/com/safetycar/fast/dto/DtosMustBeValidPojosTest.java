package com.safetycar.fast.dto;

import com.openpojo.reflection.PojoClass;
import com.openpojo.reflection.filters.FilterPackageInfo;
import com.openpojo.reflection.impl.PojoClassFactory;
import com.openpojo.validation.Validator;
import com.openpojo.validation.ValidatorBuilder;
import com.openpojo.validation.affirm.Affirm;
import com.openpojo.validation.rule.impl.GetterMustExistRule;
import com.openpojo.validation.rule.impl.SetterMustExistRule;
import com.openpojo.validation.test.impl.GetterTester;
import com.openpojo.validation.test.impl.SetterTester;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class DtosMustBeValidPojosTest {

    // Configured for expectation, so we know when a class gets added or removed.
    private static final int EXPECTED_OFFER_DTOS_COUNT = 3;
    private static final int EXPECTED_POLICY_DTOS_COUNT = 6;
    private static final int EXPECTED_USER_DTOS_COUNT = 7;

    // The packages to test
    private static final String OFFER_DTOS = "com.safetycar.web.dto.offer";
    private static final String POLICY_DTOS = "com.safetycar.web.dto.policy";
    private static final String USER_DTOS = "com.safetycar.web.dto.user";

    @Test
    public void ensureExpectedOfferDtosCount() {
        List<PojoClass> pojoClasses = PojoClassFactory.getPojoClasses(OFFER_DTOS,
                new FilterPackageInfo());
        Affirm.affirmEquals("Classes added / removed?", EXPECTED_OFFER_DTOS_COUNT, pojoClasses.size());
    }

    @Test
    public void testOfferDtosStructureAndBehavior() {
        Validator validator = ValidatorBuilder.create()
                // Add Rules to validate structure for POJO_PACKAGE
                .with(new GetterMustExistRule())
                .with(new SetterMustExistRule())
                // Add Testers to validate behaviour for POJO_PACKAGE
                .with(new SetterTester())
                .with(new GetterTester())
                .build();

        validator.validate(OFFER_DTOS, new FilterPackageInfo());
    }

    @Test
    public void ensureExpectedPolicyDtosCount() {
        List<PojoClass> pojoClasses = PojoClassFactory.getPojoClasses(POLICY_DTOS,
                new FilterPackageInfo());
        Affirm.affirmEquals("Classes added / removed?", EXPECTED_POLICY_DTOS_COUNT, pojoClasses.size());
    }

    @Test
    public void testPolicyDtosStructureAndBehavior() {
        Validator validator = ValidatorBuilder.create()
                // Add Rules to validate structure for POJO_PACKAGE
                .with(new GetterMustExistRule())
                .with(new SetterMustExistRule())
                // Add Testers to validate behaviour for POJO_PACKAGE
                .with(new SetterTester())
                .with(new GetterTester())
                .build();

        validator.validate(POLICY_DTOS, new FilterPackageInfo());
    }

    @Test
    public void ensureExpectedUserDtosCount() {
        List<PojoClass> pojoClasses = PojoClassFactory.getPojoClasses(USER_DTOS,
                new FilterPackageInfo());
        Affirm.affirmEquals("Classes added / removed?", EXPECTED_USER_DTOS_COUNT, pojoClasses.size());
    }

    @Test
    public void testUserDtosStructureAndBehavior() {
        Validator validator = ValidatorBuilder.create()
                // Add Rules to validate structure for POJO_PACKAGE
                .with(new GetterMustExistRule())
                .with(new SetterMustExistRule())
                // Add Testers to validate behaviour for POJO_PACKAGE
                .with(new SetterTester())
                .with(new GetterTester())
                .build();

        validator.validate(USER_DTOS, new FilterPackageInfo());
    }

}