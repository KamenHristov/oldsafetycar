package com.safetycar.fast.dto;

import com.safetycar.web.dto.user.SearchUsersDto;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.HashMap;
import java.util.Map;

@RunWith(MockitoJUnitRunner.class)
public class SearchUsersDtoTest {

    private SearchUsersDto dto = new SearchUsersDto();

    @Test
    public void getSearchParams_ShouldReturnStringStringMap() {
        //arrange
        SearchUsersDto dto = new SearchUsersDto();
        Map<String, String> expected = new HashMap<>();
        //act
        Map<String, String> actual = dto.getSearchParams();
        //assert
        Assertions.assertEquals(expected, actual);
    }

}
