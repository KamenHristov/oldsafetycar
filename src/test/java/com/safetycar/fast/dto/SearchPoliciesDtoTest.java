package com.safetycar.fast.dto;

import com.safetycar.web.dto.policy.SearchPolicyDto;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.HashMap;
import java.util.Map;

@RunWith(MockitoJUnitRunner.class)
public class SearchPoliciesDtoTest {

    private SearchPolicyDto dto = new SearchPolicyDto();

    @Test
    public void getSearchParams_ShouldReturnStringStringMap() {
        //arrange
        SearchPolicyDto dto = new SearchPolicyDto();
        Map<String, String> expected = new HashMap<>();
        //act
        Map<String, String> actual = dto.getSearchParams();
        //assert
        Assertions.assertEquals(expected, actual);
    }

    @Test
    public void getSearchParamsWithAdditionalParam_ShouldReturnStringStringMap() {
        //arrange
        String additionalParam = "additionalParam";
        SearchPolicyDto dto = new SearchPolicyDto();
        Map<String, String> expected = new HashMap<>();
        expected.put(additionalParam, additionalParam);
        //act
        Map<String, String> actual = dto.getSearchParams(additionalParam);
        //assert
        Assertions.assertEquals(expected, actual);
    }

}
