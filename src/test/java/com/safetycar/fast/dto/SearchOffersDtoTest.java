package com.safetycar.fast.dto;

import com.safetycar.web.dto.offer.SearchOffersDto;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.HashMap;
import java.util.Map;

@RunWith(MockitoJUnitRunner.class)
public class SearchOffersDtoTest {

    private SearchOffersDto dto = new SearchOffersDto();

    @Test
    public void getSearchParams_ShouldReturnStringStringMap() {
        //arrange
        SearchOffersDto dto = new SearchOffersDto();
        Map<String, String> expected = new HashMap<>();
        //act
        Map<String, String> actual = dto.getSearchParams();
        //assert
        Assertions.assertEquals(expected, actual);
    }

}
