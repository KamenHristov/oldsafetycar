package com.safetycar.fast.annotations;

import com.safetycar.validation.ValidCapacity;
import com.safetycar.validation.impl.ValidCapacityImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.validation.ConstraintValidatorContext;
import javax.validation.Payload;
import java.lang.annotation.Annotation;

import static com.safetycar.validation.impl.ValidCapacityImpl.MUST_BE_A_VALID_INTEGER;
import static org.mockito.Mockito.doCallRealMethod;
import static org.mockito.Mockito.times;


@ExtendWith(MockitoExtension.class)
@RunWith(MockitoJUnitRunner.class)
public class ValidCapacityImplTest {

    @Mock
    private ConstraintValidatorContext mockContext;
    @Mock
    private ConstraintValidatorContext.ConstraintViolationBuilder mockViolationBuilder;
    @Mock
    private ValidCapacityImpl validCapacity;

    @BeforeEach
    public void init() {

        doCallRealMethod().when(validCapacity).initialize(Mockito.any());
        Mockito.when(validCapacity.isValid(Mockito.any(), Mockito.any())).thenCallRealMethod();

        ValidMaxAgeTestClass testClass = new ValidMaxAgeTestClass();

        validCapacity.initialize(testClass);

    }

    @Test
    public void isValid_ShouldReturnTrue_WhenCapacityValidIntegerAboveZero() {
        //arrange
        String capacity = String.valueOf((int) (Math.random() * Integer.MAX_VALUE));
        //act
        boolean actual = validCapacity.isValid(capacity, mockContext);
        //assert
        Assertions.assertTrue(actual);
    }

    @Test
    public void isValid_ShouldReturnFalse_CapacityNotANumber_WithAppropriateMessage() {
        //arrange
        String capacity = "invalid";
        Mockito.when(mockContext.buildConstraintViolationWithTemplate(MUST_BE_A_VALID_INTEGER))
                .thenReturn(mockViolationBuilder);
        //act
        boolean actual = validCapacity.isValid(capacity, mockContext);
        //assert
        Assertions.assertFalse(actual);
        Mockito.verify(mockContext, times(1))
                .disableDefaultConstraintViolation();
        Mockito.verify(mockViolationBuilder, times(1))
                .addConstraintViolation();
    }

    @Test
    public void isValid_ShouldReturnFalse_CapacityNotAnInteger_WithAppropriateMessage() {
        //arrange
        String capacity = "220.443";
        Mockito.when(mockContext.buildConstraintViolationWithTemplate(MUST_BE_A_VALID_INTEGER))
                .thenReturn(mockViolationBuilder);
        //act
        boolean actual = validCapacity.isValid(capacity, mockContext);
        //assert
        Mockito.verify(mockContext, times(1))
                .disableDefaultConstraintViolation();
        Mockito.verify(mockViolationBuilder, times(1))
                .addConstraintViolation();
        Assertions.assertFalse(actual);
    }

    @Test
    public void isValid_ShouldReturnFalse_CapacityNegative() {
        //arrange
        String capacity = String.valueOf(-1);
        //act
        boolean actual = validCapacity.isValid(capacity, mockContext);
        //assert
        Assertions.assertFalse(actual);
    }

    private static class ValidMaxAgeTestClass implements ValidCapacity {


        @Override
        public String message() {
            return "Test Message";
        }

        @Override
        public Class<?>[] groups() {
            return new Class[]{};
        }

        @Override
        public Class<? extends Payload>[] payload() {
            return new Class[0];
        }

        @Override
        public Class<? extends Annotation> annotationType() {
            return ValidCapacity.class;
        }

    }
}
