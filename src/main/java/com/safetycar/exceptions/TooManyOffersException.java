package com.safetycar.exceptions;

public class TooManyOffersException extends RuntimeException {

    public static final int MAXIMUM_OFFERS_ALLOWED = 5;

    public TooManyOffersException() {
        super("Too many offers to consider!");
    }

    public TooManyOffersException(String message) {
        super(message);
    }

}
