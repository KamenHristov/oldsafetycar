package com.safetycar.exceptions;

public class InvalidOldPasswordException extends RuntimeException {

    public static final String DEFAULT_MESSAGE = "Invalid old password!";

    public InvalidOldPasswordException() {
        super(DEFAULT_MESSAGE);
    }

    public InvalidOldPasswordException(String message) {
        super(message);
    }
}
