package com.safetycar.exceptions;

public class SamePasswordException extends RuntimeException {

    private static final String DEFAULT_MESSAGE = "New password same as old one!";

    public SamePasswordException() {
        super(DEFAULT_MESSAGE);
    }

}
