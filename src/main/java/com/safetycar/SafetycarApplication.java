package com.safetycar;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import springfox.documentation.swagger2.annotations.EnableSwagger2;


@EnableSwagger2
@SpringBootApplication
@EnableTransactionManagement
@EnableJpaRepositories(basePackages = { "com.safetycar.repositories" })
@EntityScan({ "com.safetycar.models" })
public class SafetycarApplication {

    public static void main(String[] args) {
        SpringApplication.run(SafetycarApplication.class, args);
    }

}
