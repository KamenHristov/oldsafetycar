package com.safetycar.validation.impl;

import com.safetycar.validation.PasswordMatches;
import com.safetycar.web.dto.user.CreateUserDto;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class PasswordMatchesImpl implements ConstraintValidator<PasswordMatches, Passwordable> {

    @Override
    public boolean isValid(final Passwordable passwordable, final ConstraintValidatorContext context) {
        if (passwordable == null) return false;
        return passwordable.getPassword().equals(passwordable.getConfirmationPassword());
    }

}
