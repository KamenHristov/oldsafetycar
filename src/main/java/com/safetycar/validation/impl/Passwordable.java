package com.safetycar.validation.impl;

public interface Passwordable {

    String getPassword();

    String getConfirmationPassword();
}
