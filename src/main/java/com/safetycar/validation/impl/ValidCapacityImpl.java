package com.safetycar.validation.impl;

import com.safetycar.validation.ValidCapacity;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import static com.safetycar.util.Constants.BaseAmountConstants.CAPACITY;

public class ValidCapacityImpl implements ConstraintValidator<ValidCapacity, String> {

    public static final String MUST_BE_A_VALID_INTEGER = "Capacity must be a valid integer.";

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        if (value == null) return false;
        try {
            int capacity = Integer.parseInt(value);
            return capacity >= 0;
        } catch (NumberFormatException e) {
            context.disableDefaultConstraintViolation();
            context.buildConstraintViolationWithTemplate(MUST_BE_A_VALID_INTEGER)
                    .addConstraintViolation();
            return false;
        }
    }
}
