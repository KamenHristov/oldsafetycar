package com.safetycar.services.contracts;

import com.safetycar.models.Offer;
import com.safetycar.models.Policy;
import com.safetycar.models.User;
import com.safetycar.models.UserDetails;
import org.springframework.security.access.AccessDeniedException;

public interface AuthorisationService {

    void authorise(User user, Offer offer) throws AccessDeniedException;

    void authorise(User user, Policy policy) throws AccessDeniedException;

    void authorise(UserDetails details, User detailsOwnerOrAdmin) throws AccessDeniedException;

    void authoriseNotAdmin(User mustNotBeAdmin, String errorMessage) throws AccessDeniedException;

}
