package com.safetycar.services.contracts.base;

import com.safetycar.exceptions.EntityNotFoundException;
import com.safetycar.repositories.base.SafetyCarRepository;
import org.springframework.data.repository.CrudRepository;

public interface GetService {

    /**
     * @param repository  {@link com.safetycar.repositories.base.SafetyCarRepository}<T, ID>
     * @param id          ID entity attribute mapped with @Id
     * @param entityClass Class<T>
     * @param <T>         type of Entity
     * @return an entity of type T with @id of type ID
     * throws EntityNotFoundException if @id doesn't exist
     */
    <T, ID> T getFromRepository(SafetyCarRepository<T, ID> repository,
                                ID id,
                                Class<T> entityClass) throws EntityNotFoundException;

    /**
     * @param repository {@link com.safetycar.repositories.base.SafetyCarRepository}<T, ID>
     * @param id         ID entity attribute mapped with @Id
     * @param message    String
     * @param <T>        type of Entity
     * @return an entity of type T with @id of type ID
     * throws EntityNotFoundException with @message
     * if @id doesn't exist
     */
    <T, ID> T getFromRepository(SafetyCarRepository<T, ID> repository,
                                ID id,
                                String message) throws EntityNotFoundException;

}
