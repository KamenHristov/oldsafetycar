package com.safetycar.services.contracts.base;

import com.safetycar.exceptions.EntityNotFoundException;
import com.safetycar.repositories.base.SafetyCarRepository;
import org.springframework.data.jpa.domain.Specification;

public interface FindService {

    /**
     * @param repository {@link com.safetycar.repositories.base.SafetyCarRepository}<T, ID>
     * @param spec       {@link org.springframework.data.jpa.domain.Specification}<T>
     * @param message    custom message
     * @param <T>        type of return entity
     * @return returns an entity found by @spec criteria
     * or throws EntityNotFoundException with @message
     * if it doesn't find a match for @spec
     */
    <T, ID> T findFromRepository(SafetyCarRepository<T,ID> repository,
                                 Specification<T> spec,
                                 String message) throws EntityNotFoundException;
}
