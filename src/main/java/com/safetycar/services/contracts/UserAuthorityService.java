package com.safetycar.services.contracts;

import com.safetycar.models.User;

public interface UserAuthorityService {

    void assignUserAuthority(User user);

    void assignAgentAuthority(User user);
}
