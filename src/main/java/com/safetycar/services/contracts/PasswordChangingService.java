package com.safetycar.services.contracts;

import com.safetycar.exceptions.InvalidOldPasswordException;
import com.safetycar.exceptions.SamePasswordException;
import com.safetycar.models.PasswordToken;
import com.safetycar.models.User;

public interface PasswordChangingService {

    void changePassword(User user, String newPassword, String oldPassword)
            throws SamePasswordException, InvalidOldPasswordException;

    void changeForgottenPassword(User user, String newPassword);

}
