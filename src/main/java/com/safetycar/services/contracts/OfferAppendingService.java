package com.safetycar.services.contracts;

import com.safetycar.exceptions.DuplicateEntityException;
import com.safetycar.exceptions.TooManyOffersException;
import com.safetycar.models.Offer;
import com.safetycar.models.User;

public interface OfferAppendingService {
    /**
     * @param offer Offer to preserve in @user's personal offer list
     * @param user  non-admin user
     *              Adds the offer to the user's offer list
     *              Throws {@link com.safetycar.exceptions.TooManyOffersException}
     *              when user exceeds maximum allowed number of offers after appending.
     *              Throws {@link com.safetycar.exceptions.DuplicateEntityException}
     *              when user already has an offer or policy for the same car.
     */
    void appendOfferAndRecordHistory(Offer offer, User user) throws TooManyOffersException, DuplicateEntityException;

}
