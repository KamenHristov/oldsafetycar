package com.safetycar.services.contracts;

import com.safetycar.models.User;
import com.safetycar.models.UserDetails;
import com.safetycar.services.contracts.base.*;

import java.util.List;
import java.util.Map;

public interface UserService extends
        CreateService<User>, UpdateService<User>, GetAll<User>, GetService {

    boolean existsByMail(String email);

    User userByEmail(String email);

    User getUserByToken(String verificationToken);

    UserDetails getUserDetails(Integer id);

    List<UserDetails> getAllDetails();

    List<UserDetails> searchDetails(Map<String, String> filter);

}
