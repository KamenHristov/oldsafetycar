package com.safetycar.services.contracts;

import com.safetycar.exceptions.EntityNotFoundException;
import com.safetycar.models.PasswordToken;
import com.safetycar.models.User;
import com.safetycar.services.contracts.base.CreateService;
import com.safetycar.services.contracts.base.DeleteService;

public interface PasswordTokenService extends CreateService<PasswordToken>, DeleteService<PasswordToken> {

    PasswordToken findForgottenPasswordToken(String token);

    PasswordToken getNewUnsavedToken(User user);

    void validateToken(String token) throws EntityNotFoundException;

}
