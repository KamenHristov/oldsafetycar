package com.safetycar.services.contracts;

import com.safetycar.models.VerificationToken;
import com.safetycar.models.User;
import com.safetycar.services.contracts.base.CreateService;

public interface VerificationTokenService extends CreateService<VerificationToken> {

    VerificationToken findVerificationToken(String VerificationToken);

    VerificationToken getNewUnsavedToken(User user);

}
