package com.safetycar.services.contracts;

import com.safetycar.models.User;

public interface UserHistoryService {

    void appendHistoryOnRegistration(User user);

    void appendHistoryOnOfferCreation(User user);

    void appendHistoryOnDetailsUpdate(User user);

}
