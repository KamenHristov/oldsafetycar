package com.safetycar.services.contracts;

import com.safetycar.models.Policy;
import com.safetycar.models.User;

public interface PolicyHistoryService {

    void appendHistory(User actor, Policy policy, String action, String message);

}
