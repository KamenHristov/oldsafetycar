package com.safetycar.services.contracts;

import com.safetycar.models.Policy;
import com.safetycar.web.wrapper.TokenWrapper;

public interface MailService {

    void sendConfirmationMail(TokenWrapper wrapper);

    void sendConfirmationMailRest(TokenWrapper wrapper);

    void sendPolicyStatusMail(Policy policy);

    void sendPasswordChangeRequest(TokenWrapper wrapper);

    void sendUnhandledErrorMail(Exception e);

}
