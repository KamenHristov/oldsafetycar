package com.safetycar.services;

import com.safetycar.exceptions.InvalidOldPasswordException;
import com.safetycar.exceptions.SamePasswordException;
import com.safetycar.models.PasswordToken;
import com.safetycar.models.User;
import com.safetycar.repositories.UserRepository;
import com.safetycar.services.contracts.PasswordChangingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class PasswordChangingServiceImpl implements PasswordChangingService {

    public static final String ADMINS = "admins";
    public static final String THIS_PASSWORD_CANNOT_BE_CHANGED = "This password cannot be changed!";
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;

    @Autowired
    public PasswordChangingServiceImpl(UserRepository userRepository,
                                       PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public void changePassword(User user, String newPassword, String oldPassword) {
        ensureAdminsDemoAccountCannotChangeItsPassword(user);
        ensureOldPassSameAsCurrent(user, oldPassword);
        ensureNewPassNotSameAsOld(user, newPassword);
        changePassword(user, newPassword);
    }

    private void changePassword(User user, String newPassword) {
        user.setPassword(passwordEncoder.encode(newPassword));
        userRepository.save(user);
    }

    private void ensureNewPassNotSameAsOld(User user, String newPassword) {
        if (passwordEncoder.matches(newPassword, user.getPassword())) {
            throw new SamePasswordException();
        }
    }

    private void ensureOldPassSameAsCurrent(User user, String oldPassword) {
        if (!passwordEncoder.matches(oldPassword, user.getPassword())) {
            throw new InvalidOldPasswordException();
        }
    }

    @Override
    public void changeForgottenPassword(User user, String newPassword) {
        ensureAdminsDemoAccountCannotChangeItsPassword(user);
        changePassword(user, newPassword);
    }

    private void ensureAdminsDemoAccountCannotChangeItsPassword(User user) {
        if (user.getUserName().equals(ADMINS)) {
            throw new InvalidOldPasswordException(THIS_PASSWORD_CANNOT_BE_CHANGED);
        }
    }

}
