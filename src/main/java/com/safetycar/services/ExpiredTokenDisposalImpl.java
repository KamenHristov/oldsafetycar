package com.safetycar.services;

import com.safetycar.models.VerificationToken;
import com.safetycar.models.User;
import com.safetycar.repositories.AuthorityRepository;
import com.safetycar.repositories.VerificationTokenRepository;
import com.safetycar.repositories.UserRepository;
import com.safetycar.services.contracts.ExpiredTokenDisposal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.util.HashSet;

@Component
public class ExpiredTokenDisposalImpl implements ExpiredTokenDisposal {

    private final VerificationTokenRepository verificationTokenRepository;
    private final AuthorityRepository authorityRepository;
    private final UserRepository userRepository;

    @Autowired
    public ExpiredTokenDisposalImpl(VerificationTokenRepository verificationTokenRepository,
                                    AuthorityRepository authorityRepository,
                                    UserRepository userRepository) {
        this.verificationTokenRepository = verificationTokenRepository;
        this.authorityRepository = authorityRepository;
        this.userRepository = userRepository;
    }

    @Transactional
    @Override
    public void clearExpiredAndDeleteInactiveUser(VerificationToken verificationToken) {
        User user = verificationToken.getUser();
        verificationTokenRepository.delete(verificationToken);
        if (user.isDisabled()) {
            deleteUser(user);
        }
    }

    private void deleteUser(User user) {
        authorityRepository.deleteAuthority(user.getUserName());
        user.setAuthorities(new HashSet<>());
        userRepository.save(user);
        userRepository.delete(user);
    }
}
