package com.safetycar.services;

import com.safetycar.models.Policy;
import com.safetycar.models.User;
import com.safetycar.models.base.Token;
import com.safetycar.services.contracts.MailService;
import com.safetycar.web.wrapper.TokenWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.MessageSource;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import javax.validation.constraints.NotNull;

import java.util.Arrays;

import static com.safetycar.util.Constants.ConfigConstants.*;

@Service
public class MailServiceImpl implements MailService {

    public static final String REGISTRATION_CONFIRMATION = "Registration Confirmation";
    public static final String CONFIRMATION_RETURN_URL = "/register/confirm?token=";
    public static final String MESSAGE_REG_SUCC = "message.regSucc";
    public static final String MESSAGE_REG_SUCC_REST = "message.regSucc.rest";
    public static final String POLICY_STATUS_CHANGE = "Policy status change";
    public static final String YOUR_POLICY_S_STATUS_HAS_BEEN_CHANGED_TO = "Your policy's status has been changed to ";
    public static final String MESSAGE_FORGOTTEN_PASS = "message.forgottenPass";
    public static final String CHANGE_FORGOTTEN_PASSWORD = "Change Forgotten Password";
    public static final String CHANGE_PASSWORD_RETURN_URL = "/change-password/verification?token=";
    public static final String NEW_LINE = "\r\n";
    public static final String SPACE_COLUMN_SPACE = " : ";
    public static final String ERROR_MAIL = "some.email@mail.com";

    private final MessageSource messages;
    private final JavaMailSender javaMailSender;

    @Autowired
    public MailServiceImpl(@Qualifier(MESSAGE_SOURCE) MessageSource messages,
                           JavaMailSender javaMailSender) {
        this.messages = messages;
        this.javaMailSender = javaMailSender;
    }

    @Override
    public void sendConfirmationMail(TokenWrapper wrapper) {
        SimpleMailMessage email = buildConfirmationMail(wrapper);
        javaMailSender.send(email);
    }

    @NotNull
    private SimpleMailMessage buildConfirmationMail(TokenWrapper wrapper) {
        Token verificationToken = wrapper.getToken();

        String recipientAddress = verificationToken.getUser().getUserName();
        String confirmationUrl = getReturnUrl(wrapper, CONFIRMATION_RETURN_URL, verificationToken.getToken());
        String message = getMessage(wrapper);
        String text = message + NEW_LINE + confirmationUrl;

        return buildSimpleMailMessage(recipientAddress, text, REGISTRATION_CONFIRMATION);
    }

    @Override
    public void sendConfirmationMailRest(TokenWrapper wrapper) {
        SimpleMailMessage email = buildConfirmationMailRest(wrapper);
        javaMailSender.send(email);
    }

    @NotNull
    private SimpleMailMessage buildConfirmationMailRest(TokenWrapper wrapper) {
        Token verificationToken = wrapper.getToken();

        String recipientAddress = verificationToken.getUser().getUserName();
        String message = getMessageRest(wrapper);
        String text = message + SPACE_COLUMN_SPACE + verificationToken.getToken();

        return buildSimpleMailMessage(recipientAddress, text, REGISTRATION_CONFIRMATION);
    }

    @Override
    public void sendPolicyStatusMail(Policy policy) {
        SimpleMailMessage email = buildPolicyStatusMail(policy);
        javaMailSender.send(email);
    }

    @NotNull
    private SimpleMailMessage buildPolicyStatusMail(Policy policy) {
        User owner = policy.getOwner();
        String recipientAddress = owner.getUserName();
        String text = getMessage(policy);
        return buildSimpleMailMessage(recipientAddress, text, POLICY_STATUS_CHANGE);
    }

    @NotNull
    private String getMessage(Policy policy) {
        return YOUR_POLICY_S_STATUS_HAS_BEEN_CHANGED_TO + policy.getStatus().getStatus();
    }

    @Override
    public void sendPasswordChangeRequest(TokenWrapper wrapper) {
        SimpleMailMessage email = buildForgottenPassMail(wrapper);
        javaMailSender.send(email);
    }
    @NotNull
    private SimpleMailMessage buildForgottenPassMail(TokenWrapper wrapper) {
        Token verificationToken = wrapper.getToken();

        String recipientAddress = verificationToken.getUser().getUserName();
        String confirmationUrl = getReturnUrl(wrapper, CHANGE_PASSWORD_RETURN_URL, verificationToken.getToken());
        String message = getForgottenPassMessage(wrapper);
        String text = message + NEW_LINE + confirmationUrl;

        return buildSimpleMailMessage(recipientAddress, text, CHANGE_FORGOTTEN_PASSWORD);
    }

    private SimpleMailMessage buildSimpleMailMessage(String recipientAddress, String message, String policyStatusChange) {
        SimpleMailMessage email = new SimpleMailMessage();
        email.setTo(recipientAddress);
        email.setSubject(policyStatusChange);
        email.setText(message);
        return email;
    }

    @NotNull
    private String getMessage(TokenWrapper wrapper) {
        return messages.getMessage(MESSAGE_REG_SUCC, null, wrapper.getLocale());
    }

    @NotNull
    private String getForgottenPassMessage(TokenWrapper wrapper) {
        return messages.getMessage(MESSAGE_FORGOTTEN_PASS, null, wrapper.getLocale());
    }

    @NotNull
    private String getMessageRest(TokenWrapper wrapper) {
        return messages.getMessage(MESSAGE_REG_SUCC_REST, null, wrapper.getLocale());
    }

    @NotNull
    private String getReturnUrl(TokenWrapper wrapper, String returnUrl, String token) {
        return wrapper.getAppUrl() + returnUrl + token;
    }

    @Override
    public void sendUnhandledErrorMail(Exception e) {
        SimpleMailMessage mailMessage = getSimpleMailMessage(e);
        javaMailSender.send(mailMessage);
    }

    private SimpleMailMessage getSimpleMailMessage(Exception e) {
        SimpleMailMessage mailMessage = new SimpleMailMessage();
        mailMessage.setTo(ERROR_MAIL);
        mailMessage.setSubject("error");
        mailMessage.setText(e.toString() + "\n" + e.getMessage() + "\n" + Arrays.toString(e.getStackTrace()));
        return mailMessage;
    }
}
