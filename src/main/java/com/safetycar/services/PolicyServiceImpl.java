package com.safetycar.services;

import com.safetycar.models.Policy;
import com.safetycar.repositories.PolicyRepository;
import com.safetycar.repositories.filter.base.MapBasedSpecification;
import com.safetycar.services.contracts.PolicyService;
import com.safetycar.services.contracts.base.GetService;
import com.safetycar.services.contracts.base.GetServiceBase;
import com.safetycar.services.factories.contracts.PolicySpecFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import static com.safetycar.repositories.filter.base.BaseMapSpec.SORT_PARAMETER;
import static com.safetycar.util.Constants.QueryConstants.ID;

@Service
public class PolicyServiceImpl extends GetServiceBase implements PolicyService, GetService {


    private final PolicyRepository policyRepository;
    private final PolicySpecFactory specFactory;


    @Autowired
    public PolicyServiceImpl(PolicyRepository policyRepository,
                             PolicySpecFactory specFactory) {
        this.policyRepository = policyRepository;
        this.specFactory = specFactory;
    }

    @Override
    public void create(Policy policy) {
        policyRepository.save(policy);
    }

    @Override
    public List<Policy> getAll() {
        return getAllSortedByIdAsc();
    }

    private List<Policy> getAllSortedByIdAsc() {
        MapBasedSpecification<Policy> spec = specFactory.getSpec();
        spec.addSpec(SORT_PARAMETER, ID);
        Sort sort = spec.getSort();
        return policyRepository.findAll(spec, sort);
    }

    @Override
    public Policy getOne(Integer id) {
        return getFromRepository(policyRepository, id, Policy.class);
    }

    @Override
    public void update(Policy policy) {
        policyRepository.save(policy);
    }

    @Override
    public List<Policy> searchMyPolicies(Collection<Policy> myPolicies, Map<String, String> search) {
        MapBasedSpecification<Policy> spec = specFactory.getSpec(myPolicies, search);
        Sort sort = spec.getSort();
        return policyRepository.findAll(spec, sort);
    }
}
