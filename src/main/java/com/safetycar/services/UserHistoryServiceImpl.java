package com.safetycar.services;

import com.safetycar.models.History;
import com.safetycar.models.User;
import com.safetycar.models.UserDetails;
import com.safetycar.services.contracts.UserHistoryService;
import org.springframework.stereotype.Service;

@Service
public class UserHistoryServiceImpl implements UserHistoryService {

    public static final String REGISTRATION = "Registration";
    public static final String S_HAS_REGISTERED = "%s has registered.";
    public static final String OFFER_CREATION = "Offer Creation";
    public static final String AN_OFFER_WAS_CREATED_FOR_S = "An offer was created for %s.";
    public static final String DETAILS_UPDATED = "Details Updated";
    public static final String S_HAS_UPDATED_THEIR_DETAILS = "%s has updated their details.";

    @Override
    public void appendHistoryOnRegistration(User user) {
        History history = getHistory(user, REGISTRATION,
                S_HAS_REGISTERED);
        user.addHistory(history);
    }

    @Override
    public void appendHistoryOnOfferCreation(User user) {
        History history = getHistory(user, OFFER_CREATION,
                AN_OFFER_WAS_CREATED_FOR_S);
        user.addHistory(history);
    }

    @Override
    public void appendHistoryOnDetailsUpdate(User user) {
        History history = getHistory(user, DETAILS_UPDATED,
                S_HAS_UPDATED_THEIR_DETAILS);
        user.addHistory(history);
    }

    private History getHistory(User user, String action, String historyMessage) {
        History history = new History();
        UserDetails userDetails = user.getUserDetails();
        String email = userDetails.getUserName();
        history.setAction(action);
        history.setActor(userDetails);
        history.setHistory(String.format(historyMessage, email));
        return history;
    }
}
