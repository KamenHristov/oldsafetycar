package com.safetycar.services;

import com.safetycar.models.Offer;
import com.safetycar.models.VerificationToken;
import com.safetycar.models.User;
import com.safetycar.models.UserDetails;
import com.safetycar.models.wrappers.UserDuplicateWrapper;
import com.safetycar.security.recaptcha.SimpleRecaptchaService;
import com.safetycar.services.contracts.*;
import com.safetycar.web.dto.mappers.contracts.UserMapper;
import com.safetycar.web.dto.user.CreateDetailsDto;
import com.safetycar.web.dto.user.CreateUserDto;
import com.safetycar.web.wrapper.TokenWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import static com.safetycar.util.Constants.OfferConstants.TRANSFER_OFFER;
import static com.safetycar.util.Helper.getAppUrl;


@Service
public class AccountManagerImpl implements AccountManager {

    private final UserService userService;
    private final UserMapper userMapper;
    private final MailService mailService;
    private final OfferAppendingService offerAppendingService;
    private final VerificationTokenService tokenService;
    private final SimpleRecaptchaService simpleRecaptchaService;
    private final UserHistoryService userHistoryService;

    @Autowired
    public AccountManagerImpl(UserService userService,
                              UserMapper userMapper,
                              MailService mailService,
                              OfferAppendingService offerAppendingService,
                              VerificationTokenService tokenService,
                              SimpleRecaptchaService simpleRecaptchaService,
                              UserHistoryService userHistoryService) {
        this.userService = userService;
        this.userMapper = userMapper;
        this.mailService = mailService;
        this.offerAppendingService = offerAppendingService;
        this.tokenService = tokenService;
        this.simpleRecaptchaService = simpleRecaptchaService;
        this.userHistoryService = userHistoryService;
    }

    @Override
    @Transactional
    public void registerUserAndRecordHistory(CreateUserDto userDto,
                                             Offer offerToAppend,
                                             HttpServletRequest request,
                                             String recaptchaResponse) {
        simpleRecaptchaService.validateRecaptcha(recaptchaResponse);

        User userToRegister = userMapper.fromDto(userDto);

        offerAppendingService.appendOfferAndRecordHistory(offerToAppend, userToRegister);

        clearSessionFromOffer(request);

        userHistoryService.appendHistoryOnRegistration(userToRegister);
        userService.create(userToRegister);
        sendActivationMail(request, userToRegister);
    }

    private void clearSessionFromOffer(HttpServletRequest request) {
        HttpSession session = request.getSession();
        session.removeAttribute(TRANSFER_OFFER);
    }

    private void sendActivationMail(HttpServletRequest request, User safetyCarUser) {
        TokenWrapper wrapper = prepareRegistrationMailWrapper(request, safetyCarUser);
        mailService.sendConfirmationMail(wrapper);
    }

    @Override
    public void registerUserAndRecordHistoryRest(CreateUserDto userDto,
                                                 HttpServletRequest request) {
        User userToRegister = userMapper.fromDto(userDto);
        userHistoryService.appendHistoryOnRegistration(userToRegister);
        userService.create(userToRegister);
        sendActivationMailRest(request, userToRegister);
    }

    private void sendActivationMailRest(HttpServletRequest request, User userToRegister) {
        TokenWrapper wrapper = prepareRegistrationMailWrapper(request, userToRegister);
        mailService.sendConfirmationMailRest(wrapper);
    }

    @Override
    public CreateDetailsDto getDetailsToEdit(String userName) {
        User user = userService.userByEmail(userName);
        return userMapper.toDto(user);
    }

    @Override
    public void updateDetailsAndRecordHistory(CreateDetailsDto dto, String userName) {
        User user = userService.userByEmail(userName);
        UserDuplicateWrapper checkForChanges = new UserDuplicateWrapper(user);

        UserDetails oldDetails = user.getUserDetails();
        UserDetails updatedDetails = userMapper.updateFromDto(dto, oldDetails);
        user.setUserDetails(updatedDetails);

        appendHistoryIfDetailsChanged(user, checkForChanges, updatedDetails);
        userService.update(user);
    }

    private void appendHistoryIfDetailsChanged(User user, UserDuplicateWrapper checkForChanges, UserDetails updatedDetails) {
        if (checkForChanges.detailsHaveChanged(updatedDetails)) {
            userHistoryService.appendHistoryOnDetailsUpdate(user);
        }
    }

    public VerificationToken getVerificationToken(String token) {
        return tokenService.findVerificationToken(token);
    }

    public void activateAcc(VerificationToken verificationToken) {
        User user = verificationToken.getUser();
        user.setEnabled(true);
        userService.update(user);
    }

    private TokenWrapper prepareRegistrationMailWrapper(HttpServletRequest request, User userToRegister) {
        String appUrl = getAppUrl(request);
        VerificationToken newToken = tokenService.getNewUnsavedToken(userToRegister);
        tokenService.create(newToken);

        return new TokenWrapper(appUrl, request.getLocale(), newToken);
    }

}
