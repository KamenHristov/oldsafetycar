package com.safetycar.services;

import com.safetycar.models.Brand;
import com.safetycar.repositories.ModelRepository;
import com.safetycar.models.Model;
import com.safetycar.services.contracts.BrandService;
import com.safetycar.services.contracts.ModelService;
import com.safetycar.services.contracts.base.GetService;
import com.safetycar.services.contracts.base.GetServiceBase;
import com.safetycar.services.factories.contracts.AnonymousSpecFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import static com.safetycar.util.Constants.QueryConstants.BRAND;
import static com.safetycar.util.Constants.QueryConstants.NAME;

@Service
public class ModelServiceImpl extends GetServiceBase implements ModelService, GetService {

    public static final String YEAR = "year";

    private final ModelRepository modelRepository;
    private final BrandService brandService;
    private final AnonymousSpecFactory specFactory;

    @Autowired
    public ModelServiceImpl(ModelRepository modelRepository,
                            BrandService brandService,
                            AnonymousSpecFactory specFactory) {
        this.modelRepository = modelRepository;
        this.brandService = brandService;
        this.specFactory = specFactory;
    }

    @Override
    public List<Model> getAll() {
        return modelRepository.findAll();
    }

    @Override
    public Model getOne(Integer id) {
        return getFromRepository(modelRepository, id, Model.class);
    }

    @Override
    public List<Model> filter(int brandId) {
        Brand brand = brandService.getOne(brandId);
        Specification<Model> spec = specFactory.getEqualAnonymousSpec(BRAND, brand, Model.class);
        Sort sort = getModelSort();
        return modelRepository.findAll(spec, sort);
    }

    private Sort getModelSort() {
        List<Sort.Order> orders = new LinkedList<>();
        orders.add(Sort.Order.asc(NAME));
        orders.add(Sort.Order.asc(YEAR));
        return Sort.by(orders);
    }

    @Override
    public List<String> getModelNamesByBrandId(int id) {
        Brand brand = brandService.getOne(id);
        Specification<Model> spec = specFactory.getEqualAnonymousSpec(BRAND, brand, Model.class);
        Collection<Model> modelsCollection = modelRepository.findAll(spec);

        List<String> modelsList = new ArrayList<>();
        for (Model model : modelsCollection) {
            modelsList.add(model.getName());
        }
        return modelsList;
    }

}
