package com.safetycar.services;

import com.safetycar.enums.UserRoles;
import com.safetycar.models.Authority;
import com.safetycar.models.User;
import com.safetycar.services.contracts.UserAuthorityService;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
public class UserAuthorityServiceImpl implements UserAuthorityService {

    @Override
    public void assignUserAuthority(User user) {
        user.setAuthorities(getUserRoles(user));
    }

    @Override
    public void assignAgentAuthority(User user) {
        user.setAuthorities(getAgentRoles(user));
    }

    private Set<Authority> getUserRoles(User user) {
        Set<Authority> result = new HashSet<>();
        Authority authority = new Authority();
        authority.setUserName(user.getUserName());
        authority.setAuthority(UserRoles.ROLE_USER.name());
        result.add(authority);
        return result;
    }

    private Set<Authority> getAgentRoles(User user) {
        Set<Authority> result = new HashSet<>();
        Authority authority = new Authority();
        authority.setUserName(user.getUserName());
        authority.setAuthority(UserRoles.ROLE_AGENT.name());
        result.add(authority);
        return result;
    }
}
