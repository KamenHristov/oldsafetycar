package com.safetycar.services;

import com.safetycar.exceptions.EntityNotFoundException;
import com.safetycar.models.PasswordToken;
import com.safetycar.models.User;
import com.safetycar.repositories.PasswordTokenRepository;
import com.safetycar.services.contracts.PasswordTokenService;
import com.safetycar.services.contracts.base.FindService;
import com.safetycar.services.contracts.base.FindServiceBase;
import com.safetycar.services.factories.contracts.AnonymousSpecFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.UUID;

import static com.safetycar.util.Constants.ErrorsConstants.INVALID_TOKEN;
import static com.safetycar.web.controllers.mvc.MvcRegisterController.TOKEN;

@Service
public class PasswordTokenServiceImpl extends FindServiceBase implements FindService, PasswordTokenService {

    private final PasswordTokenRepository tokenRepository;
    private final AnonymousSpecFactory specFactory;

    @Autowired
    public PasswordTokenServiceImpl(PasswordTokenRepository tokenRepository,
                                    AnonymousSpecFactory specFactory) {
        this.tokenRepository = tokenRepository;
        this.specFactory = specFactory;
    }

    @Override
    public void create(PasswordToken token) {
        tokenRepository.save(token);
    }

    @Override
    public PasswordToken findForgottenPasswordToken(final String token) {
        Specification<PasswordToken> spec =
                specFactory.getEqualAnonymousSpec(TOKEN, token, PasswordToken.class);
        return findFromRepository(tokenRepository,
                spec, INVALID_TOKEN);
    }

    @Override
    public PasswordToken getNewUnsavedToken(User user) {
        PasswordToken token = new PasswordToken();
        token.setToken(UUID.randomUUID().toString());
        token.setUser(user);
        return token;
    }

    @Override
    public void validateToken(String token) {
        if (!tokenRepository.existsByToken(token)) {
            throw new EntityNotFoundException(INVALID_TOKEN);
        }
    }

    @Override
    public void delete(PasswordToken token) {
        tokenRepository.delete(token);
    }
}
