package com.safetycar.services;

import com.safetycar.exceptions.AccountNotActivatedException;
import com.safetycar.exceptions.DuplicateEntityException;
import com.safetycar.models.VerificationToken;
import com.safetycar.models.User;
import com.safetycar.models.UserDetails;
import com.safetycar.repositories.UserDetailsRepository;
import com.safetycar.repositories.UserRepository;
import com.safetycar.repositories.filter.base.MapBasedSpecification;
import com.safetycar.services.contracts.UserService;
import com.safetycar.services.contracts.VerificationTokenService;
import com.safetycar.services.contracts.base.GetServiceBase;
import com.safetycar.services.factories.contracts.UserSpecFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;

import static com.safetycar.repositories.filter.UserSpec.ACTIVE;
import static com.safetycar.repositories.filter.UserSpec.TRUE;
import static com.safetycar.repositories.filter.base.BaseMapSpec.SORT_PARAMETER;
import static com.safetycar.util.Constants.ErrorsConstants.*;
import static com.safetycar.util.Constants.QueryConstants.ID;

@Service
public class UserServiceImpl extends GetServiceBase implements UserService {

    public static final String MESSAGE_NOT_ACTIVATED = "auth.message.notActivated";

    private final UserRepository userRepository;
    private final UserDetailsRepository detailsRepository;
    private final VerificationTokenService tokenService;
    private final UserSpecFactory specFactory;
    private final MessageSource messageSource;

    @Autowired
    public UserServiceImpl(UserRepository userRepository,
                           UserDetailsRepository detailsRepository,
                           VerificationTokenService tokenService,
                           UserSpecFactory specFactory,
                           MessageSource messageSource) {
        this.userRepository = userRepository;
        this.detailsRepository = detailsRepository;
        this.tokenService = tokenService;
        this.specFactory = specFactory;
        this.messageSource = messageSource;
    }

    @Override
    public void create(User user) {
        ensureNewUserNotDuplicate(user);
        userRepository.save(user);
    }

    private void ensureNewUserNotDuplicate(User user) {
        if (userRepository.existsByUserName(user.getUserName())) {
            throw new DuplicateEntityException(EMAIL_ALREADY_EXISTS);
        }
    }

    @Override
    public User userByEmail(String email) {
        User user = getFromRepository(userRepository, email, NO_USER_LIKE_THIS_WAS_FOUND);
        ensureActive(user);
        return user;
    }

    private void ensureActive(User user) {
        if (user.isDisabled()) {
            throw new AccountNotActivatedException(messageSource.getMessage(MESSAGE_NOT_ACTIVATED, null, Locale.ENGLISH));
        }
    }

    @Override
    public void update(User user) {
        ensureNotDuplicate(user);
        userRepository.save(user);
    }

    private void ensureNotDuplicate(User user) {
        if (userRepository.existsByUserDetails(user.getUserDetails())) {
            throw new DuplicateEntityException(DUPLICATE_DETAILS_ERROR);
        }
    }

    @Override
    public List<User> getAll() {
        return userRepository.findAll();
    }

    @Override
    public User getUserByToken(String token) {
        VerificationToken verificationToken = tokenService.findVerificationToken(token);
        return verificationToken.getUser();
    }

    @Override
    public boolean existsByMail(String email) {
        return userRepository.existsByUserName(email);
    }

    @Override
    public UserDetails getUserDetails(Integer id) {
        return getFromRepository(detailsRepository, id, UserDetails.class);
    }

    @Override
    public List<UserDetails> getAllDetails() {
        MapBasedSpecification<User> spec = specFactory.getSpec();
        spec.addSpec(SORT_PARAMETER, ID);
        spec.addSpec(ACTIVE,TRUE);
        Sort sort = spec.getSort();
        List<User> all = userRepository.findAll(spec, sort);
        return all.stream()
                .map(User::getUserDetails)
                .collect(Collectors.toList());
    }

    @Override
    public List<UserDetails> searchDetails(Map<String, String> filter) {
        MapBasedSpecification<User> spec = specFactory.getSpec(filter);
        Sort sort = spec.getSort();
        return userRepository.findAll(spec, sort).stream()
                .map(User::getUserDetails)
                .collect(Collectors.toList());
    }

}
