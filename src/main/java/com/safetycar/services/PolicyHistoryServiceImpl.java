package com.safetycar.services;

import com.safetycar.models.History;
import com.safetycar.models.Policy;
import com.safetycar.models.User;
import com.safetycar.services.contracts.PolicyHistoryService;
import com.sun.istack.NotNull;
import org.springframework.stereotype.Service;

@Service
public class PolicyHistoryServiceImpl implements PolicyHistoryService {

    @Override
    public void appendHistory(User actor, Policy policy, String action, String message) {
        History history = getHistory(actor, action, message);
        policy.addHistory(history);
    }

    @NotNull
    private History getHistory(User user, String action, String message) {
        History history = new History();
        history.setActor(user.getUserDetails());
        history.setHistory(message);
        history.setAction(action);
        return history;
    }

}
