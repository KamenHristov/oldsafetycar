package com.safetycar.services;

import com.safetycar.models.Offer;
import com.safetycar.models.Policy;
import com.safetycar.models.User;
import com.safetycar.models.UserDetails;
import com.safetycar.services.contracts.AuthorisationService;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Service;

import static com.safetycar.util.Constants.ErrorsConstants.NO_USER_LIKE_THIS_WAS_FOUND;


@Service
public class AuthorisationServiceImpl implements AuthorisationService {

    private static final String THIS_IS_NOT_YOUR_OFFER = "This is not your Offer!";
    private static final String THIS_IS_NOT_YOUR_POLICY = "This is not your Policy!";
    private static final String THIS_IS_NOT_YOUR_PROFILE = "This is not your profile!";

    @Override
    public void authorise(User userToAuthorise, Offer offer) {
        boolean isAdmin = userToAuthorise.isAdmin();
        boolean isOwner = userToAuthorise.getOffers().contains(offer);
        if (!isAdmin && !isOwner) {
            throw new AccessDeniedException(THIS_IS_NOT_YOUR_OFFER);
        }
    }

    @Override
    public void authorise(User userToAuthorise, Policy policy) {
        boolean isOwner = userToAuthorise.getUserName().equals(policy.getOwner().getUserName());
        boolean isAdmin = userToAuthorise.isAdmin();
        if (!isAdmin && !isOwner) {
            throw new AccessDeniedException(THIS_IS_NOT_YOUR_POLICY);
        }
    }

    @Override
    public void authorise(UserDetails details, User userToAuthorise) {
        boolean isOwner = details.getUserName().equals(userToAuthorise.getUserName());
        boolean isAdmin = userToAuthorise.isAdmin();
        if (!isAdmin && !isOwner) {
            throw new AccessDeniedException(THIS_IS_NOT_YOUR_PROFILE);
        }
    }

    @Override
    public void authoriseNotAdmin(User mustNotBeAdmin, String errorMessage) {
        if (mustNotBeAdmin.isAdmin()) {
            throw new AccessDeniedException(errorMessage);
        }
    }
}
