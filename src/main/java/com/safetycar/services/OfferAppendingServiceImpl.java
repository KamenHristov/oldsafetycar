package com.safetycar.services;

import com.safetycar.exceptions.DuplicateEntityException;
import com.safetycar.models.Offer;
import com.safetycar.models.User;
import com.safetycar.repositories.OfferRepository;
import com.safetycar.services.contracts.OfferAppendingService;
import com.safetycar.exceptions.TooManyOffersException;
import com.safetycar.services.contracts.UserHistoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import static com.safetycar.exceptions.TooManyOffersException.MAXIMUM_OFFERS_ALLOWED;

@Service
public class OfferAppendingServiceImpl implements OfferAppendingService {

    private static final String OFFER_FOR_THIS_CAR_ALREADY_EXISTS = "An offer for this car already exists!";

    private final OfferRepository offerRepository;
    private final UserHistoryService historyService;

    @Autowired
    public OfferAppendingServiceImpl(OfferRepository offerRepository,
                                     UserHistoryService historyService) {
        this.offerRepository = offerRepository;
        this.historyService = historyService;
    }

    @Override
    public void appendOfferAndRecordHistory(Offer offer, User user) {
        ensureOffersWithinLimit(user);
        ensureNoDuplicates(offer, user);
        if (offer != null && !user.isAdmin()) {
            user.addOffer(offer);
            historyService.appendHistoryOnOfferCreation(user);
        }
    }

    private void ensureNoDuplicates(Offer offer, User user) {
        boolean isDuplicate = offerRepository.exists(offer, user);
        if (isDuplicate) {
            throw new DuplicateEntityException(OFFER_FOR_THIS_CAR_ALREADY_EXISTS);
        }
    }

    private void ensureOffersWithinLimit(User user) {
        boolean exceedsMaximum = user.getOffers().size() == MAXIMUM_OFFERS_ALLOWED;
        if (exceedsMaximum) {
            throw new TooManyOffersException();
        }
    }

}
