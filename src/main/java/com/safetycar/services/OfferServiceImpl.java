package com.safetycar.services;


import com.safetycar.models.Coefficient;
import com.safetycar.models.Offer;
import com.safetycar.models.User;
import com.safetycar.models.wrappers.CalculationWrapper;
import com.safetycar.repositories.OfferRepository;

import com.safetycar.repositories.filter.base.MapBasedSpecification;
import com.safetycar.services.contracts.OfferService;
import com.safetycar.services.contracts.base.GetServiceBase;
import com.safetycar.services.factories.contracts.OfferSpecFactory;
import com.safetycar.services.contracts.base.GetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import static com.safetycar.repositories.filter.base.BaseMapSpec.SORT_PARAMETER;
import static com.safetycar.util.Constants.QueryConstants.ID;

@Service
public class OfferServiceImpl extends GetServiceBase implements OfferService, GetService {

    private final OfferRepository offerRepository;
    private final OfferSpecFactory specFactory;

    @Autowired
    public OfferServiceImpl(OfferRepository offerRepository,
                            OfferSpecFactory specFactory) {
        this.offerRepository = offerRepository;
        this.specFactory = specFactory;
    }

    @Override
    public List<Offer> getAll() {
        MapBasedSpecification<Offer> spec = specFactory.getSpec();
        spec.addSpec(SORT_PARAMETER, ID);
        Sort sort = spec.getSort();
        return offerRepository.findAll(spec, sort);
    }

    @Override
    public Offer getOne(Integer id) {
        return getFromRepository(offerRepository, id, Offer.class);
    }

    @Override
    public BigDecimal calculatePremium(CalculationWrapper wrapper) {
        BigDecimal baseAmount = wrapper.getBaseAmount();
        Coefficient coefficient = wrapper.getCoefficient();

        if (baseAmount == null) return BigDecimal.ZERO;
        BigDecimal ageRisk = BigDecimal.valueOf(coefficient.getAgeRisk());
        BigDecimal accidentRisk = BigDecimal.valueOf(coefficient.getAccidentRisk());

        BigDecimal taxCoeff = BigDecimal.valueOf(coefficient.getTax());
        BigDecimal ageCoeff = baseAmount.multiply(ageRisk);
        BigDecimal accidentCoeff = baseAmount.multiply(accidentRisk);

        if (wrapper.hadAccidents()) {
            baseAmount = baseAmount.add(accidentCoeff);
        }
        if (wrapper.isAboveTwentyFive()) {
            baseAmount = baseAmount.add(ageCoeff);
        }

        baseAmount = baseAmount.add(taxCoeff);
        return baseAmount;
    }

    @Override
    public Collection<Offer> getMyOffers(User user) {
        MapBasedSpecification<Offer> spec = specFactory.getSpec(user.getOffers());
        return offerRepository.findAll(spec);
    }

    @Override
    public Collection<Offer> searchMyOffers(User user, Map<String, String> map) {
        MapBasedSpecification<Offer> spec = specFactory.getSpec(user.getOffers(), map);
        Sort sort = spec.getSort();
        return offerRepository.findAll(spec, sort);
    }

}
