package com.safetycar.services;

import com.safetycar.enums.PolicyStatuses;
import com.safetycar.models.*;
import com.safetycar.services.contracts.*;
import com.sun.istack.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class PolicyManagerImpl implements PolicyManager {

    public static final String CREATED_POLICY = "Created policy";
    public static final String CREATED_BY = "Created by ";
    public static final String ACTIVE = "active";
    public static final String UPDATED_PICTURE = "Updated Picture";
    public static final String CHANGED_STATUS_TO = "Changed status to ";
    public static final String PICTURE_UPDATED_BY = "Picture updated by ";

    private final PolicyService policyService;
    private final UserService userService;
    private final PolicyHistoryService historyService;
    private final MailService mailService;
    private final AuthorisationService authorisationService;
    private final StatusService statusService;

    @Autowired
    public PolicyManagerImpl(PolicyService policyService,
                             UserService userService,
                             PolicyHistoryService historyService,
                             MailService mailService,
                             AuthorisationService authorisationService,
                             StatusService statusService) {
        this.policyService = policyService;
        this.userService = userService;
        this.historyService = historyService;
        this.mailService = mailService;
        this.authorisationService = authorisationService;
        this.statusService = statusService;
    }

    @Override
    @Transactional
    public void createPolicy(Policy policy, Offer offer, User owner) {
        appendCreationHistory(policy, owner);
        policyService.create(policy);
        updateOwner(policy, offer, owner);
    }

    private void appendCreationHistory(Policy policy, User owner) {
        String message = getCreateMessage(owner);
        historyService.appendHistory(owner, policy, CREATED_POLICY, message);
    }

    @NotNull
    private String getCreateMessage(User owner) {
        return CREATED_BY + owner.getUserDetails().getFullName();
    }

    private void updateOwner(Policy policy, Offer offer, User owner) {
        owner.removeOffer(offer);
        owner.addPolicy(policy);
        userService.update(owner);
    }

    @Override
    @Transactional
    public void adminUpdatePolicy(Policy policyToUpdate, Status newStatus, User agent, String message) {
        authorisationService.authorise(agent, policyToUpdate);
        appendAdminUpdateHistory(policyToUpdate, newStatus, agent, message);
        updateStatus(policyToUpdate, newStatus);
        policyService.update(policyToUpdate);
        mailService.sendPolicyStatusMail(policyToUpdate);
    }

    private void appendAdminUpdateHistory(Policy policy, Status status, User agent, String message) {
        String action = getAction(status);
        historyService.appendHistory(agent, policy, action, message);
    }

    private String getAction(Status status) {
        return CHANGED_STATUS_TO + status.getStatus();
    }

    @Override
    @Transactional
    public void userUpdatePolicy(Policy policy, User owner, Image image) {
        policy.setImage(image);
        updateStatus(policy, statusService.getOne(PolicyStatuses.PENDING.getId()));
        appendUserUpdateHistory(policy, owner);
        policyService.update(policy);
    }

    private void updateStatus(Policy policy, Status status) {
        policy.setStatus(status);
    }

    private void appendUserUpdateHistory(Policy policy, User owner) {
        String message = getUserUpdateMessage(owner);
        historyService.appendHistory(owner, policy, UPDATED_PICTURE, message);
    }

    private String getUserUpdateMessage(User owner) {
        return PICTURE_UPDATED_BY + owner.getUserDetails().getFullName();
    }

}
