package com.safetycar.services.factories.contracts;

import org.springframework.data.jpa.domain.Specification;

public interface AnonymousSpecFactory {

    /**
     * @param attribute   attribute of T
     * @param value       value to compare with
     * @param entityClass class of T
     * @param <T>         type of entity to query for
     * @param <V>         type of @value
     * @return Returns a {@link org.springframework.data.jpa.domain.Specification<T>}
     * with an implemented toPredicate() method. The implementation of toPredicate()
     * in turn returns an equal predicate between @attribute and @value
     */
    <T, V> Specification<T> getEqualAnonymousSpec(String attribute, V value, Class<T> entityClass);

}
