package com.safetycar.services.factories.contracts;

import com.safetycar.models.Offer;
import com.safetycar.repositories.filter.base.MapBasedSpecification;
import com.safetycar.repositories.filter.OfferSpec;

import java.util.Collection;
import java.util.Map;

public interface OfferSpecFactory {

    MapBasedSpecification<Offer> getSpec(Collection<Offer> collection);

    MapBasedSpecification<Offer> getSpec(Collection<Offer> collection,
                                         Map<String, String> filter);

    MapBasedSpecification<Offer> getSpec();

}
