package com.safetycar.services.factories.contracts;

import com.safetycar.models.BaseAmount;
import com.safetycar.repositories.filter.BaseAmountSpec;
import com.safetycar.repositories.filter.base.MapBasedSpecification;

import java.util.Map;

public interface BaseAmountSpecFactory {

    MapBasedSpecification<BaseAmount> getSpec(Map<String, String> filter);

    MapBasedSpecification<BaseAmount> getSpec();

}
