package com.safetycar.services.factories.contracts;

import com.safetycar.models.User;
import com.safetycar.repositories.filter.base.MapBasedSpecification;

import java.util.Map;

public interface UserSpecFactory {

    MapBasedSpecification<User> getSpec(Map<String, String> filter);

    MapBasedSpecification<User> getSpec();

}
