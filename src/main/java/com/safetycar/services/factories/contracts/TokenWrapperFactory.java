package com.safetycar.services.factories.contracts;

import com.safetycar.models.base.Token;
import com.safetycar.web.wrapper.TokenWrapper;

import java.util.Locale;

public interface TokenWrapperFactory {

    TokenWrapper getTokenWrapper(String appUrl, Locale locale, Token token);
}
