package com.safetycar.services.factories;

import com.safetycar.services.factories.contracts.AnonymousSpecFactory;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

@Component
public class AnonymousSpecFactoryImpl implements AnonymousSpecFactory {

    @Override
    public <T, V> Specification<T> getEqualAnonymousSpec(final String attribute,
                                                      final V value,
                                                      final Class<T> returnTypeClass) {

        return (root, query, criteriaBuilder) ->
                criteriaBuilder.equal(root.get(attribute), value);
    }

}
