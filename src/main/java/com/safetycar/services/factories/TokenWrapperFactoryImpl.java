package com.safetycar.services.factories;

import com.safetycar.models.base.Token;
import com.safetycar.services.factories.contracts.TokenWrapperFactory;
import com.safetycar.web.wrapper.TokenWrapper;
import org.springframework.stereotype.Component;

import java.util.Locale;

@Component
public class TokenWrapperFactoryImpl implements TokenWrapperFactory {
    @Override
    public TokenWrapper getTokenWrapper(String appUrl, Locale locale, Token token) {
        return new TokenWrapper(appUrl, locale, token);
    }
}
