package com.safetycar.services.factories;

import com.safetycar.models.Offer;
import com.safetycar.repositories.filter.base.MapBasedSpecification;
import com.safetycar.repositories.filter.OfferSpec;
import com.safetycar.services.factories.contracts.OfferSpecFactory;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.Map;

@Component
public class OfferSpecFactoryImpl implements OfferSpecFactory {

    @Override
    public MapBasedSpecification<Offer> getSpec(Collection<Offer> collection) {
        return new OfferSpec(collection);
    }

    @Override
    public MapBasedSpecification<Offer> getSpec(Collection<Offer> collection, Map<String, String> map) {
        return new OfferSpec(collection, map);
    }

    @Override
    public MapBasedSpecification<Offer> getSpec() {
        return new OfferSpec();
    }

}
