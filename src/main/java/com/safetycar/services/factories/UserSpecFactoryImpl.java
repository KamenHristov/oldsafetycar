package com.safetycar.services.factories;

import com.safetycar.models.User;
import com.safetycar.repositories.filter.base.MapBasedSpecification;
import com.safetycar.repositories.filter.UserSpec;
import com.safetycar.services.factories.contracts.UserSpecFactory;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class UserSpecFactoryImpl implements UserSpecFactory {

    @Override
    public MapBasedSpecification<User> getSpec(Map<String, String> filter) {
        return new UserSpec(filter);
    }

    @Override
    public MapBasedSpecification<User> getSpec() {
        return new UserSpec();
    }

}
