package com.safetycar.services.factories;

import com.safetycar.models.BaseAmount;
import com.safetycar.repositories.filter.BaseAmountSpec;
import com.safetycar.repositories.filter.base.MapBasedSpecification;
import com.safetycar.services.factories.contracts.BaseAmountSpecFactory;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class BaseAmountSpecFactoryImpl implements BaseAmountSpecFactory {

    @Override
    public MapBasedSpecification<BaseAmount> getSpec(Map<String, String> map) {
        return new BaseAmountSpec(map);
    }

    @Override
    public MapBasedSpecification<BaseAmount> getSpec() {
        return new BaseAmountSpec();
    }

}
