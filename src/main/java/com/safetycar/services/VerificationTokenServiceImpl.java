package com.safetycar.services;

import com.safetycar.exceptions.ExpiredException;
import com.safetycar.models.VerificationToken;
import com.safetycar.models.User;
import com.safetycar.repositories.VerificationTokenRepository;
import com.safetycar.services.contracts.ExpiredTokenDisposal;
import com.safetycar.services.contracts.VerificationTokenService;
import com.safetycar.services.contracts.base.FindService;
import com.safetycar.services.contracts.base.FindServiceBase;
import com.safetycar.services.factories.contracts.AnonymousSpecFactory;
import com.sun.istack.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.Calendar;
import java.util.Locale;
import java.util.UUID;

import static com.safetycar.util.Constants.ErrorsConstants.INVALID_TOKEN;
import static com.safetycar.web.controllers.mvc.MvcRegisterController.TOKEN;

@Service
public class VerificationTokenServiceImpl extends FindServiceBase implements VerificationTokenService, FindService {

    public static final String AUTH_MESSAGE_EXPIRED = "auth.message.expired";

    private final VerificationTokenRepository verificationTokenRepository;
    private final MessageSource messageSource;
    private final ExpiredTokenDisposal expiredTokenDisposal;
    private final AnonymousSpecFactory specFactory;

    @Autowired
    public VerificationTokenServiceImpl(VerificationTokenRepository verificationTokenRepository,
                                        MessageSource messageSource,
                                        ExpiredTokenDisposal expiredTokenDisposal,
                                        AnonymousSpecFactory specFactory) {
        this.verificationTokenRepository = verificationTokenRepository;
        this.messageSource = messageSource;
        this.expiredTokenDisposal = expiredTokenDisposal;
        this.specFactory = specFactory;
    }


    @Override
    public void create(VerificationToken newToken) {
        verificationTokenRepository.save(newToken);
    }

    @Override
    public VerificationToken getNewUnsavedToken(User user) {
        VerificationToken newToken = new VerificationToken();
        newToken.setToken(UUID.randomUUID().toString());
        newToken.setUser(user);
        return newToken;
    }

    @Override
    public VerificationToken findVerificationToken(String token) {
        VerificationToken verificationToken = getExistingToken(token);
        ensureTokenNotExpired(verificationToken);
        return verificationToken;
    }

    @NotNull
    private VerificationToken getExistingToken(final String token) {
        Specification<VerificationToken> spec =
                specFactory.getEqualAnonymousSpec(TOKEN, token, VerificationToken.class);
        return findFromRepository(verificationTokenRepository, spec, INVALID_TOKEN);
    }

    private void ensureTokenNotExpired(VerificationToken verificationToken) {
        Calendar cal = Calendar.getInstance();
        boolean isExpired = (verificationToken.getExpiryDate().getTime() - cal.getTime().getTime()) <= 0;
        if (isExpired) {
            String message = messageSource.getMessage(AUTH_MESSAGE_EXPIRED, null, Locale.ENGLISH);
            expiredTokenDisposal.clearExpiredAndDeleteInactiveUser(verificationToken);
            throw new ExpiredException(message);
        }
    }

}
