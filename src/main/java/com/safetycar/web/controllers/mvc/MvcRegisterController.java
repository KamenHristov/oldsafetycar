package com.safetycar.web.controllers.mvc;


import com.safetycar.exceptions.InvalidRecaptchaException;
import com.safetycar.models.Offer;
import com.safetycar.models.VerificationToken;
import com.safetycar.services.contracts.AccountManager;
import com.safetycar.web.dto.user.CreateUserDto;
import com.safetycar.exceptions.DuplicateEntityException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import static com.safetycar.util.Constants.OfferConstants.*;
import static com.safetycar.util.Constants.UserConstants.CREATE_USER_DTO;
import static com.safetycar.util.Constants.ValidationConstants.BINDING_RESULT_ERRORS;
import static com.safetycar.util.Constants.ValidationConstants.ERROR_;
import static com.safetycar.util.Constants.Views.*;
import static com.safetycar.util.Constants.QueryConstants.EMAIL;
import static com.safetycar.web.controllers.mvc.MvcOfferController.REGISTER_FIRST;

@Controller
@RequestMapping
public class MvcRegisterController {

    public static final String REGISTER_ENDPOINT = "/register";
    public static final String REGISTER_CONFIRM_ENDPOINT = "/register/confirm";
    public static final String TOKEN = "token";
    public static final String G_RECAPTCHA_RESPONSE = "g-recaptcha-response";
    public static final String RECAPTCHA_MESSAGE = "recaptchaMessage";

    private final AccountManager accountManager;

    @Autowired
    public MvcRegisterController(AccountManager accountManager) {
        this.accountManager = accountManager;
    }

    @GetMapping(REGISTER_ENDPOINT)
    public String getRegisterPage(Model model,
                                  CreateUserDto dto,
                                  HttpServletRequest request) {
        prepareRegistrationPage(model, dto, request);
        return REGISTER_VIEW;
    }

    @PostMapping(REGISTER_ENDPOINT)
    public String handleRegisterUser(
            @Valid @ModelAttribute(CREATE_USER_DTO) CreateUserDto dto,
            BindingResult bindingResult,
            @RequestParam(G_RECAPTCHA_RESPONSE) String recaptchaResponse,
            HttpServletRequest request,
            Model model
    ) {
        if (bindingResult.hasErrors()) {
            prepareRegistrationPage(model, dto, request);
            return REGISTER_VIEW;
        }
        Offer offerToAppend = getOffer(request);
        try {
            accountManager.registerUserAndRecordHistory(dto, offerToAppend, request, recaptchaResponse);
            clearSession(request);
            return EMAIL_CONFIRMATION_VIEW;
        } catch (DuplicateEntityException e) {
            bindingResult.rejectValue(EMAIL, ERROR_ +
                    CREATE_USER_DTO, e.getMessage());
            return handleError(dto, model, BINDING_RESULT_ERRORS, e.getMessage());
        } catch (InvalidRecaptchaException e) {
            return handleError(dto, model, RECAPTCHA_MESSAGE, e.getMessage());
        }
    }

    @Nullable
    private Offer getOffer(HttpServletRequest request) {
        Offer offer = null;
        HttpSession session = request.getSession(false);
        if (session != null) {
            offer = (Offer) session.getAttribute(TRANSFER_OFFER);
        }
        return offer;
    }

    private void prepareRegistrationPage(Model model,
                                         CreateUserDto dto,
                                         HttpServletRequest request) {
        String msg = getRegisterFirstMessage(request);
        model.addAttribute(REGISTER_FIRST, msg);
        model.addAttribute(CREATE_USER_DTO, dto);
    }

    @Nullable
    private String getRegisterFirstMessage(HttpServletRequest request) {
        HttpSession session = request.getSession(false);
        String attribute = null;
        if (session != null) {
            attribute = (String) session.getAttribute(REGISTER_FIRST);
        }
        return attribute;
    }

    private void clearSession(HttpServletRequest request) {
        HttpSession session = request.getSession(false);
        if (session != null) {
            session.removeAttribute(REGISTER_FIRST);
        }
    }

    private String handleError(CreateUserDto dto, Model model, String errorName, String message) {
        model.addAttribute(CREATE_USER_DTO, dto);
        model.addAttribute(errorName, message);
        return REGISTER_VIEW;
    }

    @GetMapping(REGISTER_CONFIRM_ENDPOINT)
    public String confirmRegistration(@RequestParam(TOKEN) String token) {
        VerificationToken verificationToken = accountManager.getVerificationToken(token);
        accountManager.activateAcc(verificationToken);
        return REGISTER_CONFIRMATION_VIEW;
    }
}
