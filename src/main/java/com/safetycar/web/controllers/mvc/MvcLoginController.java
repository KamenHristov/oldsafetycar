package com.safetycar.web.controllers.mvc;

import com.safetycar.exceptions.EntityNotFoundException;
import com.safetycar.models.PasswordToken;
import com.safetycar.models.User;
import com.safetycar.services.contracts.*;
import com.safetycar.services.factories.contracts.TokenWrapperFactory;
import com.safetycar.web.dto.user.ForgottenPasswordDto;
import com.safetycar.web.wrapper.TokenWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.ui.Model;
import org.springframework.security.web.WebAttributes;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;

import org.springframework.security.core.AuthenticationException;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import static com.safetycar.util.Constants.ErrorsConstants.NO_USER_LIKE_THIS_WAS_FOUND;
import static com.safetycar.util.Constants.OfferConstants.MESSAGE;
import static com.safetycar.util.Constants.ValidationConstants.ERROR;
import static com.safetycar.util.Constants.Views.*;
import static com.safetycar.util.Helper.getAppUrl;
import static com.safetycar.web.controllers.mvc.MvcRegisterController.TOKEN;
import static com.safetycar.web.controllers.mvc.MvcUserController.PASSWORD_DTO;

@Controller
public class MvcLoginController {

    public static final String LOGIN_ENDPOINT = "/login";
    public static final String LOGIN_ERROR_ENDPOINT = "/login/error";
    public static final String FORGOTTEN_PASS_EMAIL_SENT_MESSAGE = "An email has been sent to this address. Follow the link to change your password.";
    public static final String REDIRECT_LOGIN = "redirect:/login";
    public static final String REQUEST_PASS_RESET_ENDPOINT = "/email/send";

    private final UserService userService;
    private final PasswordTokenService tokenService;
    private final PasswordChangingService passwordChangingService;
    private final MailService mailService;
    private final AuthorisationService authorisationService;
    private final TokenWrapperFactory wrapperFactory;

    @Autowired
    public MvcLoginController(UserService userService,
                              PasswordTokenService tokenService,
                              PasswordChangingService passwordChangingService,
                              MailService mailService,
                              AuthorisationService authorisationService,
                              TokenWrapperFactory wrapperFactory) {
        this.userService = userService;
        this.tokenService = tokenService;
        this.passwordChangingService = passwordChangingService;
        this.mailService = mailService;
        this.authorisationService = authorisationService;
        this.wrapperFactory = wrapperFactory;
    }

    @GetMapping(LOGIN_ENDPOINT)
    public String showLoginPage() {
        return LOGIN_VIEW;
    }

    @GetMapping(LOGIN_ERROR_ENDPOINT)
    public String showError(Model model, HttpServletRequest request) {
        HttpSession session = request.getSession(false);
        if (session != null) {
            AuthenticationException e = (AuthenticationException) session
                    .getAttribute(WebAttributes.AUTHENTICATION_EXCEPTION);
            String message = null;
            if (e != null) {
                message = e.getMessage();
            }
            model.addAttribute(ERROR, message);
        }
        return LOGIN_VIEW;
    }

    @PostMapping(REQUEST_PASS_RESET_ENDPOINT)
    public String requestPasswordResetPublic(Model model,
                                             @ModelAttribute(name = "email") String email,
                                             HttpServletRequest request) {
        try {
            User user = userService.userByEmail(email);
            authorisationService.authoriseNotAdmin(user, NO_USER_LIKE_THIS_WAS_FOUND);
            PasswordToken token = tokenService.getNewUnsavedToken(user);
            tokenService.create(token);
            sendMail(request, token);
            model.addAttribute(MESSAGE, FORGOTTEN_PASS_EMAIL_SENT_MESSAGE);
            return LOGIN_VIEW;
        } catch (EntityNotFoundException | AccessDeniedException e) {
            model.addAttribute(ERROR, e.getMessage());
            return LOGIN_VIEW;
        }
    }

    private void sendMail(HttpServletRequest request, PasswordToken unsavedToken) {
        String appUrl = getAppUrl(request);
        TokenWrapper tokenWrapper = wrapperFactory.getTokenWrapper(appUrl, request.getLocale(), unsavedToken);
        mailService.sendPasswordChangeRequest(tokenWrapper);
    }

    @GetMapping("/change-password/verification")
    public String verifyPasswordResetToken(@RequestParam(TOKEN) String token, Model model) {
        tokenService.validateToken(token);
        ForgottenPasswordDto dto = new ForgottenPasswordDto();
        dto.setToken(token);
        return prepareForgottenPassword(model, dto);
    }

    private String prepareForgottenPassword(Model model, ForgottenPasswordDto dto) {
        model.addAttribute(PASSWORD_DTO, dto);
        return RESET_PASSWORD_VIEW;
    }

    @PostMapping("/change-password/submit")
    public String handleResetPasswordPublic(Model model,
                                            @Valid @ModelAttribute(name = PASSWORD_DTO) ForgottenPasswordDto dto,
                                            BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return prepareForgottenPassword(model, dto);
        }
        changePassword(dto);
        return REDIRECT_LOGIN;
    }

    private void changePassword(ForgottenPasswordDto dto) {
        PasswordToken passwordToken = tokenService.findForgottenPasswordToken(dto.getToken());
        User user = passwordToken.getUser();
        passwordChangingService.changeForgottenPassword(user, dto.getPassword());
        tokenService.delete(passwordToken);
    }

}
