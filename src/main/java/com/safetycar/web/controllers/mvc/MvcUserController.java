package com.safetycar.web.controllers.mvc;

import com.safetycar.exceptions.DuplicateEntityException;
import com.safetycar.exceptions.InvalidOldPasswordException;
import com.safetycar.exceptions.SamePasswordException;
import com.safetycar.models.Image;
import com.safetycar.models.Policy;
import com.safetycar.models.User;
import com.safetycar.models.UserDetails;
import com.safetycar.services.contracts.*;
import com.safetycar.web.dto.mappers.contracts.ImageMapper;
import com.safetycar.web.dto.mappers.contracts.UserMapper;
import com.safetycar.web.dto.policy.CreatePolicyDto;
import com.safetycar.web.dto.user.CreateDetailsDto;
import com.safetycar.web.dto.user.PasswordChangeDto;
import com.safetycar.web.dto.user.ShowUserDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.security.Principal;
import java.sql.Date;
import java.util.Optional;

import static com.safetycar.util.Constants.UserConstants.*;
import static com.safetycar.util.Constants.ValidationConstants.BINDING_RESULT_ERRORS;
import static com.safetycar.util.Constants.ValidationConstants.ERROR_;
import static com.safetycar.util.Constants.QueryConstants.TELEPHONE;
import static com.safetycar.util.Constants.Views.*;
import static com.safetycar.web.controllers.mvc.MvcPolicyController.*;

@Controller
@RequestMapping("/users")
public class MvcUserController {

    public static final String PROFILE_EDIT_ENDPOINT = "/profile/edit";
    public static final String SEARCH_USERS_DTO = "searchUsersDto";
    public static final String USER_DETAILS = "userDetails";
    public static final String PASSWORD_DTO = "passwordDto";
    public static final String PROFILE_EDIT_PASSWORD_ENDPOINT = "/profile/edit/password";
    public static final String PASSWORD_ERROR = "passwordError";
    public static final String PASSWORD = "password";
    public static final String OLD_PASSWORD = "oldPassword";
    public static final String PASSWORD_ERROR_MESSAGE = "Password error!";
    public static final String IMAGE_ID = "imageId";

    private final AccountManager accountManager;
    private final UserService userService;
    private final UserMapper userMapper;
    private final PolicyService policyService;
    private final AuthorisationService authorisationService;
    private final ImageMapper imageMapper;
    private final PolicyManager policyManager;
    private final PasswordChangingService passwordChangingService;

    @Autowired
    public MvcUserController(AccountManager accountManager,
                             UserService userService,
                             UserMapper userMapper,
                             PolicyService policyService,
                             AuthorisationService authorisationService,
                             ImageMapper imageMapper,
                             PolicyManager policyManager,
                             PasswordChangingService passwordChangingService) {
        this.accountManager = accountManager;
        this.userService = userService;
        this.userMapper = userMapper;
        this.policyService = policyService;
        this.authorisationService = authorisationService;
        this.imageMapper = imageMapper;
        this.policyManager = policyManager;
        this.passwordChangingService = passwordChangingService;
    }

    @PostMapping(PROFILE_EDIT_PASSWORD_ENDPOINT)
    @PreAuthorize("hasRole('ROLE_USER')")
    public String handleChangePassword(
            @Valid @ModelAttribute(PASSWORD_DTO) PasswordChangeDto dto,
            BindingResult bindingResult,
            HttpServletRequest request,
            Model model,
            Principal principal) {
        if (bindingResult.hasErrors()) {
            model.addAttribute(PASSWORD_ERROR, PASSWORD_ERROR_MESSAGE);
            prepareProfileEditPage(model, request, principal.getName(), dto);
            return PROFILE_EDIT_VIEW;
        }
        //TODO this should use two factor authentication
        try {
            User user = userService.userByEmail(principal.getName());
            passwordChangingService.changePassword(user, dto.getPassword(), dto.getOldPassword());
            prepareProfileEditPage(model, request, principal.getName(), dto);
            return PROFILE_EDIT_VIEW;
        } catch (SamePasswordException e) {
            return handleError(dto, bindingResult, request, model, principal, PASSWORD, e.getMessage());
        } catch (InvalidOldPasswordException e) {
            return handleError(dto, bindingResult, request, model, principal, OLD_PASSWORD, e.getMessage());
        }
    }

    private String handleError(PasswordChangeDto dto, BindingResult bindingResult, HttpServletRequest request,
                               Model model, Principal principal, String attribute, String message) {
        bindingResult.rejectValue(attribute, ERROR_ + PASSWORD_DTO, message);
        model.addAttribute(PASSWORD_ERROR, message);
        prepareProfileEditPage(model, request, principal.getName(), dto);
        return PROFILE_EDIT_VIEW;
    }

    @PreAuthorize("hasRole('ROLE_USER')")
    @GetMapping(PROFILE_EDIT_ENDPOINT)
    public String getDetailsUpdateForm(Model model,
                                       Principal principal,
                                       HttpServletRequest request) {
        prepareProfileEditPage(model, request, principal.getName(), new PasswordChangeDto());
        return PROFILE_EDIT_VIEW;
    }

    private void prepareProfileEditPage(Model model, HttpServletRequest request,
                                        String userName, PasswordChangeDto passDto) {
        CreateDetailsDto detailsDto = accountManager.getDetailsToEdit(userName);
        prepareProfileEditPage(model, request, detailsDto, passDto);
    }

    @PreAuthorize("hasRole('ROLE_USER')")
    @PostMapping(PROFILE_EDIT_ENDPOINT)
    public String handleEditDetailsForm(
            @Valid @ModelAttribute(CREATE_USER_DTO) CreateDetailsDto dto,
            BindingResult bindingResult,
            Model model,
            Principal principal,
            HttpServletRequest request) {
        if (bindingResult.hasErrors()) {
            prepareProfileEditPage(model, request, dto, new PasswordChangeDto());
            return PROFILE_EDIT_VIEW;
        }
        try {
            String redirect = handleRedirect(dto, principal, request);
            if (redirect != null) return redirect;
            accountManager.updateDetailsAndRecordHistory(dto, principal.getName());
            prepareProfileEditPage(model, request, dto, new PasswordChangeDto());
            return PROFILE_EDIT_VIEW;
        } catch (DuplicateEntityException e) {
            return handleDuplicateError(dto, bindingResult, model, e, new PasswordChangeDto());
        }
    }

    private void prepareProfileEditPage(Model model, HttpServletRequest request,
                                        CreateDetailsDto detailsDto, PasswordChangeDto passDto) {
        String message = extractMessageFromSession(request);
        model.addAttribute(CREATE_USER_DTO, detailsDto);
        model.addAttribute(DETAILS, message);
        model.addAttribute(PASSWORD_DTO, passDto);
    }

    private String handleRedirect(CreateDetailsDto dto, Principal principal, HttpServletRequest request) {
        HttpSession session = request.getSession(false);
        Optional<Object> redirect;
        if (session != null) {
            redirect = Optional.ofNullable(session.getAttribute(REDIRECT));
            session.removeAttribute(DETAILS);
            if (redirect.isPresent()) {
                accountManager.updateDetailsAndRecordHistory(dto, principal.getName());
                return (String) redirect.get();
            }
        }
        return null;
    }

    private String handleDuplicateError(CreateDetailsDto dto, BindingResult bindingResult,
                                        Model model, DuplicateEntityException e, PasswordChangeDto passwordChangeDto) {
        bindingResult.rejectValue(TELEPHONE, ERROR_ +
                CREATE_USER_DTO, e.getMessage());
        model.addAttribute(CREATE_USER_DTO, dto);
        model.addAttribute(BINDING_RESULT_ERRORS, e.getMessage());
        model.addAttribute(PASSWORD_DTO, passwordChangeDto);
        return PROFILE_EDIT_VIEW;
    }

    @GetMapping("/profiles/{id}")
    public String showUserProfileById(@PathVariable Integer id,
                                      Model model,
                                      Principal principal) {
        User logged = userService.userByEmail(principal.getName());
        UserDetails details = userService.getUserDetails(id);
        authorisationService.authorise(details, logged);
        return getProfileView(model, details);
    }

    @PreAuthorize("hasRole('ROLE_USER')")
    @GetMapping("/profile")
    public String showLoggedUserProfile(Model model, Principal principal) {
        User logged = userService.userByEmail(principal.getName());
        UserDetails details = logged.getUserDetails();
        return getProfileView(model, details);
    }

    private String getProfileView(Model model, UserDetails details) {
        ShowUserDto dto = userMapper.toDto(details);
        model.addAttribute(USER, dto);
        model.addAttribute(USER_DETAILS, details);
        return USER_PROFILE_VIEW;
    }

    @GetMapping("/manage/policy/{id}")
    @PreAuthorize("hasRole('ROLE_USER')")
    public String getManagePolicyPage(Model model, Principal principal,
                                      @PathVariable int id) {
        User user = userService.userByEmail(principal.getName());
        Policy policy = policyService.getOne(id);
        authorisationService.authorise(user, policy);

        CreatePolicyDto dto = getCreatePolicyDto(policy);

        preparePolicyUpdate(model, policy, dto);
        return POLICY_UPDATE_VIEW;
    }

    private CreatePolicyDto getCreatePolicyDto(Policy policy) {
        java.util.Date submissionDate = policy.getSubmissionDate();
        long bumpAhead = System.currentTimeMillis() - submissionDate.getTime();

        java.util.Date oldStartDate = policy.getStartDate();
        Date newStartDate = new Date(oldStartDate.getTime() + bumpAhead);
        Date convertToLocal = new Date(newStartDate.getTime());

        CreatePolicyDto dto = new CreatePolicyDto();
        dto.setStartDate(convertToLocal.toLocalDate());
        dto.setUpdatePolicyId(policy.getId());
        return dto;
    }

    @PostMapping("/manage/policy")
    @PreAuthorize("hasRole('ROLE_USER')")
    public String handleManageUserPolicy(Model model,
                                         Principal principal,
                                         @Valid @ModelAttribute(POLICY_DTO) CreatePolicyDto dto,
                                         BindingResult bindingResult) {
        Policy policy = policyService.getOne(dto.getUpdatePolicyId());
        User owner = userService.userByEmail(principal.getName());
        authorisationService.authorise(owner, policy);
        if (bindingResult.hasErrors()) {
            preparePolicyUpdate(model, policy, dto);
            return POLICY_UPDATE_VIEW;
        }
        Image image = imageMapper.updateFromMultipart(policy.getImage(), dto.getFile());
        policyManager.userUpdatePolicy(policy, owner, image);
        preparePolicyUpdate(model, policy, dto);
        return POLICY_UPDATE_VIEW;
    }

    private void preparePolicyUpdate(Model model, Policy policy,
                                     CreatePolicyDto policyDto) {
        model.addAttribute(OFFER, policy.getOffer());
        model.addAttribute(USER, policy.getOwner().getUserDetails());
        model.addAttribute(POLICY_DTO, policyDto);
        model.addAttribute(IMAGE_ID, policy.getImage().getId());
    }

    private String extractMessageFromSession(HttpServletRequest request) {
        HttpSession session = request.getSession(false);
        String message = null;
        if (session != null) {
            message = (String) session.getAttribute(DETAILS);
        }
        return message;
    }
}
