package com.safetycar.web.controllers.rest;

import com.safetycar.models.VerificationToken;
import com.safetycar.models.User;
import com.safetycar.models.UserDetails;
import com.safetycar.services.contracts.*;
import com.safetycar.web.dto.mappers.contracts.UserMapper;
import com.safetycar.web.dto.user.CreateDetailsDto;
import com.safetycar.web.dto.user.CreateUserDto;
import com.safetycar.web.dto.user.RestShowUserDto;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.mapstruct.Context;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Collection;

@RestController
@RequestMapping("api/users")
public class RestUserController {
    private final AccountManager accountManager;
    private final UserService userService;
    private final UserMapper userMapper;

    @Autowired
    public RestUserController(AccountManager accountManager,
                              UserService userService,
                              UserMapper userMapper) {
        this.accountManager = accountManager;
        this.userService = userService;
        this.userMapper = userMapper;
    }

    @GetMapping
    @PreAuthorize("hasRole('ROLE_AGENT')")
    @ApiOperation(value = "Show All User Details", notes = "Agent access only. Returns User Details for every User in the DB",
            response = RestShowUserDto.class)
    public Collection<RestShowUserDto> getAllDetails() {
        return userMapper.toRestShowDto(userService.getAllDetails());
    }

    @GetMapping("/{id}")
    @ApiOperation(value = "Show User by Id", notes = "Returns information for user by Id.",
            response = RestShowUserDto.class)
    public RestShowUserDto getDetails(@ApiParam(value = "Valid ID for the User you want to retrieve", example = "1")
                                      @PathVariable Integer id) {
        return userMapper.toRestShowDto(userService.getUserDetails(id));
    }

    @PostMapping("/register")
    @ApiOperation(value = "Register new User no recaptcha.", notes = "Create new inactive user." +
            " Activate with email within 24 hours of registering.",
            response = User.class)
    public User registerUser(@Valid CreateUserDto dto,
                             @Context HttpServletRequest req) {
        accountManager.registerUserAndRecordHistoryRest(dto, req);
        return userService.userByEmail(dto.getEmail());
    }

    @PutMapping("/register/confirm")
    @ApiOperation(value = "Activate newly registered User.", notes = "Following the link from your email" +
            " a unique token is verified and if valid activates this user." +
            " If the token is expired you may register again with the same email.",
            response = User.class)
    public User emailConfirmation(@RequestParam(required = true) String token) {
        VerificationToken verificationToken = accountManager.getVerificationToken(token);
        accountManager.activateAcc(verificationToken);
        return userService.userByEmail(verificationToken.getUser().getUserName());
    }

    @PutMapping("/details/update")
    @ApiOperation(value = "Update Details of User after creation",
            notes = "Update user details of created user by userId. " +
                    "Enter First & Last name, valid UserId, Address, and Phone Number",
            response = RestShowUserDto.class)
    public RestShowUserDto updateDetails(@Valid CreateDetailsDto dto) {
        UserDetails userDetails = userMapper.updateFromDto(dto, userService.getUserDetails(dto.getId()));
        String username = userDetails.getUserName();
        accountManager.updateDetailsAndRecordHistory(dto, username);
        return userMapper.toRestShowDto(userDetails);
    }


}
