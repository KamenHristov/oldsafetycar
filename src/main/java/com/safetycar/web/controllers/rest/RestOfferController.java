package com.safetycar.web.controllers.rest;

import com.safetycar.models.*;
import com.safetycar.models.User;
import com.safetycar.models.UserDetails;
import com.safetycar.models.wrappers.CalculationWrapper;
import com.safetycar.web.dto.mappers.contracts.OfferMapper;
import com.safetycar.web.dto.offer.CreateOfferDtoForRest;
import com.safetycar.services.contracts.*;
import com.safetycar.web.dto.offer.OfferDto;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashMap;
import java.util.Map;

import static com.safetycar.util.Constants.BaseAmountConstants.CAPACITY;
import static com.safetycar.util.Constants.BaseAmountConstants.CAR_AGE;
import static com.safetycar.util.Helper.getAgeFromPastLocalDate;


@RequestMapping("/api/offers")
@RestController
public class RestOfferController {

    private final OfferService offerService;
    private final OfferMapper offerMapper;
    private final OfferAppendingService offerAppendingService;
    private final UserService userService;
    private final CoefficientService coefficientService;
    private final BaseAmountService baseAmountService;

    @Autowired
    public RestOfferController(OfferService offerService,
                               OfferMapper offerMapper,
                               OfferAppendingService offerAppendingService,
                               UserService userService,
                               CoefficientService coefficientService,
                               BaseAmountService baseAmountService) {
        this.offerService = offerService;
        this.offerMapper = offerMapper;
        this.offerAppendingService = offerAppendingService;
        this.userService = userService;
        this.coefficientService = coefficientService;
        this.baseAmountService = baseAmountService;
    }

    @GetMapping
    @ApiOperation(value = "List All offers sorted by Id", notes = "Provides Id of offer to continue with policy creation",
            response = Offer.class)
    public Iterable<Offer> showAllOffers() {
        return offerService.getAll();
    }

    @GetMapping("/{id}")
    @ApiOperation(value = "Show Offer by Id", notes = "Returns detailed information for offer by Id.",
            response = Offer.class)
    public Offer showOne(@ApiParam(value = "Valid ID for the Offer you want to retrieve", example = "1") @PathVariable int id) {
        return offerService.getOne(id);
    }

    @PostMapping("/create")
    @ApiOperation(value = "Create offer for UserId", notes = "Creates offer from dto and save it to DB",
            response = Offer.class)
    public Offer createOffer(@Valid @RequestBody CreateOfferDtoForRest dto) {
        Offer offer = offerMapper.fromDtoCreateOfferRest(dto);
        UserDetails userDetails = userService.getUserDetails(dto.getOwnerId());
        User user = userService.userByEmail(userDetails.getUserName());
        offerAppendingService.appendOfferAndRecordHistory(offer, user);
        userService.update(user);
        return offer;
    }


    @PostMapping("/calculate")
    @ApiOperation(value = "Calculate price for insurance offer",
            notes = "Calculate the price for offer by entering: Is driver above 25 y.o (true/false), " +
                    "Cubic Capacity of the Car(number), " +
                    "Date for first registration of vehicle(yyyy-mm-dd), " +
                    "Driver had accidents in previous year(true/false), " +
                    "Car model Id",
            response = Offer.class)
    public BigDecimal calculateAmount(@RequestBody @Valid OfferDto offerDto) {

        CalculationWrapper wrapper = getCalculationWrapper(offerDto);
        BigDecimal result = offerService.calculatePremium(wrapper);

        return result.setScale(2, RoundingMode.HALF_UP);
    }

    private CalculationWrapper getCalculationWrapper(OfferDto offerDto) {
        Map<String, String> criteria = getCriteria(offerDto);
        BaseAmount baseAmount = baseAmountService.getBaseAmount(criteria);

        boolean hadAccidents = offerDto.getHadAccidents().equals("true");
        boolean aboveTwentyFive = offerDto.getAboveTwentyFive().equals("true");

        Coefficient coefficient = coefficientService.getCoefficient();
        return new CalculationWrapper(baseAmount.getBaseAmount(),
                hadAccidents, aboveTwentyFive, coefficient);
    }

    private Map<String, String> getCriteria(OfferDto offerDto) {
        int capacity = Integer.parseInt(offerDto.getCapacity());
        int carAge = getAgeFromPastLocalDate(offerDto.getDateRegistered());

        Map<String, String> criteria = new HashMap<>();
        criteria.put(CAPACITY, String.valueOf(capacity));
        criteria.put(CAR_AGE, String.valueOf(carAge));
        return criteria;
    }


}
