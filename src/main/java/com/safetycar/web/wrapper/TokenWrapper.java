package com.safetycar.web.wrapper;

import com.safetycar.models.base.Token;

import java.util.Locale;
import java.util.Objects;

public class TokenWrapper {

    private final String appUrl;
    private final Locale locale;
    private final Token token;

    public TokenWrapper(String appUrl, Locale locale, Token token) {
        this.appUrl = appUrl;
        this.locale = locale;
        this.token = token;
    }

    public String getAppUrl() {
        return appUrl;
    }

    public Locale getLocale() {
        return locale;
    }

    public Token getToken() {
        return token;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TokenWrapper that = (TokenWrapper) o;
        return Objects.equals(appUrl, that.appUrl) &&
                Objects.equals(locale, that.locale) &&
                Objects.equals(token, that.token);
    }

    @Override
    public int hashCode() {
        return Objects.hash(appUrl, locale, token);
    }
}