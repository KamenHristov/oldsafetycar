package com.safetycar.web.errors.handlers;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Component;

@Component
public class SafetyCarLoggerImpl implements SafetyCarLogger {

    private final Log logger = LogFactory.getLog(getClass());

    public void warn(Object message, Throwable throwable) {
        logger.warn(message, throwable);
    }

    public void warn(Throwable throwable) {
        logger.warn(throwable);
    }
}
