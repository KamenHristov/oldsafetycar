package com.safetycar.web.errors.handlers;

public interface SafetyCarLogger {

    void warn(Object message, Throwable throwable);

    void warn(Throwable throwable);
}
