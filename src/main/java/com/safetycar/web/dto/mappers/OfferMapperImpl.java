package com.safetycar.web.dto.mappers;

import com.safetycar.models.*;
import com.safetycar.models.wrappers.CalculationWrapper;
import com.safetycar.services.contracts.*;
import com.safetycar.web.dto.mappers.contracts.CarMapper;
import com.safetycar.web.dto.mappers.contracts.OfferMapper;
import com.safetycar.web.dto.offer.CreateOfferDtoForRest;
import com.safetycar.web.dto.offer.OfferDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import static com.safetycar.util.Constants.BaseAmountConstants.CAPACITY;
import static com.safetycar.util.Constants.BaseAmountConstants.CAR_AGE;
import static com.safetycar.util.Helper.getAgeFromPastLocalDate;

@Component
public class OfferMapperImpl implements OfferMapper {

    private final OfferService offerService;
    private final CoefficientService coefficientService;
    private final BaseAmountService baseAmountService;
    private final CarMapper carMapper;

    @Autowired
    public OfferMapperImpl(OfferService offerService,
                           CoefficientService coefficientService,
                           BaseAmountService baseAmountService, CarMapper carMapper) {
        this.offerService = offerService;
        this.coefficientService = coefficientService;
        this.baseAmountService = baseAmountService;
        this.carMapper = carMapper;
    }

    @Override
    public Offer fromDto(OfferDto dto) {

        boolean aboveTwentyFive = Boolean.parseBoolean(dto.getAboveTwentyFive());
        boolean hadAccidents = Boolean.parseBoolean(dto.getHadAccidents());

        Car car = carMapper.fromDto(dto);

        return getOffer(aboveTwentyFive, hadAccidents, car);
    }

    @Override
    public Offer fromDtoCreateOfferRest(CreateOfferDtoForRest dto) {
        boolean aboveTwentyFive = Boolean.parseBoolean(dto.getAboveTwentyFive());
        boolean hadAccidents = Boolean.parseBoolean(dto.getHadAccidents());
        Car car = carMapper.fromDto(dto);
        return getOffer(aboveTwentyFive, hadAccidents, car);
    }

    private Offer getOffer(boolean aboveTwentyFive, boolean hadAccidents, Car car) {
        int correctCarAge = getAgeFromPastLocalDate(car.getFirstRegistration());

        BaseAmount baseAmount = getBaseAmount(car.getCapacity(), correctCarAge);
        BigDecimal premium = getPremium(aboveTwentyFive, hadAccidents, baseAmount);

        Offer offer = new Offer();
        offer.setAboveTwentyFive(aboveTwentyFive);
        offer.setHadAccidents(hadAccidents);
        offer.setCar(car);
        offer.setPremium(premium);

        return offer;
    }

    private BaseAmount getBaseAmount(int capacity, int carAge) {
        Map<String, String> filter = new HashMap<>();
        filter.put(CAPACITY, String.valueOf(capacity));
        filter.put(CAR_AGE, String.valueOf(carAge));
        return baseAmountService.getBaseAmount(filter);
    }

    private BigDecimal getPremium(boolean aboveTwentyFive, boolean hadAccidents, BaseAmount baseAmount) {
        Coefficient coefficient = coefficientService.getCoefficient();
        CalculationWrapper wrapper = new CalculationWrapper(baseAmount.getBaseAmount(),
                hadAccidents, aboveTwentyFive, coefficient);
        return offerService.calculatePremium(wrapper);
    }
}
