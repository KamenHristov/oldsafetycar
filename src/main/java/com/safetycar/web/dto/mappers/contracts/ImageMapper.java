package com.safetycar.web.dto.mappers.contracts;

import com.safetycar.models.Image;
import org.springframework.web.multipart.MultipartFile;

public interface ImageMapper {

    Image updateFromMultipart(Image image, MultipartFile file);

    Image fromMultipart(MultipartFile file);
}
