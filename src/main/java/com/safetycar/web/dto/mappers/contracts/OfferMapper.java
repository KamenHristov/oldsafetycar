package com.safetycar.web.dto.mappers.contracts;

import com.safetycar.models.Offer;
import com.safetycar.web.dto.offer.CreateOfferDtoForRest;
import com.safetycar.web.dto.offer.OfferDto;

public interface OfferMapper {

    Offer fromDto(OfferDto dto);

    Offer fromDtoCreateOfferRest(CreateOfferDtoForRest dto);
}
