package com.safetycar.web.dto.mappers.contracts;

import com.safetycar.models.Car;
import com.safetycar.web.dto.offer.CreateOfferDtoForRest;
import com.safetycar.web.dto.offer.OfferDto;

public interface CarMapper {

    Car fromDto(OfferDto dto);

    Car fromDto(CreateOfferDtoForRest dto);
}
