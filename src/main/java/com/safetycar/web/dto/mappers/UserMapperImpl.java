package com.safetycar.web.dto.mappers;


import com.safetycar.models.User;
import com.safetycar.models.UserDetails;
import com.safetycar.services.contracts.UserAuthorityService;
import com.safetycar.web.dto.mappers.contracts.UserMapper;
import com.safetycar.web.dto.user.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class UserMapperImpl implements UserMapper {

    private final PasswordEncoder passwordEncoder;
    private final UserAuthorityService userAuthorityService;

    @Autowired
    public UserMapperImpl(PasswordEncoder passwordEncoder,
                          UserAuthorityService userAuthorityService) {
        this.passwordEncoder = passwordEncoder;
        this.userAuthorityService = userAuthorityService;
    }

    @Override
    public User fromDto(CreateUserDto dto) {
        UserDetails userDetails = new UserDetails();
        userDetails.setUserName(dto.getEmail());
        User user = new User();
        user.setUserName(dto.getEmail());
        user.setPassword(passwordEncoder.encode(dto.getPassword()));
        userAuthorityService.assignUserAuthority(user);
        user.setUserDetails(userDetails);
        return user;
    }

    @Override
    public UserDetails updateFromDto(CreateDetailsDto dto, UserDetails userDetails) {
        userDetails.setAddress(dto.getAddress());
        userDetails.setFirstName(dto.getFirstName());
        userDetails.setLastName(dto.getLastName());
        userDetails.setTelephone(dto.getTelephone());
        userDetails.setActive(true);
        return userDetails;
    }

    @Override
    public CreateDetailsDto toDto(User user) {
        CreateDetailsDto dto = new CreateDetailsDto();
        UserDetails details = user.getUserDetails();
        dto.setId(details.getIntegerId());
        dto.setTelephone(details.getTelephone());
        dto.setAddress(details.getAddress());
        dto.setFirstName(details.getFirstName());
        dto.setLastName(details.getLastName());
        return dto;
    }

    @Override
    public ShowUserDto toDto(UserDetails userDetails) {
        ShowUserDto dto = new ShowUserDto();
        dto.setFullName(userDetails.getFullName());
        dto.setSimulatedOffers(userDetails.getOffersCount());
        dto.setUserId(userDetails.getIntegerId());

        dto.setApprovedNCount(userDetails.getApprovedPoliciesCount());
        dto.setPendingCount(userDetails.getPendingPoliciesCount());

        BigDecimal approvedPoliciesNet = userDetails.getApprovedPoliciesNet();
        BigDecimal pendingPoliciesNet = userDetails.getPendingPoliciesNet();
        dto.setApprovedNet(approvedPoliciesNet.setScale(2, RoundingMode.HALF_UP));
        dto.setPendingNet(pendingPoliciesNet.setScale(2, RoundingMode.HALF_UP));

        dto.setHistoryList(new LinkedHashSet<>(userDetails.getHistory()));
        return dto;
    }

    @Override
    public Collection<ShowUserDto> toDto(Iterable<UserDetails> detailsCollection) {
        List<ShowUserDto> dtos = new LinkedList<>();
        for (UserDetails userDetails : detailsCollection) {
            dtos.add(this.toDto(userDetails));
        }
        return dtos;
    }

    @Override
    public RestShowUserDto toRestShowDto(UserDetails details) {
        RestShowUserDto dto = new RestShowUserDto();
        dto.setUserId(details.getIntegerId());
        dto.setFullName(details.getFullName());
        dto.setAddress(details.getAddress());
        dto.setTelephone(details.getTelephone());
        dto.setUsername(details.getUserName());
        return dto;
    }

    @Override
    public Collection<RestShowUserDto> toRestShowDto(Collection<UserDetails> detailsCollection) {
        return detailsCollection.stream().map(this::toRestShowDto)
                .collect(Collectors.toCollection(LinkedList::new));
    }
}
