package com.safetycar.web.dto.mappers;

import com.safetycar.models.Car;
import com.safetycar.models.Model;
import com.safetycar.services.contracts.ModelService;
import com.safetycar.web.dto.mappers.contracts.CarMapper;
import com.safetycar.web.dto.offer.CreateOfferDtoForRest;
import com.safetycar.web.dto.offer.OfferDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDate;

@Component
public class CarMapperImpl implements CarMapper {

    private final ModelService modelService;

    @Autowired
    public CarMapperImpl(ModelService modelService) {
        this.modelService = modelService;
    }

    @Override
    public Car fromDto(OfferDto dto) {
        return getCar(Integer.parseInt(dto.getCapacity()), dto.getModelId(), dto.getDateRegistered());
    }

    @Override
    public Car fromDto(CreateOfferDtoForRest dto) {
        return getCar(dto.getCapacity(), dto.getModelId(), dto.getDateRegistered());
    }

    private Car getCar(int capacity, int modelId, LocalDate dateRegistered) {
        Model model = modelService.getOne(modelId);
        Car car = new Car();
        car.setCapacity(capacity);
        car.setFirstRegistration(dateRegistered);
        car.setModelYearBrand(model);
        return car;
    }
}
