package com.safetycar.web.dto.mappers.contracts;

import com.safetycar.models.Offer;
import com.safetycar.models.Policy;
import com.safetycar.models.User;
import com.safetycar.web.dto.policy.CreatePolicyDto;
import com.safetycar.web.dto.policy.RestCreatePolicyDto;
import com.safetycar.web.dto.policy.ShowDetailedPolicyDto;
import com.safetycar.web.dto.policy.ShowPolicyDto;

import java.util.List;

public interface PolicyMapper {

    Policy assemble(Offer offer, CreatePolicyDto dto, User owner);

    ShowPolicyDto toDto(Policy policy);

    List<ShowPolicyDto> toDto(Iterable<Policy> policies);

    ShowDetailedPolicyDto toDetailedDto(Policy policy);

    Policy fromRestCreatePolicyDto(Offer offer, User user, RestCreatePolicyDto dto);

}
