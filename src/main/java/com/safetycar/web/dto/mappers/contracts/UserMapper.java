package com.safetycar.web.dto.mappers.contracts;

import com.safetycar.models.User;
import com.safetycar.models.UserDetails;
import com.safetycar.web.dto.user.CreateDetailsDto;
import com.safetycar.web.dto.user.CreateUserDto;
import com.safetycar.web.dto.user.RestShowUserDto;
import com.safetycar.web.dto.user.ShowUserDto;

import java.util.Collection;

public interface UserMapper {

    User fromDto(CreateUserDto dto);

    UserDetails updateFromDto(CreateDetailsDto dto, UserDetails userDetails);

    CreateDetailsDto toDto(User user);

    ShowUserDto toDto(UserDetails userDetails);

    Collection<ShowUserDto> toDto(Iterable<UserDetails> detailsCollection);

    RestShowUserDto toRestShowDto(UserDetails details);

    Collection<RestShowUserDto> toRestShowDto(Collection<UserDetails> detailsCollection);

}
