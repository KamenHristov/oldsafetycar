package com.safetycar.web.dto;

import java.util.Map;
import java.util.Objects;

public abstract class SearchDtoBase {

    private String desc;
    private String sortParam;

    public SearchDtoBase() {
    }

    public abstract Map<String, String> getSearchParams();

    public String getDesc() {
        return desc;
    }

    public String getSortParam() {
        return sortParam;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public void setSortParam(String sortParam) {
        this.sortParam = sortParam;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SearchDtoBase searchDtoBase = (SearchDtoBase) o;
        return Objects.equals(desc, searchDtoBase.desc) &&
                Objects.equals(sortParam, searchDtoBase.sortParam);
    }

    @Override
    public int hashCode() {
        return Objects.hash(desc, sortParam);
    }

}
