package com.safetycar.web.dto.offer;


import com.safetycar.validation.ValidCapacity;
import com.safetycar.web.dto.offer.base.OfferDtoBase;

import javax.validation.constraints.PositiveOrZero;

public class OfferDto extends OfferDtoBase {

    public static final String MUST_SELECT_MODEL = "Must select model";
    public static final String MUST_SELECT_MAKE = "Must select make";
    public static final String MUST_BE_A_POSITIVE_NUMBER = "Must be a positive number";
    public static final String CAN_NOT_BE_NULL = "Must be valid date";

    @PositiveOrZero(message = MUST_SELECT_MAKE)
    private int makeId;

    @ValidCapacity(message = MUST_BE_A_POSITIVE_NUMBER)
    private String capacity;

    private String view;

    public OfferDto() {
    }

    public String getView() {
        return view;
    }

    public int getMakeId() {
        return makeId;
    }

    public String getCapacity() {
        return capacity;
    }

    public void setMakeId(int makeId) {
        this.makeId = makeId;
    }

    public void setCapacity(String capacity) {
        this.capacity = capacity;
    }

    public void setView(String redirect) {
        this.view = redirect;
    }

}
