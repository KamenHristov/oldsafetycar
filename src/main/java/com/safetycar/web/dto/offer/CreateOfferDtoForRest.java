package com.safetycar.web.dto.offer;


import com.safetycar.web.dto.offer.base.OfferDtoBase;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;

import static com.safetycar.web.dto.offer.OfferDto.*;

public class CreateOfferDtoForRest extends OfferDtoBase {

    public static final String OWNER_CAN_NOT_BE_NULL = "Owner Can not be null";
    public static final String CAPACITY_CAN_NOT_BE_NULL = "Capacity Can not be null";

    @PositiveOrZero(message = MUST_BE_A_POSITIVE_NUMBER)
    @NotNull(message = CAPACITY_CAN_NOT_BE_NULL)
    private Integer capacity;

    @NotNull(message = OWNER_CAN_NOT_BE_NULL)
    private Integer ownerId;

    public CreateOfferDtoForRest() {
    }

    public Integer getCapacity() {
        return capacity;
    }

    public Integer getOwnerId() {
        return ownerId;
    }

    public void setCapacity(Integer capacity) {
        this.capacity = capacity;
    }

    public void setOwnerId(Integer ownerId) {
        this.ownerId = ownerId;
    }

}
