package com.safetycar.web.dto.offer.base;

import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;

import java.time.LocalDate;
import java.util.Objects;

import static com.safetycar.web.dto.offer.OfferDto.CAN_NOT_BE_NULL;
import static com.safetycar.web.dto.offer.OfferDto.MUST_SELECT_MODEL;

public class OfferDtoBase {

    @PositiveOrZero(message = MUST_SELECT_MODEL)
    private int modelId;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @NotNull(message = CAN_NOT_BE_NULL)
    private LocalDate dateRegistered;

    private String aboveTwentyFive = "false";
    private String hadAccidents = "false";

    public OfferDtoBase() {
    }

    public String getAboveTwentyFive() {
        return aboveTwentyFive;
    }

    public String getHadAccidents() {
        return hadAccidents;
    }

    public int getModelId() {
        return modelId;
    }

    public LocalDate getDateRegistered() {
        return dateRegistered;
    }

    public void setModelId(int modelId) {
        this.modelId = modelId;
    }

    public void setDateRegistered(LocalDate dateRegistered) {
        this.dateRegistered = dateRegistered;
    }

    public void setAboveTwentyFive(String aboveTwentyFive) {
        this.aboveTwentyFive = aboveTwentyFive;
    }

    public void setHadAccidents(String hadAccidents) {
        this.hadAccidents = hadAccidents;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        OfferDtoBase that = (OfferDtoBase) o;
        return modelId == that.modelId &&
                Objects.equals(dateRegistered, that.dateRegistered);
    }

    @Override
    public int hashCode() {
        return Objects.hash(modelId, dateRegistered);
    }
}
