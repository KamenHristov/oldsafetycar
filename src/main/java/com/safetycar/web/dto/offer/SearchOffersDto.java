package com.safetycar.web.dto.offer;

import com.safetycar.web.dto.SearchDtoBase;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

import static com.safetycar.repositories.filter.base.BaseMapSpec.DESC_SORT;
import static com.safetycar.repositories.filter.base.BaseMapSpec.SORT_PARAMETER;
import static com.safetycar.util.Constants.QueryConstants.*;

public class SearchOffersDto extends SearchDtoBase {

    private String premium;
    private String submissionDate;
    private String expirationDate;

    public SearchOffersDto() {
    }

    public String getPremium() {
        return premium;
    }

    public String getSubmissionDate() {
        return submissionDate;
    }

    public String getExpirationDate() {
        return expirationDate;
    }

    public void setPremium(String premium) {
        this.premium = premium;
    }

    public void setSubmissionDate(String submissionDate) {
        this.submissionDate = submissionDate;
    }

    public void setExpirationDate(String expirationDate) {
        this.expirationDate = expirationDate;
    }

    @Override
    public Map<String, String> getSearchParams() {
        Map<String, String> search = new HashMap<>();
        Optional<String> premium = Optional.ofNullable(this.premium);
        Optional<String> sort = Optional.ofNullable(this.getDesc());
        Optional<String> sortParameter = Optional.ofNullable(this.getSortParam());
        Optional<String> submissionDate = Optional.ofNullable(this.submissionDate);
        Optional<String> expirationDate = Optional.ofNullable(this.expirationDate);

        premium.ifPresent(s -> search.put(PREMIUM, s));
        sort.ifPresent(s -> search.put(DESC_SORT, s));
        sortParameter.ifPresent(s -> search.put(SORT_PARAMETER, s));
        submissionDate.ifPresent(s -> search.put(SUBMISSION_DATE, s));
        expirationDate.ifPresent(s -> search.put(EXPIRATION_DATE, s));
        return search;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        SearchOffersDto that = (SearchOffersDto) o;
        return Objects.equals(premium, that.premium) &&
                Objects.equals(submissionDate, that.submissionDate) &&
                Objects.equals(expirationDate, that.expirationDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), premium, submissionDate, expirationDate);
    }
}
