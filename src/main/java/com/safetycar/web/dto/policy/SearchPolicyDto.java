package com.safetycar.web.dto.policy;

import com.safetycar.web.dto.SearchDtoBase;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

import static com.safetycar.repositories.filter.UserSpec.STATUS;
import static com.safetycar.repositories.filter.base.BaseMapSpec.DESC_SORT;
import static com.safetycar.repositories.filter.base.BaseMapSpec.SORT_PARAMETER;
import static com.safetycar.util.Constants.QueryConstants.*;

public class SearchPolicyDto extends SearchDtoBase {

    private String status;
    private String brand;
    private String searchingAsRole;

    private int userId;

    public SearchPolicyDto() {
    }

    public String getSearchingAsRole() {
        return searchingAsRole;
    }

    public int getUserId() {
        return userId;
    }

    public String getStatus() {
        return status;
    }

    public String getBrand() {
        return brand;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public void setSearchingAsRole(String searchingAsRole) {
        this.searchingAsRole = searchingAsRole;
    }

    @Override
    public Map<String, String> getSearchParams() {
        Map<String, String> search = new HashMap<>();
        Optional<String> status = Optional.ofNullable(this.status);
        Optional<String> sort = Optional.ofNullable(this.getDesc());
        Optional<String> sortParameter = Optional.ofNullable(this.getSortParam());
        Optional<String> brand = Optional.ofNullable(this.brand);
        status.ifPresent(s -> search.put(STATUS, s));
        sort.ifPresent(s -> search.put(DESC_SORT, s));
        sortParameter.ifPresent(s -> search.put(SORT_PARAMETER, s));
        brand.ifPresent(s -> search.put(BRAND, s));
        return search;
    }

    public Map<String, String> getSearchParams(String additionalParam) {
        Map<String, String> searchParams = getSearchParams();
        searchParams.put(additionalParam, additionalParam);
        return searchParams;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        SearchPolicyDto that = (SearchPolicyDto) o;
        return userId == that.userId &&
                Objects.equals(status, that.status) &&
                Objects.equals(brand, that.brand) &&
                Objects.equals(searchingAsRole, that.searchingAsRole);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), status, brand, searchingAsRole, userId);
    }

}
