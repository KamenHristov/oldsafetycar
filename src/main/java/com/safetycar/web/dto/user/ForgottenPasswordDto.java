package com.safetycar.web.dto.user;

import com.safetycar.validation.impl.Passwordable;
import com.safetycar.web.dto.user.base.PasswordDtoBase;

import java.util.Objects;

public class ForgottenPasswordDto extends PasswordDtoBase implements Passwordable {

    private String token;

    public ForgottenPasswordDto() {
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        ForgottenPasswordDto that = (ForgottenPasswordDto) o;
        return Objects.equals(token, that.token);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), token);
    }
}
