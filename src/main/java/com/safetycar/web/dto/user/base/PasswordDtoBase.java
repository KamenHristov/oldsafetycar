package com.safetycar.web.dto.user.base;

import com.safetycar.validation.PasswordMatches;
import com.safetycar.validation.impl.Passwordable;

import javax.validation.constraints.Size;

import java.util.Objects;

import static com.safetycar.util.Constants.ValidationConstants.PASSWORD_MUST_BE_AT_LEAST_SIX_CHARACTERS_LONG;
import static com.safetycar.util.Constants.ValidationConstants.PLEASE_PROVIDE_MATCHING_PASSWORDS;

@PasswordMatches(message = PLEASE_PROVIDE_MATCHING_PASSWORDS)
public class PasswordDtoBase implements Passwordable {

    @Size(min = 6, message = PASSWORD_MUST_BE_AT_LEAST_SIX_CHARACTERS_LONG)
    private String password;
    private String confirmationPassword;

    public PasswordDtoBase() {
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setConfirmationPassword(String confirmationPassword) {
        this.confirmationPassword = confirmationPassword;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getConfirmationPassword() {
        return confirmationPassword;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PasswordDtoBase that = (PasswordDtoBase) o;
        return Objects.equals(password, that.password) &&
                Objects.equals(confirmationPassword, that.confirmationPassword);
    }

    @Override
    public int hashCode() {
        return Objects.hash(password, confirmationPassword);
    }
}
