package com.safetycar.web.dto.user;

import com.safetycar.validation.impl.Passwordable;
import com.safetycar.web.dto.user.base.PasswordDtoBase;

import java.util.Objects;


public class PasswordChangeDto extends PasswordDtoBase implements Passwordable {

    private String oldPassword;

    public PasswordChangeDto() {
    }

    public String getOldPassword() {
        return oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }

    public boolean isChangingPassword() {
        return getPassword() != null && getConfirmationPassword() != null && oldPassword != null;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        PasswordChangeDto that = (PasswordChangeDto) o;
        return Objects.equals(oldPassword, that.oldPassword);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), oldPassword);
    }
}
