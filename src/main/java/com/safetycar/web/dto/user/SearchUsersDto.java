package com.safetycar.web.dto.user;

import com.safetycar.web.dto.SearchDtoBase;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

import static com.safetycar.repositories.filter.UserSpec.*;

public class SearchUsersDto extends SearchDtoBase {

    private String fullName;
    private String policyStatus;

    public SearchUsersDto() {
    }

    public String getFullName() {
        return fullName;
    }

    public String getPolicyStatus() {
        return policyStatus;
    }

    public void setFullName(String lastName) {
        this.fullName = lastName;
    }

    public void setPolicyStatus(String policyStatus) {
        this.policyStatus = policyStatus;
    }

    @Override
    public Map<String, String> getSearchParams() {
        Optional<String> search = Optional.ofNullable(fullName);
        Optional<String> status = Optional.ofNullable(policyStatus);
        Optional<String> sort = Optional.ofNullable(this.getDesc());
        Optional<String> sortParameter = Optional.ofNullable(this.getSortParam());
        Map<String, String> filters = new HashMap<>();

        search.ifPresent(s -> filters.put(FULL_NAME, s));
        status.ifPresent(s -> filters.put(POLICY_STATUS, s));
        sort.ifPresent(s -> filters.put(DESC_SORT, s));
        sortParameter.ifPresent(s -> filters.put(SORT_PARAMETER, s));
        return filters;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        SearchUsersDto dto = (SearchUsersDto) o;
        return Objects.equals(fullName, dto.fullName) &&
                Objects.equals(policyStatus, dto.policyStatus);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), fullName, policyStatus);
    }
}
