package com.safetycar.web.dto.user;

import com.safetycar.models.History;

import java.math.BigDecimal;
import java.util.Objects;
import java.util.Set;

public class ShowUserDto {

    private int userId;
    private String fullName;
    private int simulatedOffers;
    private int approvedNCount;
    private int pendingCount;
    private BigDecimal approvedNet;
    private BigDecimal pendingNet;
    private Set<History> historyList;

    public ShowUserDto() {
    }

    public Set<History> getHistoryList() {
        return historyList;
    }

    public BigDecimal getApprovedPoliciesNet() {
        return approvedNet;
    }

    public BigDecimal getPendingPoliciesNet() {
        return pendingNet;
    }

    public int getUserId() {
        return userId;
    }

    public String getFullName() {
        return fullName;
    }

    public int getSimulatedOffers() {
        return simulatedOffers;
    }

    public int getApprovedNCount() {
        return approvedNCount;
    }

    public int getPendingCount() {
        return pendingCount;
    }

    public BigDecimal getApprovedNet() {
        return approvedNet;
    }

    public BigDecimal getPendingNet() {
        return pendingNet;
    }

    public void setHistoryList(Set<History> historyList) {
        this.historyList = historyList;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public void setSimulatedOffers(int simulatedOffers) {
        this.simulatedOffers = simulatedOffers;
    }

    public void setApprovedNCount(int approvedNCount) {
        this.approvedNCount = approvedNCount;
    }

    public void setPendingCount(int pendingCount) {
        this.pendingCount = pendingCount;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public void setApprovedNet(BigDecimal approvedNet) {
        this.approvedNet = approvedNet;
    }

    public void setPendingNet(BigDecimal pendingNet) {
        this.pendingNet = pendingNet;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ShowUserDto that = (ShowUserDto) o;
        return userId == that.userId &&
                simulatedOffers == that.simulatedOffers &&
                approvedNCount == that.approvedNCount &&
                pendingCount == that.pendingCount &&
                Objects.equals(fullName, that.fullName) &&
                Objects.equals(approvedNet, that.approvedNet) &&
                Objects.equals(pendingNet, that.pendingNet);
    }

    @Override
    public int hashCode() {
        return Objects.hash(userId, fullName, simulatedOffers, approvedNCount, pendingCount, approvedNet, pendingNet);
    }

    @Override
    public String toString() {
        return "ShowUserDto{" +
                "userId=" + userId +
                ", fullName='" + fullName + '\'' +
                ", simulatedOffers=" + simulatedOffers +
                ", approvedNCount=" + approvedNCount +
                ", pendingCount=" + pendingCount +
                ", approvedNet=" + approvedNet +
                ", pendingNet=" + pendingNet +
                '}';
    }
}
