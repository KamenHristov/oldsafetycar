package com.safetycar.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.validation.Validator;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import org.springframework.web.context.request.RequestContextListener;
import org.springframework.web.servlet.config.annotation.*;
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;

import static com.safetycar.util.Constants.ConfigConstants.COM_SAFETYCAR_WEB;

@Configuration
@ComponentScan(basePackages = {COM_SAFETYCAR_WEB})
public class MvcConfig implements WebMvcConfigurer {

    public static final String[] STATIC_RESOURCES = {
            "/assets/",
            "/css/",
            "/js/",
            "/less/"
    };
    public static final String STATIC_PATH = "/static/**";
    private final MessageSource messageSource;

    @Autowired
    public MvcConfig(MessageSource messageSource) {
        this.messageSource = messageSource;
    }

    @Override
    public void addViewControllers(final ViewControllerRegistry registry) {
        //TODO
        registry.addViewController("/").setViewName("/index");
        registry.addViewController("/index.html");
    }

    @Override
    public void configureDefaultServletHandling(final DefaultServletHandlerConfigurer configurer) {
        configurer.enable();
    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        //TODO
        registry.addResourceHandler(STATIC_PATH).addResourceLocations(STATIC_RESOURCES);
    }

    @Override
    public void addInterceptors(final InterceptorRegistry registry) {
        //TODO
        final LocaleChangeInterceptor localeChangeInterceptor = new LocaleChangeInterceptor();
        localeChangeInterceptor.setParamName("lang");
        registry.addInterceptor(localeChangeInterceptor);
    }

    @Bean
    @ConditionalOnMissingBean(RequestContextListener.class)
    public RequestContextListener requestContextListener() {
        return new RequestContextListener();
    }

    @Override
    public Validator getValidator() {
        LocalValidatorFactoryBean validator = new LocalValidatorFactoryBean();
        validator.setValidationMessageSource(messageSource);
        return validator;
    }

}