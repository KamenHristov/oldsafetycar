package com.safetycar.config;

import com.safetycar.security.CustomAuthenticationProvider;
import com.safetycar.services.contracts.UserService;
import com.safetycar.util.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.JdbcUserDetailsManager;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

import javax.sql.DataSource;

import static com.safetycar.util.Constants.ConfigConstants.COM_SAFETYCAR_SECURITY;
import static com.safetycar.util.Constants.ConfigConstants.USER_DETAILS_SERVICE;
import static com.safetycar.web.controllers.mvc.MvcIndex.INDEX_ENDPOINT;

@Configuration
@Order(2)
@ComponentScan(basePackages = COM_SAFETYCAR_SECURITY)
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    public static final String USER = "USER";
    public static final String AGENT = "AGENT";

    public static final String[] ROLES = {"USER,AGENT"};
    private static final String[] PUBLIC_MATCHERS = {"/css/**", "/js/**",
            "/assets/img/**", "/img/**", "/", INDEX_ENDPOINT, "/login/**", "/login/confirm/**",
            "/register/**", "/images/**", "/proceed", "/api/models/**", "/email/send",
            "/change-password/verification", "/change-password/submit"};
    private static final String[] USER_MATCHERS = {"/details", "/offer",};
    private static final String[] AGENT_MATCHERS = {"/users", "/user",};
    private static final String[] SHARED_MATCHERS = {"/policy", "/policies"};

    private final DataSource dataSource;
    private final AuthenticationSuccessHandler authenticationSuccessHandler;
    private final UserDetailsService userDetailsService;
    private final UserService userService;
    private final MessageSource messageSource;

    @Autowired
    public SecurityConfig(DataSource dataSource,
                          AuthenticationSuccessHandler authenticationSuccessHandler,
                          @Qualifier(USER_DETAILS_SERVICE) UserDetailsService userDetailsService,
                          UserService userService,
                          MessageSource messageSource) {
        this.dataSource = dataSource;
        this.authenticationSuccessHandler = authenticationSuccessHandler;
        this.userDetailsService = userDetailsService;
        this.userService = userService;
        this.messageSource = messageSource;
    }

    @Bean
    public UserDetailsManager userDetailsManager() {
        JdbcUserDetailsManager jdbcUserDetailsManager = new JdbcUserDetailsManager();
        jdbcUserDetailsManager.setDataSource(dataSource);
        return jdbcUserDetailsManager;
    }

    @Bean
    public DaoAuthenticationProvider authProvider() {
        final CustomAuthenticationProvider authProvider = new CustomAuthenticationProvider(userService, messageSource);
        authProvider.setUserDetailsService(userDetailsService);
        authProvider.setPasswordEncoder(encoder());
        return authProvider;
    }

    @Bean
    public PasswordEncoder encoder() {
        return new BCryptPasswordEncoder(11);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable().authorizeRequests()
                .antMatchers(PUBLIC_MATCHERS).permitAll()
                .antMatchers(USER_MATCHERS).hasRole(USER)
                .antMatchers(AGENT_MATCHERS).hasRole(AGENT)
                .antMatchers(SHARED_MATCHERS).hasAnyRole(ROLES)
                .anyRequest()
                .authenticated()
                .and()
                .formLogin()
                .successHandler(authenticationSuccessHandler)
                .failureUrl("/login/error")
                .loginPage("/login")
                .loginProcessingUrl("/authenticate")
                .permitAll()
                .and()
                .logout()
                .permitAll()
                .and();
    }


}


