package com.safetycar.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

import java.util.Collections;

import static com.safetycar.util.Constants.ConfigConstants.*;

@Configuration
public class SwaggerConfiguration {

    private static final String HEROKU_URL = "https://safety-car-v1.herokuapp.com";

    private static final String API_EMAIL = "spring.mail.username";

    private final String apiEmail;

    public SwaggerConfiguration(Environment environment) {
        this.apiEmail = environment.getProperty(API_EMAIL);
    }

    @Bean
    public Docket swaggerConfigurationDocket() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .paths(PathSelectors.ant("/api/**"))
                .apis(RequestHandlerSelectors.basePackage(CONTROLLERS_REST))
                .build()
                .apiInfo(getApiInfo());
    }


    private ApiInfo getApiInfo() {
        String title = "SafetyCar API Documentation";
        String description = "Api for calculating price for Insurance, creating Users, " +
                "creating Insurance Policies, and moderate them.";
        String version = "1.0";
        Contact georgi_shutov_and_kamen_hristov = getContact();

        return new ApiInfo(
                title,
                description,
                version,
                HEROKU_URL,
                georgi_shutov_and_kamen_hristov,
                "API License",
                HEROKU_URL,
                Collections.emptyList());

    }

    private Contact getContact() {
        String names = "Georgi Shutov and Kamen Hristov";
        String repoUrl = "https://gitlab.com/g.h.shutov/safety-car-project";
        return new Contact(names,
                repoUrl,
                apiEmail);
    }
}
