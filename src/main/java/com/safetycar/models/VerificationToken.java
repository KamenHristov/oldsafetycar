package com.safetycar.models;

import com.safetycar.models.base.TokenBase;

import javax.persistence.*;
import java.util.Calendar;
import java.util.Date;
import java.util.Objects;

import static com.safetycar.util.Constants.VerificationTokenConstants.EXPIRATION;

@Entity(name = "VerificationToken")
@Table(name = "tokens_verification")
public class VerificationToken extends TokenBase {

    @Column(name = "expiry_date")
    @Temporal(value = TemporalType.TIMESTAMP)
    private Date expiryDate;

    public VerificationToken() {
        expiryDate = calculateExpiryDate();
    }

    private Date calculateExpiryDate() {
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date(System.currentTimeMillis()));
        cal.add(Calendar.MINUTE, EXPIRATION);
        return cal.getTime();
    }

    public static int getEXPIRATION() {
        return EXPIRATION;
    }

    public Date getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(Date expiryDateMilliSec) {
        this.expiryDate = expiryDateMilliSec;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        VerificationToken token = (VerificationToken) o;
        return Objects.equals(expiryDate, token.expiryDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), expiryDate);
    }
}