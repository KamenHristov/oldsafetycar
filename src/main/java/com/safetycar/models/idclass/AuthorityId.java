package com.safetycar.models.idclass;

import java.io.Serializable;
import java.util.Objects;

public class AuthorityId implements Serializable {

    private String userName;
    private String authority;

    public AuthorityId() {
    }

    public AuthorityId(String userName, String authority) {
        this.userName = userName;
        this.authority = authority;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AuthorityId that = (AuthorityId) o;
        return Objects.equals(userName, that.userName) &&
                Objects.equals(authority, that.authority);
    }

    @Override
    public int hashCode() {
        return Objects.hash(userName, authority);
    }
}
