package com.safetycar.models.base;

import com.safetycar.models.User;

public interface Token {

    String getToken();

    User getUser();
}
