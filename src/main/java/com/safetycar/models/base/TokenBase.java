package com.safetycar.models.base;

import com.safetycar.models.User;

import javax.persistence.*;
import java.util.Objects;

@MappedSuperclass
public class TokenBase implements Token {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(nullable = false, name = "username")
    private User user;

    @Column(name = "token")
    private String token;

    public TokenBase() {
    }

    public int getId() {
        return id;
    }

    public String getToken() {
        return token;
    }

    public User getUser() {
        return user;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TokenBase tokenBase = (TokenBase) o;
        return id == tokenBase.id &&
                Objects.equals(user, tokenBase.user) &&
                Objects.equals(token, tokenBase.token);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, user, token);
    }
}
