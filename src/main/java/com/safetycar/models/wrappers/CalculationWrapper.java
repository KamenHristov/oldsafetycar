package com.safetycar.models.wrappers;

import com.safetycar.models.Coefficient;

import java.math.BigDecimal;
import java.util.Objects;

public class CalculationWrapper {

    private final BigDecimal baseAmount;
    private final boolean hadAccidents;
    private final boolean aboveTwentyFive;
    private final Coefficient coefficient;

    public CalculationWrapper(final BigDecimal baseAmount,
                              final boolean hadAccidents,
                              final boolean aboveTwentyFive,
                              final Coefficient coefficient) {
        this.baseAmount = baseAmount;
        this.hadAccidents = hadAccidents;
        this.aboveTwentyFive = aboveTwentyFive;
        this.coefficient = coefficient;
    }

    public BigDecimal getBaseAmount() {
        return baseAmount;
    }

    public boolean hadAccidents() {
        return hadAccidents;
    }

    public boolean isAboveTwentyFive() {
        return aboveTwentyFive;
    }

    public Coefficient getCoefficient() {
        return coefficient;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CalculationWrapper wrapper = (CalculationWrapper) o;
        return hadAccidents == wrapper.hadAccidents &&
                aboveTwentyFive == wrapper.aboveTwentyFive &&
                Objects.equals(baseAmount, wrapper.baseAmount) &&
                Objects.equals(coefficient, wrapper.coefficient);
    }

    @Override
    public int hashCode() {
        return Objects.hash(baseAmount, hadAccidents, aboveTwentyFive, coefficient);
    }
}
