package com.safetycar.models.wrappers;

import com.safetycar.models.User;
import com.safetycar.models.UserDetails;

import javax.validation.constraints.NotNull;

public class UserDuplicateWrapper {
		
		private String email;
		private String telephone;
		private String address;
		private String firstName;
		private String lastName;
		
		public UserDuplicateWrapper() {
		}
		
		public UserDuplicateWrapper(@NotNull User user) {
				UserDetails details = user.getUserDetails();
				assert details != null;
				this.email = details.getUserName();
				this.telephone = details.getTelephone();
				this.address = details.getAddress();
				this.firstName = details.getFirstName();
				this.lastName = details.getLastName();
		}
		
		public UserDuplicateWrapper(String email,
				String telephone,
				String address,
				String firstName,
				String lastName) {
				this.email = email;
				this.telephone = telephone;
				this.address = address;
				this.firstName = firstName;
				this.lastName = lastName;
		}
		
		public UserDuplicateWrapper(UserDetails details) {
				this.email = details.getUserName();
				this.telephone = details.getTelephone();
				this.address = details.getAddress();
				this.firstName = details.getFirstName();
				this.lastName = details.getLastName();
		}
		
		public boolean isDuplicateOf(UserDetails userDetails) {
				if (!userDetails.isActive()) {
						return false;
				}
				boolean telephoneEquals = userDetails.getTelephone().equals(this.telephone);
				
				boolean firstNameEquals = userDetails.getFirstName().equals(this.firstName);
				boolean lastNameEquals = userDetails.getLastName().equals(this.lastName);
				boolean addressEquals = userDetails.getAddress().equals(this.address);
				
				return telephoneEquals || (firstNameEquals && lastNameEquals && addressEquals);
		}
		
		public boolean detailsHaveChanged(UserDetails userDetails) {
				if (!userDetails.isActive()) {
						return true;
				}
				boolean telephoneEquals = userDetails.getTelephone().equals(this.telephone);
				
				boolean firstNameEquals = userDetails.getFirstName().equals(this.firstName);
				boolean lastNameEquals = userDetails.getLastName().equals(this.lastName);
				boolean addressEquals = userDetails.getAddress().equals(this.address);
				return !telephoneEquals || !firstNameEquals || !lastNameEquals || !addressEquals;
		}
		
		public String getEmail() {
				return email;
		}
		
		public String getAddress() {
				return address;
		}
		
		public String getFirstName() {
				return firstName;
		}
		
		public String getLastName() {
				return lastName;
		}
		
		public String getTelephone() {
				return telephone;
		}
		
		public void setEmail(String email) {
				this.email = email;
		}
		
		public void setTelephone(String telephone) {
				this.telephone = telephone;
		}
		
		public void setAddress(String address) {
				this.address = address;
		}
		
		public void setFirstName(String firstName) {
				this.firstName = firstName;
		}
		
		public void setLastName(String lastName) {
				this.lastName = lastName;
		}
}
