package com.safetycar.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.*;

@Entity
@Table(name = "users")
public class User {

    @Id
    @Column(name = "username")
    private String userName;

    @Column(name = "password")
    private String password;

    @Column(name = "enabled")
    private boolean enabled;

    @Column(name = "deleted")
    private boolean deleted;

    @OneToMany(fetch = FetchType.EAGER, cascade = {CascadeType.ALL})
    @JoinColumn(name = "username")
    private Set<Authority> authorities;

    @OneToOne(fetch = FetchType.EAGER, cascade = {CascadeType.ALL})
    @JoinColumn(name = "username")
    private UserDetails userDetails;

    public User() {
    }

    public void addHistory(History history) {
        this.userDetails.addHistory(history);
    }

    public void addOffer(Offer offer) {
        this.userDetails.addOffer(offer);
    }

    public void removeOffer(Offer offer) {
        this.userDetails.removeOffer(offer);
    }

    public void addPolicy(Policy policy) {
        this.userDetails.addPolicy(policy);
    }

    public void removePolicy(Policy policy) {
        this.userDetails.removePolicy(policy);
    }

    public void setUserName(String username) {
        this.userName = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public void setAuthorities(Set<Authority> authorities) {
        this.authorities = authorities;
    }

    public UserDetails getUserDetails() {
        return userDetails;
    }

    public String getUserName() {
        return userName;
    }

    public String getPassword() {
        return password;
    }

    public boolean isDisabled() {
        return !enabled;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public Set<Authority> getAuthorities() {
        if (authorities == null) return new HashSet<>()
                ;
        return new HashSet<>(authorities);
    }

    public void setUserDetails(UserDetails userDetails) {
        this.userDetails = userDetails;
    }

    @JsonIgnore
    public Collection<Offer> getOffers() {
        return userDetails.getOffers();
    }

    @JsonIgnore
    public Collection<Policy> getPolicies() {
        return userDetails.getPolicies();
    }

    @JsonIgnore
    public Collection<History> getHistory() {
        return userDetails.getHistory();
    }

    public boolean isAdmin() {
        return authorities.stream().anyMatch(r -> r.getAuthority().equals("ROLE_AGENT"));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return enabled == user.enabled &&
                deleted == user.deleted &&
                Objects.equals(userName, user.userName) &&
                Objects.equals(password, user.password) &&
                Objects.equals(authorities, user.authorities) &&
                Objects.equals(userDetails, user.userDetails);
    }

    @Override
    public int hashCode() {
        return Objects.hash(userName, password, enabled, deleted, authorities, userDetails);
    }

    @Override
    public String toString() {
        return userName;
    }
}
