package com.safetycar.models;

import com.safetycar.models.base.TokenBase;

import javax.persistence.*;

@Entity
@Table(name = "tokens_forgotten_password")
public class PasswordToken extends TokenBase {

    public PasswordToken() {
    }

}
