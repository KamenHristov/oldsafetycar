package com.safetycar.repositories;

import com.safetycar.models.Offer;
import com.safetycar.models.User;
import com.safetycar.repositories.base.SafetyCarRepository;

public interface OfferRepository extends SafetyCarRepository<Offer,
        Integer>, OfferExists {

    @Override
    boolean exists(Offer offer, User user);

}
