package com.safetycar.repositories;

import com.safetycar.models.UserDetails;

public interface UserExists {

    boolean existsByUserDetails(UserDetails userDetails);

}
