package com.safetycar.repositories;

import com.safetycar.models.UserDetails;
import com.safetycar.repositories.base.SafetyCarRepository;

import java.util.List;
import java.util.Optional;

public interface UserDetailsRepository extends SafetyCarRepository<UserDetails, Integer> {

    @Override
    default Optional<UserDetails> findById(Integer id) {
        return Optional.ofNullable(findByIntegerId(id));
    }

    UserDetails findByIntegerId(Integer id);

}
