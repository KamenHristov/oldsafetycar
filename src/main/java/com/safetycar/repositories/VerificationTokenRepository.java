package com.safetycar.repositories;


import com.safetycar.models.VerificationToken;
import com.safetycar.models.User;
import com.safetycar.repositories.base.SafetyCarRepository;

public interface VerificationTokenRepository
        extends SafetyCarRepository<VerificationToken, Integer> {

}
