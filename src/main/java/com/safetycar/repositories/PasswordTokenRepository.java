package com.safetycar.repositories;

import com.safetycar.models.PasswordToken;
import com.safetycar.repositories.base.SafetyCarRepository;

public interface PasswordTokenRepository
        extends SafetyCarRepository<PasswordToken, Integer> {

    boolean existsByToken(String token);

}
