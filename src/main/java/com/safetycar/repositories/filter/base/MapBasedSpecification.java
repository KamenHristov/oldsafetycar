package com.safetycar.repositories.filter.base;

import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;

import java.util.Map;

/** This class adds a {@link java.util.Map<String, String>} to
 * {@link org.springframework.data.jpa.domain.Specification} to manage
 *  query parameters and values.
 *
 * @param <T> Type of entity to query for
 */
public interface MapBasedSpecification<T> extends Specification<T> {

    Map<String, String> getSpecs();

    void addSpec(String attribute, String value);

    void removeSpec(String attribute);

    /**
     * @return {@link org.springframework.data.domain.Sort} object to use along
     * with the specification
     */
    Sort getSort();

}
