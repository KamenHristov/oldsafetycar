package com.safetycar.repositories.filter;

import com.safetycar.models.Offer;
import com.safetycar.repositories.filter.base.MapBasedSpecification;

import java.util.Map;

public class SafetyCarSpecFactory {

        public static MapBasedSpecification<Offer> getOfferSpec() {
                return new OfferSpec();
        }

        public static MapBasedSpecification<Offer>
        getOfferSpecWithFilteр(Map<String,String> filterParams) {
                return new OfferSpec(filterParams);
        }

        public static MapBasedSpecification<Offer> getOfferSpecWithFilterAndIn
            (Map<String,String> filterParams) {
                return new OfferSpec();
        }
}
