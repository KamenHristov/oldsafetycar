package com.safetycar.repositories.filter;

import com.safetycar.models.Authority;
import com.safetycar.models.Policy;
import com.safetycar.models.User;
import com.safetycar.models.UserDetails;
import com.safetycar.repositories.filter.base.BaseMapSpec;

import javax.persistence.criteria.*;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import static com.safetycar.util.Constants.QueryConstants.*;
import static com.safetycar.web.controllers.mvc.MvcUserController.USER_DETAILS;

public class UserSpec extends BaseMapSpec<User> {

    public static final String PENDING_POLICIES_NET = "pendingPoliciesNet";
    public static final String APPROVED_POLICIES_NET = "approvedPoliciesNet";
    public static final String PENDING_POLICIES_COUNT = "pendingPoliciesCount";
    public static final String APPROVED_POLICIES_COUNT = "approvedPoliciesCount";
    public static final String POLICY_STATUS = "policyStatus";
    public static final String ID = "integerId";
    public static final String ACTIVE = "active";
    public static final String INACTIVE = "inactive";
    public static final String PENDING = "pending";
    public static final String STATUS = "status";
    public static final String POLICIES = "policies";
    public static final String OFFERS_COUNT = "offersCount";
    public static final String REJECTED = "rejected";
    public static final String FULL_NAME = "fullName";
    public static final String TRUE = "true";
    public static final String USER_DETAILS_ = "userDetails.";
    public static final String OWNER = "owner";
    public static final String AUTHORITIES = "authorities";
    public static final String AUTHORITY = "authority";
    public static final String ROLE_USER = "ROLE_USER";
    public static final String SQL_LIKE_SYMBOL = "%";
    public static final String USERNAME = "userName";
    public static final String APPROVED = "approved";

    private boolean isAdmin;
    private Join<User, UserDetails> detailsJoin;
    private List<Predicate> predicates;

    public UserSpec() {
    }

    public UserSpec(Map<String, String> specs) {
        super(specs);
    }

    @Override
    public Predicate toPredicate(Root<User> root,
                                 CriteriaQuery<?> query,
                                 CriteriaBuilder criteriaBuilder) {
        this.detailsJoin = getDetailsJoin(root);
        query.distinct(true);
        return getPredicate(criteriaBuilder, root, query);
    }

    private Predicate getPredicate(CriteriaBuilder criteriaBuilder, Root<User> root, CriteriaQuery<?> query) {
        this.predicates = new LinkedList<>();
        Map<String, String> specs = super.getSpecs();

        if (!this.isAdmin) {
            Join<User, Authority> roleJoin = root.join(AUTHORITIES);
            Predicate resultsDoNotContainAgents =
                    criteriaBuilder.equal(roleJoin.get(AUTHORITY), ROLE_USER);
            predicates.add(resultsDoNotContainAgents);
        }

        for (Map.Entry<String, String> entry : specs.entrySet()) {
            switch (entry.getKey()) {
                case FULL_NAME: {
                    Predicate likeFullName = getLikeFullNamePredicate(criteriaBuilder, entry.getValue());
                    this.predicates.add(likeFullName);
                    break;
                }
                case ACTIVE: {
                    int tinyIntTrue = 1;
                    this.predicates.add(criteriaBuilder.equal(this.detailsJoin.get(ACTIVE), tinyIntTrue));
                    break;
                }
                case POLICY_STATUS: {
                    appendStatusPredicateWhenPresent(criteriaBuilder, query, entry.getValue());
                    break;
                }
            }
        }
        return criteriaBuilder.and(this.predicates.toArray(Predicate[]::new));
    }

    private Join<User, UserDetails> getDetailsJoin(Root<User> root) {
        return root.join(USER_DETAILS, JoinType.INNER);
    }

    private Predicate getLikeFullNamePredicate(CriteriaBuilder criteriaBuilder,
                                               String value) {
        Expression<String> fullNameConcat = getFullNameConcat(criteriaBuilder);

        String sqlLikeValue = getSqlLikeValue(value);
        return criteriaBuilder.like(fullNameConcat, sqlLikeValue);
    }

    private Expression<String> getFullNameConcat(CriteriaBuilder criteriaBuilder) {
        String concatSeparator = " ";
        Expression<String> concatFirstName = criteriaBuilder
                .concat(this.detailsJoin.get(FIRST_NAME), concatSeparator);
        return criteriaBuilder
                .concat(concatFirstName, this.detailsJoin.get(LAST_NAME));
    }

    private String getSqlLikeValue(String value) {
        return SQL_LIKE_SYMBOL + value + SQL_LIKE_SYMBOL;
    }

    private void appendStatusPredicateWhenPresent(CriteriaBuilder criteriaBuilder,
                                                  CriteriaQuery<?> query,
                                                  String value) {
        switch (value) {
            case PENDING:
            case REJECTED:
            case INACTIVE:
            case APPROVED: {
                Predicate policyStatus = getPolicyStatusPredicate(criteriaBuilder, query, value);
                this.predicates.add(policyStatus);
            }
        }
    }

    private Predicate getPolicyStatusPredicate(CriteriaBuilder criteriaBuilder, CriteriaQuery<?> query, String status) {
        Root<UserDetails> detailsRoot = query.from(UserDetails.class);
        Join<UserDetails, Policy> detailsPolicyJoin = detailsRoot.join(POLICIES);

        Path<String> statusPath = detailsPolicyJoin.get(STATUS).get(STATUS);
        Predicate likeStatus = criteriaBuilder.like(statusPath, getSqlLikeValue(status));

        Path<Object> userNamePath = detailsPolicyJoin.get(OWNER).get(USERNAME);
        Predicate simulateUserJoin = criteriaBuilder.equal(userNamePath, this.detailsJoin.get(USERNAME));

        return criteriaBuilder.and(likeStatus, simulateUserJoin);
    }

    public String getSortParam(String sortParameter) {
        switch (sortParameter) {
            case PENDING_POLICIES_NET:
            case APPROVED_POLICIES_NET:
            case PENDING_POLICIES_COUNT:
            case APPROVED_POLICIES_COUNT:
            case OFFERS_COUNT: {
                break;
            }
            default: {
                sortParameter = ID;
                break;
            }
        }
        return USER_DETAILS_ + sortParameter;
    }

    public boolean isAdmin() {
        return isAdmin;
    }

    public void setIsAdmin(boolean isAdmin) {
        this.isAdmin = isAdmin;
    }
}
