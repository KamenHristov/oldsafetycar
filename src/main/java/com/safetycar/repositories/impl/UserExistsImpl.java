package com.safetycar.repositories.impl;

import com.safetycar.models.UserDetails;
import com.safetycar.models.wrappers.UserDuplicateWrapper;
import com.safetycar.repositories.UserExists;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Tuple;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.util.LinkedList;
import java.util.List;

import static com.safetycar.repositories.filter.UserSpec.USERNAME;
import static com.safetycar.util.Constants.QueryConstants.*;

@Component
public class UserExistsImpl implements UserExists {

    @PersistenceContext
    private EntityManager entityManager;

    /**
     * Users are considered duplicates when their phone number is not unique
     * or the combination of their firstName, lastName and address is not unique
     *
     * @param userDetails
     * @return true if duplicate
     */
    @Override
    public boolean existsByUserDetails(UserDetails userDetails) {
        List<Tuple> possibleDuplicates = queryForDuplicates(userDetails);
        for (Tuple duplicate : possibleDuplicates) {
            if (!userDetails.isActive()) {
                return false;
            }
            boolean telephoneEquals = userDetails.getTelephone().equals(duplicate.get(0));

            boolean firstNameEquals = userDetails.getFirstName().equals(duplicate.get(1));
            boolean lastNameEquals = userDetails.getLastName().equals(duplicate.get(2));
            boolean addressEquals = userDetails.getAddress().equals(duplicate.get(3));

            return telephoneEquals || (firstNameEquals && lastNameEquals && addressEquals);
        }
        return false;
    }

    private List<Tuple> queryForDuplicates(UserDetails userDetails) {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<Tuple> cq = cb.createQuery(Tuple.class);
        Root<UserDetails> userRoot = cq.from(UserDetails.class);

        List<Path<Object>> userDetailsPaths = getAttrPaths(userRoot);

        Predicate excludeThis = getExcludeThisPredicate(userDetails, cb, userDetailsPaths);
        cq.multiselect(userDetailsPaths.toArray(Path[]::new)).where(excludeThis);

        TypedQuery<Tuple> query = entityManager.createQuery(cq);
        return query.getResultList();
    }

    private Predicate getExcludeThisPredicate(UserDetails userDetails, CriteriaBuilder cb,
                                              List<Path<Object>> userDetailsPaths) {
        Path<Object> userName = userDetailsPaths.get(0);
        return cb.notEqual(userName, userDetails.getUserName());
    }

    private List<Path<Object>> getAttrPaths(Root<UserDetails> userRoot) {
        List<Path<Object>> paths = new LinkedList<>();
        paths.add(userRoot.get(USERNAME));
        paths.add(userRoot.get(TELEPHONE));
        paths.add(userRoot.get(FIRST_NAME));
        paths.add(userRoot.get(LAST_NAME));
        paths.add(userRoot.get(ADDRESS));
        return paths;
    }
}
