package com.safetycar.repositories.impl;

import com.safetycar.models.Car;
import com.safetycar.models.Offer;
import com.safetycar.models.Policy;
import com.safetycar.models.User;
import com.safetycar.repositories.OfferExists;
import org.springframework.stereotype.Component;

import java.util.Set;
import java.util.stream.Collectors;

@Component
public class OfferExistsImpl implements OfferExists {

    @Override
    public boolean exists(final Offer newOffer, final User user) {
        if (newOffer == null) return false;
        final Car newCar = newOffer.getCar();
        final Set<Car> usersCars = getOfferCars(user);
        addPolicyCarsTo(usersCars, user);
        return usersCars.contains(newCar);
    }

    private void addPolicyCarsTo(Set<Car> usersCars, User userToCheckOffersFrom) {
        userToCheckOffersFrom
                .getUserDetails()
                .getPolicies().stream()
                .map(Policy::getCar).forEach(usersCars::add);
    }

    private Set<Car> getOfferCars(User user) {
        return user
                .getOffers()
                .stream()
                .map(Offer::getCar)
                .collect(Collectors.toSet());
    }
}
