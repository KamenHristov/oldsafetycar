package com.safetycar.repositories;

import com.safetycar.models.Authority;
import com.safetycar.models.idclass.AuthorityId;
import com.safetycar.repositories.base.SafetyCarRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

public interface AuthorityRepository extends SafetyCarRepository<Authority, AuthorityId> {

    @Modifying
    @Transactional
    @Query(value = "delete from Authority where userName = :userName")
    void deleteAuthority(@Param(value = "userName") String userName);
}
