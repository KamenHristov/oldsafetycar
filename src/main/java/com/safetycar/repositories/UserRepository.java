package com.safetycar.repositories;

import com.safetycar.models.User;
import com.safetycar.models.UserDetails;
import com.safetycar.repositories.base.SafetyCarRepository;

public interface UserRepository extends SafetyCarRepository<User, String>, UserExists {

    boolean existsByUserDetails(UserDetails userDetails);

    boolean existsByUserName(String username);

}
