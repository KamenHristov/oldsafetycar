package com.safetycar.security;


import com.safetycar.exceptions.AccountNotActivatedException;
import com.safetycar.exceptions.EntityNotFoundException;
import com.safetycar.models.User;
import com.safetycar.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.util.Locale;

public class CustomAuthenticationProvider extends DaoAuthenticationProvider {

    public static final String MESSAGE_INVALID_USER = "auth.message.invalidUser";
    public static final String MESSAGE_BAD_CREDENTIALS = "auth.message.badCredentials";
    private final UserService userService;

    private final MessageSource messageSource;

    public CustomAuthenticationProvider(UserService userService,
                                        MessageSource messageSource) {
        this.userService = userService;
        this.messageSource = messageSource;
    }

    @Override
    public Authentication authenticate(Authentication auth) throws AuthenticationException {
        try {
            final User user = userService.userByEmail(auth.getName());
            final Authentication result = super.authenticate(auth);
            return new UsernamePasswordAuthenticationToken(user,
                    result.getCredentials(),
                    result.getAuthorities());
        } catch (final EntityNotFoundException e) {
            throw new UsernameNotFoundException(messageSource.getMessage(MESSAGE_INVALID_USER, null, Locale.ENGLISH));
        } catch (AccountNotActivatedException e) {
            throw new DisabledException(e.getMessage());
        } catch (BadCredentialsException e) {
            throw new BadCredentialsException(messageSource.getMessage(MESSAGE_BAD_CREDENTIALS, null, Locale.ENGLISH));
        }
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.equals(UsernamePasswordAuthenticationToken.class);
    }
}