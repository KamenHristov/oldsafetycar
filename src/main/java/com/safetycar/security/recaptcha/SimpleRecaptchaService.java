package com.safetycar.security.recaptcha;

public interface SimpleRecaptchaService {

    void validateRecaptcha(String response);

}
