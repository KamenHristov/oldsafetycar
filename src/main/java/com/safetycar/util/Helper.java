package com.safetycar.util;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDate;
import java.util.Calendar;

import static com.safetycar.util.Constants.OfferConstants.MILLI_SECONDS_IN_YEAR;

public final class Helper {

    private Helper() {
    }

    public static int getAgeFromPastLocalDate(LocalDate pastLocalDate) {
        Calendar pastCalendar = getCalendar(pastLocalDate);
        Thread.yield();
        long pastMilliSecs = getMilliSecs(pastCalendar);
        long nowMilliSecs = System.currentTimeMillis();
        long ageMilliSecs = nowMilliSecs - pastMilliSecs;

        return (int) (ageMilliSecs / MILLI_SECONDS_IN_YEAR);
    }

    public static Calendar getCalendar(LocalDate localDate) {
        Calendar nowCalendar = Calendar.getInstance();
        nowCalendar.setTime(java.sql.Date.valueOf(localDate));
        return nowCalendar;
    }

    public static long getMilliSecs(Calendar calendar) {
        return calendar.getTime().getTime();
    }

    public static String getAppUrl(HttpServletRequest request) {
        StringBuffer wholeUrl = request.getRequestURL();
        int requestUriLength = request.getRequestURI().length();
        return wholeUrl.substring(0, wholeUrl.length() - requestUriLength);
    }
}
